﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncChangedAdminSiteAccount.Models
{
    public class AdminMembershipChangeModel
    {
        public int AdminId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phonenumber { get; set; }
        public string Address { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdateType { get; set; }
    }
}
