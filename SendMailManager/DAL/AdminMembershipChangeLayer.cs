﻿using BCRM.BXH.ThaiLand.Library;
using Npgsql;
using SyncChangedAdminSiteAccount.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncChangedAdminSiteAccount.DAL
{
    public class AdminMembershipChangeLayer
    {
        public List<AdminMembershipChangeModel> GetByLimited(int limited)
        {
            var ret = new List<AdminMembershipChangeModel>();
            using (var db = new PostgresSQL(ConfigurationManager.ConnectionStrings["AdminBXHIndo"].ToString()))
            {
                using (var command = db.CreateCommand("func_bcrm_membershipchange_getnotprocess", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_limitrecord", limited));
                    //Truyền tham số cho command
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var city = new AdminMembershipChangeModel();
                            EntityBase.SetObjectValue(reader, ref city);
                            if (city != null)
                            {
                                ret.Add(city);
                            }
                        }
                    }
                }
            }
            return ret;
        }

        public int UpdateProcess(int adminId,bool isProcess)
        {
            using (var db = new PostgresSQL(ConfigurationManager.ConnectionStrings["AdminBXHIndo"].ToString()))
            {
                using (var command = db.CreateCommand("func_bcrm_membershipchange_updatestatus", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_adminid", adminId));
                    command.Parameters.Add(new NpgsqlParameter("_isprocess", isProcess));
                    return (int)command.ExecuteScalar();
                }
            }
        }
    }
}
