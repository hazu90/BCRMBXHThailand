﻿using BCRM.BXH.ThaiLand.Library;
using Npgsql;
using SyncChangedAdminSiteAccount.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncChangedAdminSiteAccount.DAL
{
    public class SyncBXHAdminNBCRMLayer
    {
        public void Create(int adminId,int customerId, string email, string phoneNumber)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("syncbcrmbxhnadmin_create", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_accountid", adminId));
                    command.Parameters.Add(new NpgsqlParameter("_email", email));
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    command.ExecuteNonQuery();
                }
            }
        }

        public SyncBcrmBxhNAdminModel GetByAdminId(int adminId)
        {
            var ret = new SyncBcrmBxhNAdminModel();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("service_syncbcrmbxhnadmin_getbyadminid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_adminid", adminId));
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            EntityBase.SetObjectValue(reader, ref ret);
                            return ret;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
    }
}
