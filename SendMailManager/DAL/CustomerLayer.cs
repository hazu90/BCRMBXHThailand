﻿using BCRM.BXH.ThaiLand.Library;
using Npgsql;
using SyncChangedAdminSiteAccount.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncChangedAdminSiteAccount.DAL
{
    public class CustomerLayer
    {
        public int Create(string fullName,string email , string phoneNumber,string address ,string createdBy, DateTime createdDate,int type,int cityId,string assignTo)
        {
            using (var db = new PostgresSQL())
            {
                var query = new StringBuilder();
                using (var command = db.CreateCommand("service_customer_create", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_fullname", string.IsNullOrEmpty(fullName) ? "" : fullName));
                    command.Parameters.Add(new NpgsqlParameter("_email", string.IsNullOrEmpty(email) ? "" : email));
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", string.IsNullOrEmpty(phoneNumber) ? "" : phoneNumber));
                    command.Parameters.Add(new NpgsqlParameter("_address", string.IsNullOrEmpty(address) ? "" : address ));
                    command.Parameters.Add(new NpgsqlParameter("_createddate", createdDate ));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    return (int)command.ExecuteScalar();
                }
            }
        }
        public void Update(int customerId, string fullName,string email,string phoneNumber,string address,string updatedBy,DateTime updatedDate)
        {
            using (var db = new PostgresSQL())
            {
                var query = new StringBuilder();
                using (var command = db.CreateCommand("service_customer_update", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_fullname", string.IsNullOrEmpty(fullName) ? "" : fullName ));
                    command.Parameters.Add(new NpgsqlParameter("_email", string.IsNullOrEmpty(email) ? "" : email ));
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", string.IsNullOrEmpty(phoneNumber) ? "" : phoneNumber ));
                    command.Parameters.Add(new NpgsqlParameter("_address", string.IsNullOrEmpty(address) ? "" : address ));
                    command.Parameters.Add(new NpgsqlParameter("_lastmodifiedby", updatedBy));
                    command.Parameters.Add(new NpgsqlParameter("_lastmodifieddate", updatedDate));
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.ExecuteNonQuery();
                }
            }
        }

        public CustomerModel GetById(int customerId)
        {
            var ret = new CustomerModel();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("service_customer_getbyid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", customerId));
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            EntityBase.SetObjectValue(reader, ref ret);
                            return ret;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
        public bool IsExistedPhonenumber(int customerId, string phoneNumber)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("customer_isexistedphonenumber", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_id", customerId));
                    cmd.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    var iIsExisted = (int)cmd.ExecuteScalar();
                    return (iIsExisted > 0);
                }
            }
        }
        public bool IsExistedEmail(int customerId, string email)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("customer_isexistedemail", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_id", customerId));
                    cmd.Parameters.Add(new NpgsqlParameter("_email", email));
                    var iIsExisted = (int)cmd.ExecuteScalar();
                    return (iIsExisted > 0);
                }
            }
        }
    }
}
