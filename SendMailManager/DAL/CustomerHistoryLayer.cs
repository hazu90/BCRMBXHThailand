﻿using BCRM.BXH.ThaiLand.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncChangedAdminSiteAccount.DAL
{
    public class CustomerHistoryLayer
    {
        public void Create(int customerId, string action,string oldValue,string newValue,DateTime updatedDate,string updatedBy)
        {
            using (var db = new PostgresSQL())
            {
                var query = new StringBuilder();
                using (var command = db.CreateCommand("customerhistory_create", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_action", action));
                    command.Parameters.Add(new NpgsqlParameter("_oldvalue", oldValue));
                    command.Parameters.Add(new NpgsqlParameter("_newvalue", oldValue));
                    command.Parameters.Add(new NpgsqlParameter("_note", ""));
                    command.Parameters.Add(new NpgsqlParameter("_updatedby", updatedBy));
                    command.Parameters.Add(new NpgsqlParameter("_updateddate", updatedDate));
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.ExecuteNonQuery();
                }
            }
        }

        

    }
}
