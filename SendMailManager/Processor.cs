﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using BCRM.BXH.ThaiLand.Library;
using SyncChangedAdminSiteAccount.DAL;
using SyncChangedAdminSiteAccount.Models;

namespace SyncChangedAdminSiteAccount
{
    public class Processor
    {
        private Processor()
        {

        }

        public static Processor GetInstance()
        {
            return new Processor();
        }

        public void Run()
        {
            Logger.GetInstance().Write("#####################################");
            Logger.GetInstance().Write(string.Format("Start: {0:yyyy/MM/dd HH:mm:ss:FFF}", DateTime.Now));
            try
            {
                // Lấy thông tin các thay đổi
                var adminMembershipChangeLayer = new AdminMembershipChangeLayer();
                var lstChangedInAdmin = adminMembershipChangeLayer.GetByLimited(100);
                var customerLayer = new CustomerLayer();
                var syncBXHAdminNBcrmLayer = new SyncBXHAdminNBCRMLayer();
                var customerHistoryLayer = new CustomerHistoryLayer();
                foreach(var item in lstChangedInAdmin)
                {
                    if(item.UpdateType == 1)
                    {
                        if(string.IsNullOrEmpty(item.Phonenumber) && string.IsNullOrEmpty(item.Email) )
                        {
                            continue;
                        }

                        // Kiểm tra số điện thoại này đã tồn tại chưa
                        if(!string.IsNullOrEmpty(item.Phonenumber) && customerLayer.IsExistedPhonenumber(0,item.Phonenumber))
                        {
                            adminMembershipChangeLayer.UpdateProcess(item.AdminId, false);
                            continue;
                        }
                        // Kiểm tra số điện thoại này đã tồn tại chưa
                        if (!string.IsNullOrEmpty(item.Email) && customerLayer.IsExistedEmail(0, item.Email))
                        {
                            adminMembershipChangeLayer.UpdateProcess(item.AdminId, false);
                            continue;
                        }
                        // Tạo mới thông tin khách hàng
                        var customerId = customerLayer.Create(item.FullName, item.Email, item.Phonenumber,item.Address, "Guest", item.CreatedDate,TypeOfCustomer.Dealer.GetHashCode()
                                                                            , ConfigurationManager.AppSettings["DefaultCityId"].ToInt(0)
                                                                            , "");
                        // Tạo mới thông tin đồng bộ
                        syncBXHAdminNBcrmLayer.Create(item.AdminId, customerId, item.Email, item.Phonenumber);
                        adminMembershipChangeLayer.UpdateProcess(item.AdminId, true);
                    }
                    else if(item.UpdateType == 2 || item.UpdateType == 3)
                    {
                        var syncModel = syncBXHAdminNBcrmLayer.GetByAdminId(item.AdminId);
                        if(syncModel == null)
                        {
                            if(string.IsNullOrEmpty(item.Phonenumber) && string.IsNullOrEmpty(item.Email) )
                            {
                                adminMembershipChangeLayer.UpdateProcess(item.AdminId, false);
                                continue;
                            }

                            // Kiểm tra số điện thoại này đã tồn tại chưa
                            if (!string.IsNullOrEmpty(item.Phonenumber) && customerLayer.IsExistedPhonenumber(0, item.Phonenumber))
                            {
                                adminMembershipChangeLayer.UpdateProcess(item.AdminId, false);
                                continue;
                            }
                            // Kiểm tra số điện thoại này đã tồn tại chưa
                            if (!string.IsNullOrEmpty(item.Email) && customerLayer.IsExistedEmail(0, item.Email))
                            {
                                adminMembershipChangeLayer.UpdateProcess(item.AdminId, false);
                                continue;
                            }
                            // Tạo mới thông tin khách hàng
                            var customerId = customerLayer.Create(item.FullName, item.Email, item.Phonenumber,item.Address,"Guest", item.CreatedDate, TypeOfCustomer.Dealer.GetHashCode()
                                                                               ,ConfigurationManager.AppSettings["DefaultCityId"].ToInt(0)
                                                                               , "");
                            // Tạo mới thông tin đồng bộ
                            syncBXHAdminNBcrmLayer.Create(item.AdminId, customerId, item.Email, item.Phonenumber);
                            // Cập nhật lại bảng thay đổi tài khoản
                            adminMembershipChangeLayer.UpdateProcess(item.AdminId, true);
                        }
                        else
                        {
                            var customerInfo = customerLayer.GetById(syncModel.CustomerId);
                            var lstChange = GetChangedCustomerInfo(customerInfo, new CustomerModel() { FullName = item.FullName, Email = item.Email, PhoneNumber = item.Phonenumber, Address = item.Address });
                            if(lstChange.Count >0)
                            {
                                if (string.IsNullOrEmpty(item.Phonenumber) && string.IsNullOrEmpty(item.Email))
                                {
                                    adminMembershipChangeLayer.UpdateProcess(item.AdminId, false);
                                    continue;
                                }

                                // Kiểm tra số điện thoại này đã tồn tại chưa
                                if (!string.IsNullOrEmpty(item.Phonenumber) && customerLayer.IsExistedPhonenumber(customerInfo.Id, item.Phonenumber))
                                {
                                    adminMembershipChangeLayer.UpdateProcess(item.AdminId, false);
                                    continue;
                                }
                                // Kiểm tra số điện thoại này đã tồn tại chưa
                                if (!string.IsNullOrEmpty(item.Email) && customerLayer.IsExistedEmail(customerInfo.Id, item.Email))
                                {
                                    adminMembershipChangeLayer.UpdateProcess(item.AdminId, false);
                                    continue;
                                }
                                customerLayer.Update(customerInfo.Id, item.FullName, item.Email, item.Phonenumber, item.Address,"System",DateTime.Now);
                                foreach(var changeItem in lstChange)
                                {
                                    customerHistoryLayer.Create(customerInfo.Id, changeItem.Action, changeItem.OldValue, changeItem.NewValue, DateTime.Now, "System");
                                }
                            }
                            // Cập nhật trạng thái của thay đổi này là đã xử lý
                            adminMembershipChangeLayer.UpdateProcess(item.AdminId, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.GetInstance().Write(ex);
            }
            finally
            {
                Logger.GetInstance().Write(string.Format("End: {0:yyyy/MM/dd HH:mm:ss:FFF}", DateTime.Now));
                MemoryManagement.FlushMemory();
            }
        }

        public List<CustomerHistoryModel> GetChangedCustomerInfo(CustomerModel oldCustomerInfo, CustomerModel newCustomerInfo)
        {
            var lstChanged = new List<CustomerHistoryModel>();
            // Kiểm tra có thay đổi tên khách hàng hay không
            if (!oldCustomerInfo.FullName.Equals(newCustomerInfo.FullName))
            {
                lstChanged.Add(new CustomerHistoryModel()
                {
                    Action = "Fullname",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.FullName,
                    NewValue = newCustomerInfo.FullName,
                    UpdatedBy = "System",
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            // Kiểm tra có thay đổi số điện thoại hay không
            if (!oldCustomerInfo.PhoneNumber.Equals(newCustomerInfo.PhoneNumber))
            {
                lstChanged.Add(new CustomerHistoryModel()
                {
                    Action = "Phone number",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.PhoneNumber,
                    NewValue = newCustomerInfo.PhoneNumber,
                    UpdatedBy = "System",
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            // Kiểm tra có thay đổi email hay không
            if (!oldCustomerInfo.Email.Equals(newCustomerInfo.Email))
            {
                lstChanged.Add(new CustomerHistoryModel()
                {
                    Action = "Email",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.Email,
                    NewValue = newCustomerInfo.Email,
                    UpdatedBy = "System",
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            // Kiểm tra có thay đổi thông tin địa chỉ hay không
            if (!oldCustomerInfo.Address.Equals(newCustomerInfo.Address))
            {
                lstChanged.Add(new CustomerHistoryModel()
                {
                    Action = "Address",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.Address,
                    NewValue = newCustomerInfo.Address,
                    UpdatedBy = "System",
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            return lstChanged;
        }

    }
}
