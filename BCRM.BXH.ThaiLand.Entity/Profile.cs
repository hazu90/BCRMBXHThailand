﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.ThaiLand.Entity
{
    public class Profile
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneAlias { get; set; }
        public string Name { get; set; }
        public int PositionType { get; set; }
        public int CustomerId { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
