﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.ThaiLand.Entity
{
    public class UserRoleGroup
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public int GroupId { get; set; }
    }
}
