﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.ThaiLand.Entity
{
    public class Users
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DisplayName { get; set; }
        public string Mobile { get; set; }
        public DateTime DOB { get; set; }
        public string Email { get; set; }
        public int GroupId { get; set; }
        public string Avatar { get; set; }
        public int AdminUserId { get; set; }
        public string AdminUserName { get; set; }
        public int Status { get; set; }
        public DateTime LastActivityDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
