﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.ThaiLand.Entity
{
    public class CustomerCareHistory
    {
        public int Id { get; set; }
        public long AdminId { get; set; }
        public long CrawlerAdminId { get; set; }
        public string ReceivedBy { get; set; }
        public DateTime ReceivedDate { get; set; }
        public int PhoningStatus { get; set; }
        public string Note { get; set; }
        public DateTime? SuccessContactDate { get; set; }
        public int UserId { get; set; }
        public DateTime? WaitingOverReject { get; set; }
        public int CustomerId { get; set; }
        public bool IsReturned { get; set; }
    }
}
