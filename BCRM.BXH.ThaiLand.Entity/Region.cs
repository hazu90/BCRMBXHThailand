﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.ThaiLand.Entity
{
    public class Region
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameAlias { get; set; }
        public string NameEnglish { get; set; }
    }
}
