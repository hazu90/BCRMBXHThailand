﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.ThaiLand.Entity
{
    public class CrawlerCustomer
    {
        public int Id { get; set; }
        public int AdminId { get; set; }
        public string ReceivedBy { get; set; }
        public DateTime ReceiveDate { get; set; }
        public int PhoneStatus { get; set; }
        public DateTime RejectDeadline { get; set; }
        public DateTime ContactDeadline { get; set; }
    }
}
