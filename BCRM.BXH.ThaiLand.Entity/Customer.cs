﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.ThaiLand.Entity
{
    public class Customer
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public int RegionId { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public string AssignTo { get; set; }
        public int GroupId { get; set; }
        public DateTime CareDate { get; set; }
        public int ICareDate { get; set; }
        public DateTime StartCareDate { get; set; }
        public int CompanyId { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string Address { get; set; }
        public bool IsBlacklist { get; set; }
        public string Note { get; set; }
    }
}
