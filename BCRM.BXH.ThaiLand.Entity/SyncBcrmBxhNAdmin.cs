﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.ThaiLand.Entity
{
    public class SyncBcrmBxhNAdmin
    {
        public int CustomerId { get; set; }
        public long AccountId { get; set; }
        public string   Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
