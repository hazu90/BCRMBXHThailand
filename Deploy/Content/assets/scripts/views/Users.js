﻿var user_index={
    init: function (groups_array) {
        user_index.build_create_view(groups_array);
        user_index.build_change_passwor();

        user_index.build_set_permission([{Value:'',Text:'--Choose all groups--'}].concat(groups_array) );
        app.initPortletTools($('#wapListUser'));
        app.initPortletTools($('#divUserForCreate'));
        app.initPortletTools($('#divChangePassword'));
        app.initPortletTools($('#divSetPermission'));
        app.initPortletTools($('#divListPermissions'));
        app.initDateTimePickers($('#divUserForCreate'));
        app.initUniform($('#divUserForCreate'));
        user_index.action();
        user_list.action();
    },
    action: function () {
        $('#btnAddUser',$('#divbodyUser')).off('click').on('click', function () {
            $('#divUserForCreate').removeClass('hide');
            $('#lnkRemoveListUser').trigger('click');
            $('#txtPasswordCreate').parent().parent().parent().removeClass('hide');
            $('.caption',$('#divUserForCreate')).html('<i class="icon-plus"></i>Create a new user');
            $('#txtUsernameCreate',$('#divUserForCreate')).removeAttr('disabled');
            $('#btnUserCreate',$('#divUserForCreate')).data('id',0);
            user_index.clear_create_view();
        });
        $('#btnResetCreate',$('#divUserForCreate')).off('click').on('click', function () {
           user_index.clear_create_view();
        });
        $('#btnUserCreate',$('#divUserForCreate')).off('click').on('click', function () {
            var data = $(this).data();
            var form_create = $('#divUserForCreate');
            syscommon.remove_valid_message(form_create);
            var valid = true;
            var txtUsernameCreate = $('#txtUsernameCreate',form_create );
            if(txtUsernameCreate.val() == null || $.trim(txtUsernameCreate.val()) == '' ){
                syscommon.show_valid_message(txtUsernameCreate, "You must text user name.");
                valid = false;
            }
            else{
                if($.trim(txtUsernameCreate.val()).indexOf(' ') > -1 ){
                    syscommon.show_valid_message(txtUsernameCreate, "User name does not have whitespace .");
                    valid = false;
                }
            }

            if(data.id ==0){
                var txtPasswordCreate = $('#txtPasswordCreate',form_create);
                if(txtPasswordCreate.val() == null || $.trim(txtPasswordCreate.val()) == '' ){
                    syscommon.show_valid_message(txtPasswordCreate, "You must text password.");
                    valid = false;
                }
            }

            var txtEmailCreate = $('#txtEmailCreate',form_create);
            if(txtEmailCreate.val() == null || $.trim(txtEmailCreate.val()) == '' ){
                syscommon.show_valid_message(txtEmailCreate, "You must text email.");
                valid = false;
            }
            else{
                if(!sysvalid.email($.trim(txtEmailCreate.val())) ){
                    syscommon.show_valid_message(txtEmailCreate, "Invalid Email Address.");
                    valid = false;
                }
            }

            var dllGroupIdCreate = $('#dllGroupIdCreate',form_create);
            if(dllGroupIdCreate.val() == null || dllGroupIdCreate.val()==''){
                syscommon.show_valid_message(dllGroupIdCreate, "You must select a group.");
                valid = false;
            }

            if(valid){
                var option = {
                    controller:'User',
                    action:'Save',
                    data:{
                        UserName : $('#txtUsernameCreate',form_create).val(),
                        Password : $('#txtPasswordCreate',form_create).val(),
                        DisplayName : $('#txtDisplayNameCreate',form_create).val(),
                        Email:$('#txtEmailCreate',form_create).val(),
                        GroupId:$('#dllGroupIdCreate',form_create).val(),
                        DOB:sysformat.date($('#txtDOBCreate',form_create).val()),
                        AdminUserName : $('#txtAdminUsernameCreate',form_create).val(),
                        Id:data.id
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.info(response.Message);
                            $('#btnUserCloseCreate',$('#divUserForCreate')).trigger('click');
                            user_index.search();
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });
        $('#btnUserCloseCreate',$('#divUserForCreate')).off('click').on('click', function () {
            $('#lnkRemoveCreateUser').trigger('click');
        });
        $('#btnCloseChangePassword',$('#divChangePassword')).off('click').on('click', function () {
            $('#lnkRemoveChangePassword',$('#divChangePassword')).trigger('click');
        });
        $('#btnChangePassword',$('#divChangePassword')).off('click').on('click', function () {
            var data = $(this).data();
            var form = $('#divChangePassword');
            var valid = true;
            if ($("#txtNewPass", form).val() == null || $("#txtNewPass", form).val() == "") {
                syscommon.show_valid_message($("#txtNewPass", form), "You must text new password.");
                valid = false;
            }
            if ($("#txtConfirmPass", form).val() == null || $("#txtConfirmPass", form).val() == "") {
                syscommon.show_valid_message($("#txtConfirmPass", form), "You must text confirm password.");
                valid = false;
            }
            if ($("#txtConfirmPass", form).val() != $("#txtNewPass", form).val()) {
                syscommon.show_valid_message($("#txtConfirmPass", form), "Confirm password is not valid.");
                valid = false;
            }

            if(valid){
                var option = {
                    controller:'User',
                    action:'ChangePassword',
                    data:{
                        UserId : data.userid,
                        NewPassword :$("#txtNewPass", form).val(),
                        ConfirmPassword :$("#txtConfirmPass", form).val()
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.info(response.Message);
                            $('#lnkRemoveChangePassword').trigger('click');
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });
        $('#btnAddPermission',$('#divListPermissions')).off('click').on('click', function () {
            $('#divListPermissions').addClass('hide');
            $('#divSetPermission').removeClass('hide');
        });
        $('#btnRoleCreate',$('#divSetPermission')).off('click').on('click', function () {
            var data = $(this).data();
            var grId = $('#dllGroupIdPermission',$('#divSetPermission')).val();
            if(grId ==null  || grId ==''){
                grId = 0;
            }
            var option = {
                controller:'UserRoleGroup',
                action:'CreateAction',
                data:{
                    userId:data.id,
                    roleId:$('#dllRoleIdPermission',$('#divSetPermission')).val(),
                    groupId:grId
                },
                callback: function (response) {
                    if(response.Code == ResponseCode.Success){
                        $('#lnkRemoveSetPermission').trigger('click');
                        var option = {
                            controller:'UserRoleGroup',
                            action:'Search',
                            data:{
                                userId:$('#btnRoleCreate',$('#divSetPermission')).data().id
                            },
                            dataType:'html',
                            element:$('#divListUserRoleGroup'),
                            callback: function () {

                            }
                        };
                        sysrequest.send(option);
                        sysmess.info(response.Message);
                    }
                    else if(response.Code == ResponseCode.Error){
                        sysmess.error(response.Message);
                    }
                    else{
                        sysmess.warning(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        });
        $('#btnRoleCloseCreate',$('#divSetPermission')).off('click').on('click', function () {
            $('#lnkRemoveSetPermission',$('#divSetPermission')).trigger('click');
        });
        $('#btnCloseUserRoleGroup',$('#divListPermissions')).off('click').on('click', function () {
            $('#lnkRemoveListPermission').trigger('click');
        });
    },
    build_create_view: function (groups_array) {
            var html= syshtml.raw({
                Type:'form',
                Head:{
                    Title:'Create/Edit user',
                    Icon:'icon-plus',
                    RemoveId:'lnkRemoveCreateUser',
                    Relation: 'btnAddUser'
                },
                Body:[
                    [
                        {Type:'ui',Class:'span11 hide',UI:{Type:'label',Id:'lbEmpty'},Required:true },
                        {Type:'ui',Class:'span6',UI:{Type:'text',Id:'txtUsernameCreate',Title:'Username',Class:'m-wrap span12'},Required:true},
                        {Type:'html',Class:'span5',UI:{Html:'<div class="control-group"><label class="control-label" for="txtPasswordCreate">Password<span class="required">*</span>   </label>   <div class="controls"><input type="password" id="txtPasswordCreate" class="m-wrap span12" value="">   </div></div>'},Required:true},
                        {Type:'ui',Class:'span6',UI:{Type:'text',Id:'txtDisplayNameCreate',Title:'Display name',Class:'m-wrap span12'},Required:false},
                        {Type:'ui',Class:'span5',UI:{Type:'text',Id:'txtEmailCreate',Title:'Email',Class:'m-wrap span12'},Required:true},
                        {Type:'ui',Class:'span6',UI:{Type:'select',Option:groups_array,  Id:'dllGroupIdCreate',Title:'Groups',Class:'m-wrap span12'},Required:true},
                        {Type:'ui',Class:'span5',UI:{Type:'text',Id:'txtDOBCreate',Title:'Date of birth',Class:'m-wrap span12',AddOn:{Class:'date-picker',Item:[{Title:'Clear',Class:'icon-remove'},{Title:'Calendar',Class:'icon-calendar'}]}},Required:false},
                        {Type:'ui',Class:'span6 hide',UI:{Type:'text',Id:'txtAdminUsernameCreate',Title:'Admin Account',Class:'m-wrap span12 hide'},Required:false}
                        //,
                        //{Type:'html',Class:'span6',UI:{Html:html_lst_role}}
                    ]
                ],
                Button:[
                    { Id: "btnResetCreate", Class: "btn blue", Value: 0, Icon: "icon-refresh", Text: "Refresh", Attribute: [{ Name: "title", Value: "Reset to default"}] },
                    { Id: "btnUserCreate", Class: "btn blue", Value: 0, Icon: "icon-save", Text: "Save", Attribute: [{ Name: "title", Value: "Add / Edit a user"},{ Name: "data-id", Value: "0"}] },
                    { Id: "btnUserCloseCreate", Class: "btn blue", Value: 0, Icon: "icon-remove", Text: "Close", Attribute: [{ Name: "title", Value: "close create"}] }
                ]
            });
        $(html).appendTo($('#divUserForCreate'));
    },
    build_change_passwor: function () {
        var html= syshtml.raw({
            Type:'form',
            Head:{
                Title:'Change password',
                Icon:'icon-plus',
                RemoveId:'lnkRemoveChangePassword',
                Relation: 'btnAddUser'
            },
            Body:[
                [
                    {Type:'ui',Class:'span11 hide',UI:{Type:'label',Id:'lbEmpty'},Required:true },
                    {Type:'html',Class:'span6',UI:{Html:'<div class="control-group"><label class="control-label" for="txtNewPass">New Password<span class="required">*</span>   </label>   <div class="controls"><input type="password" id="txtNewPass" class="m-wrap span12" value="">   </div></div>'},Required:true},
                    {Type:'html',Class:'span6',UI:{Html:'<div class="control-group"><label class="control-label" for="txtConfirmPass">Confirm Password<span class="required">*</span>   </label>   <div class="controls"><input type="password" id="txtConfirmPass" class="m-wrap span12" value="">   </div></div>'},Required:true}
                ]
            ],
            Button:[
                { Id: "btnChangePassword", Class: "btn blue", Value: 0, Icon: "icon-save", Text: "Change", Attribute: [{ Name: "title", Value: "Change a new password"},{ Name: "data-userid", Value: 0}] },
                { Id: "btnCloseChangePassword", Class: "btn blue", Value: 0, Icon: "icon-remove", Text: "Close" }
            ]
        });
        $(html).appendTo($('#divChangePassword'));
    },
    build_set_permission: function (groups_array) {
        var roles_array=[
                            {Value:6,Text:'Sale'}
                           ,{Value:7,Text:'Manager'}
                           ,{Value:8,Text:'Admin'}
                           //,{Value:9,Text:'Coordinate'}
                           //,{Value:10,Text:'TeamLeader'}
                        ];
        var html= syshtml.raw({
            Type:'form',
            Head:{
                Title:'Set Permission',
                Icon:'icon-plus',
                RemoveId:'lnkRemoveSetPermission',
                Relation: 'btnAddPermission'
            },
            Body:[
                [
                    {Type:'ui',Class:'span11 hide',UI:{Type:'label',Id:'lbEmpty'},Required:true },
                    {Type:'ui',Class:'span6',UI:{Type:'select',Option:groups_array,  Id:'dllGroupIdPermission',Title:'Groups',Class:'m-wrap span12'},Required:true},
                    {Type:'ui',Class:'span6',UI:{Type:'select',Option:roles_array,  Id:'dllRoleIdPermission',Title:'Role',Class:'m-wrap span12'},Required:true}
                ]
            ],
            Button:[
                { Id: "btnRoleCreate", Class: "btn blue", Value: 0, Icon: "icon-save", Text: "Add role", Attribute: [{ Name: "title", Value: "Add new a role"},{ Name: "data-id", Value: "0"}] },
                { Id: "btnRoleCloseCreate", Class: "btn blue", Value: 0, Icon: "icon-remove", Text: "Close", Attribute: [{ Name: "title", Value: "close create"}] }
            ]
        });
        $(html).appendTo($('#divSetPermission'));
    },
    search: function () {
        var option = {
            controller:'User',
            action:'Search',
            data:{},
            dataType:'html',
            callback: function () {
                user_list.action();
            },
            element:$('#divListUser')
        };
        sysrequest.send(option);
    },
    clear_create_view: function () {
        var div_create = $('#divUserForCreate');
        if($('#btnUserCreate',$('#divUserForCreate')).data('id') ==''
          || $('#btnUserCreate',$('#divUserForCreate')).data('id') =='0'){
            $('#txtUsernameCreate',div_create).val('');
        }
        $('#txtPasswordCreate',div_create).val('');
        $('#txtDisplayNameCreate',div_create).val('');
        $('#txtEmailCreate',div_create).val('');
        $('#txtDOBCreate',div_create).val('');
        $('#txtAdminUsernameCreate',div_create).val('');
    }
};
var user_list={
    action: function () {
        $('a[name=btnBlock]',$('#divListUser')).each(function () {
            $(this).off('click').on('click', function () {
                var data = $(this).data();
                var option = {
                    controller:'User',
                    action:'Block',
                    data:{
                        id:data.userid
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.info(response.Message);
                            user_index.search();
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysmess.confirm('Do you want to block this user ?', function () {
                    sysrequest.send(option);
                })
            });
        });
        $('a[name=btnUnblock]',$('#divListUser')).each(function () {
            $(this).off('click').on('click', function () {
                var data = $(this).data();
                var option = {
                    controller:'User',
                    action:'Unblock',
                    data:{
                        id:data.userid
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.info(response.Message);
                            user_index.search();
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysmess.confirm('Do you want to unblock this user ?', function () {
                    sysrequest.send(option);
                });

            });
        });
        $('a[name=btnChangePassword]',$('#divListUser')).each(function () {
            $(this).off('click').on('click', function () {
                $('#divChangePassword').removeClass('hide');
                $('#lnkRemoveListUser').trigger('click');
                var data = $(this).data();
                $('#btnChangePassword').data('userid',data.userid);
            });
        });
        $('a[name=btnEditUser]',$('#divListUser')).each(function () {
            $(this).off('click').on('click', function () {
                var data = $(this).data();
                var option = {
                    controller:'User',
                    action:'GetById',
                    data:{
                        userId:data.userid
                    },
                    callback: function (response) {
                        if(response.Data != null){
                            $('#btnAddUser').trigger('click');
                            $('#lnkRemoveListUser').trigger('click');
                            var form_create = $('#divUserForCreate');
                            var user_obj = response.Data;
                            $('.caption',$('#divUserForCreate')).html('<i class="icon-edit"></i>Edit user ('+user_obj.UserName +')');
                            $('#txtUsernameCreate',form_create).val(user_obj.UserName);
                            $('#txtUsernameCreate',form_create).attr('disabled','disabled');
                            $('#txtPasswordCreate',form_create).parent().parent().parent().addClass('hide');
                            $('#txtDisplayNameCreate',form_create).val(user_obj.DisplayName);
                            $('#txtEmailCreate',form_create).val(user_obj.Email);
                            $('#dllGroupIdCreate',form_create).val(user_obj.GroupId);
                            if(sysformat.date(systimer.date(user_obj.DOB), 'dd/mm/yyyy') =='01/01/1'){
                                $('#txtDOBCreate',form_create).val('' );
                            }
                            else{
                                $('#txtDOBCreate',form_create).val(sysformat.date(systimer.date(user_obj.DOB), 'dd/mm/yyyy') );
                            }
                            $('#txtAdminUsernameCreate',form_create).val(user_obj.AdminUserName);
                            $('#btnUserCreate',form_create).data('id',user_obj.Id);
                        }
                    }
                };
                sysrequest.send(option);
            });
        });
        $('a[name=btnSetPermission]',$('#divListUser')).each(function () {
            $(this).off('click').on('click', function () {
                $('#divListPermissions').removeClass('hide');
                $('#lnkRemoveListUser').trigger('click');
                var data = $(this).data();
                $('#btnRoleCreate').data('id',data.userid);
                $('.caption',$('#divListPermissions')).html('<i class="icon-reorder"></i>List of Permission (' + data.username +')' );
                $('.caption',$('#divSetPermission')).html('<i class="icon-reorder"></i>Set Permission (' + data.username +')' );
                user_list.permissions_search();
            });
        });
    },
    permissions_action: function () {
        $('a[name=btnRemove]',$('#divListUserRoleGroup')).each(function () {
            $(this).off('click').on('click', function () {
                var data = $(this).data();
                var userId = data.userid;
                var groupId = data.groupid;
                var roleId = data.roleid;
                sysmess.confirm('Do you want to remove this role', function () {
                    var option = {
                        controller:'UserRoleGroup',
                        action:'Delete',
                        data:{
                            userId:userId,
                            groupId:groupId,
                            roleId:roleId
                        },
                        callback: function (response) {
                            if(response.Code == ResponseCode.Success){
                                sysmess.info(response.Message);
                                user_list.permissions_search();
                            }
                            else if(response.Code == ResponseCode.Error){
                                sysmess.error(response.Message);
                            }
                            else{
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                });
            });
        });
    },
    permissions_search: function () {
        var userId = $('#btnRoleCreate').data('id');
        var option = {
            controller:'UserRoleGroup',
            action:'Search',
            data:{
                userId:userId
            },
            dataType:'html',
            element:$('#divListUserRoleGroup'),
            callback: function () {
                user_list.permissions_action();
            }
        };
        sysrequest.send(option);
    }
};