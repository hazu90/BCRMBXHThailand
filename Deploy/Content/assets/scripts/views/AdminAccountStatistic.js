﻿var admin_account_statistic_index = {
    init: function () {
        $(document).ready(function () {
            admin_account_statistic_index.action();
            admin_account_statistic_index.search();
        });
    },
    search : function () {
        var frm_search = $('#frmSearchAdminAccountStatistic');
        var dllGroupId = $('select[name=GroupId]',frm_search);
        var groupId = dllGroupId.val();
        var dllAssignTo = $('select[name=AssignTo]',frm_search);
        var txtTimeToCreationFrom = $('input[name=TimeToCreationFrom]',frm_search);
        var txtTimeToCreationTo = $('input[name=TimeToCreationTo]',frm_search);

        if(txtTimeToCreationFrom.val() == null || txtTimeToCreationFrom.val() ==''){
            sysmess.warning('You must enter both of creation date !');
            return;
        }

        if(txtTimeToCreationTo.val() == null || txtTimeToCreationTo.val() ==''){
            sysmess.warning('You must enter both of creation date !');
            return;
        }

        var option ={
            controller : 'AdminAccountStatistic',
            action:'Search',
            data:{
                GroupId : dllGroupId.val() ,
                AssignTo : dllAssignTo.val(),
                TimeToCreationFrom : sysformat.date(txtTimeToCreationFrom.val()),
                TimeToCreationTo : sysformat.date(txtTimeToCreationTo.val())
            },
            dataType:'html',
            element:$('#divListAdminAccountStatistic'),
            callback: function () {
            }
        };
        sysrequest.send(option);
    },
    get_user_by_groupid: function () {
        var frm_search = $('#frmSearchAdminAccountStatistic');
        var groupId =$('select[name=GroupId]',frm_search).val();
        var option = {
            controller:'AdminAccountStatistic',
            action : 'GetUserByGroupId',
            data:{
                groupId : groupId
            },
            callback: function (response) {
                var dllAssignTo = $('select[name=AssignTo]',frm_search);
                var html = '';
                if( response.Code == ResponseCode.Success ){
                    if(response.Data != null && response.Data.length > 0   ){
                        for(var index = 0 ;index < response.Data.length ; index++ ){
                            html += '<option value="'+ response.Data[index].Key +'">' + response.Data[index].Value +'</option>'
                        }
                    }
                    dllAssignTo.html(html);
                }
                else{
                    dllAssignTo.html(html);
                }
                dllAssignTo.trigger('liszt:updated');
            }
        };
        sysrequest.send(option);
    },
    action: function () {
        var frm_search = $('#frmSearchAdminAccountStatistic');
        $('select[name=GroupId]',frm_search).off('change').on('change', function () {
            admin_account_statistic_index.get_user_by_groupid();
        });
        $('button[name=search]',frm_search).off('click').on('click', function () {
            admin_account_statistic_index.search(1);
        });
        $('button[name=reset]',frm_search).off('click').on('click', function () {
            $('select[name=GroupId]',frm_search).val($('select[name=GroupId] option:first',frm_search).val());
            admin_account_statistic_index.get_user_by_groupid();
            $('input[name=TimeToCreationFrom]',frm_search).val('');
            $('input[name=TimeToCreationTo]',frm_search).val('');
        });
    }
};