﻿var group_index = {
    init: function (leaderArray) {
        var lst_choice_leader=[{Value:'',Text:'--Choose a leader --'}].concat(leaderArray);
        group_index.build_create_vew(lst_choice_leader);
        app.initChoosenSelect($('#divGroupForCreate'));
        $('#ddlLeaderCreate_chzn',$('#divGroupForCreate')).addClass('span10');
        $('#ddlLeaderCreate_chzn',$('#divGroupForCreate')).attr('style','');
        $('#ddlLeaderCreate_chzn .chzn-drop',$('#divGroupForCreate')).addClass('span12');
        $('#ddlLeaderCreate_chzn .chzn-drop',$('#divGroupForCreate')).attr('style','left: -9000px; top: 33px;');
        $('#ddlLeaderCreate_chzn .chzn-drop input',$('#divGroupForCreate')).addClass('span12');
        $('#ddlLeaderCreate_chzn .chzn-drop input',$('#divGroupForCreate')).attr('style','');

        group_index.build_edit_view(lst_choice_leader);
        app.initChoosenSelect($('#divGroupForEdit'));
        $('#ddlLeaderEdit_chzn',$('#divGroupForEdit')).addClass('span10');
        $('#ddlLeaderEdit_chzn',$('#divGroupForEdit')).attr('style','');
        $('#ddlLeaderEdit_chzn .chzn-drop',$('#divGroupForEdit')).addClass('span12');
        $('#ddlLeaderEdit_chzn .chzn-drop',$('#divGroupForEdit')).attr('style','left: -9000px; top: 33px;');
        $('#ddlLeaderEdit_chzn .chzn-drop input',$('#divGroupForEdit')).addClass('span12');
        $('#ddlLeaderEdit_chzn .chzn-drop input',$('#divGroupForEdit')).attr('style','');

        group_index.action();
        group_list.action();
    },
    action: function () {
        $('#btnAddGroup',$('#wapListGroup')).off('click').on('click', function () {
            syscommon.remove_valid_message();
            group_index.clear_create_view();
        });
        var div_create_group = $('#divGroupForCreate');
        $('#btnCreateGroup',div_create_group).off('click').on('click', function () {
            syscommon.remove_valid_message();
            var valid = true;
            var txtGroupName = $("#txtGroupNameCreate", div_create_group);
            if (txtGroupName.val() == null || $.trim(txtGroupName.val()) == "") {
                syscommon.show_valid_message(txtGroupName, "You must text group name.");
                valid = false;
            }

            var txtGroupAlias = $("#txtGroupAliasCreate", div_create_group);
            if (txtGroupAlias.val() == null || $.trim(txtGroupAlias.val()) == "") {
                syscommon.show_valid_message(txtGroupAlias, "You must text group alias.");
                valid = false;
            }
            if(valid)
            {
                var option = {
                    controller:'Group',
                    action:'CreateAction',
                    data:{
                        Name:$('#txtGroupNameCreate',div_create_group).val(),
                        Leader:$('#ddlLeaderCreate',div_create_group).val(),
                        Description:$('#txtDescriptionCreate',div_create_group).val(),
                        Priority: $('#txtPriorityCreate',div_create_group).val(),
                        Alias:$('#txtGroupAliasCreate',div_create_group).val(),
                        ParentId:0
                    },
                    blockUI:div_create_group,
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success)
                        {
                            sysmess.info(response.Message);
                            group_index.search();
                        }
                        else if(response.Code == ResponseCode.Error)
                        {
                            sysmess.error(response.Message);
                        }
                        else
                        {
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });
        $('#btnResetCreate',div_create_group).off('click').on('click', function () {
            group_index.clear_create_view();
        });
        var div_edit_group = $('#divGroupForEdit');
        $('#btnSaveGroup',div_edit_group).off('click').on('click', function () {
            var data = $(this).data();
            syscommon.remove_valid_message();
            var valid = true;
            var txtGroupName = $("#txtGroupNameEdit", div_edit_group);
            if (txtGroupName.val() == null || $.trim(txtGroupName.val()) == "") {
                syscommon.show_valid_message(txtGroupName, "You must text group name.");
                valid = false;
            }

            var txtGroupAlias = $("#txtGroupAliasEdit", div_edit_group);
            if (txtGroupAlias.val() == null || $.trim(txtGroupAlias.val()) == "") {
                syscommon.show_valid_message(txtGroupAlias, "You must text group alias.");
                valid = false;
            }
            if(valid){
                var option = {
                    controller:'Group',
                    action:'EditAction',
                    data:{
                        Id:data.id,
                        Name:$('#txtGroupNameEdit',div_edit_group).val(),
                        Leader:$('#ddlLeaderEdit',div_edit_group).val(),
                        Description:$('#txtDescriptionEdit',div_edit_group).val(),
                        Priority: $('#txtPriorityEdit',div_edit_group).val(),
                        Alias:$('#txtGroupAliasEdit',div_edit_group).val(),
                        ParentId:0
                    },
                    blockUI:div_edit_group,
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.info(response.Message);
                            group_index.search();
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });
    },
    clear_create_view: function () {
        var div_create_group = $('#divGroupForCreate');
        $('#txtGroupNameCreate',div_create_group).val('');
        $('#txtGroupAliasCreate',div_create_group).val('');
        $('#ddlLeaderCreate',div_create_group).val('');
        $('#txtDescriptionCreate',div_create_group).val('');
        $('#txtPriorityCreate',div_create_group).val('');
    },
    build_create_vew: function (leaderArray) {
        var html= syshtml.raw({
            Type:'modal',
            Head:{Title:'Create group' },
            Box:{
                Class:'overflow-visible'
            },
            Body:[
                [
                    {Type:'ui',Class:'span11 hide',UI:{Type:'label',Id:'lbEmpty'},Required:true },
                    {Type:'ui',Class:'span5',UI:{Type:'text',Id:'txtGroupNameCreate',Title:'Group name',Class:'m-wrap span10'},Required:true},
                    {Type:'ui',Class:'span5',UI:{Type:'text',Id:'txtGroupAliasCreate',Title:'Group Alias',Class:'m-wrap span10'},Required:true},
                    {Type:'ui',Class:'span5',UI:{Type:'select',Option:leaderArray,Id:'ddlLeaderCreate',Title:'Leader',Class:'m-wrap span10 chosen'},Required:false},
                    {Type:'html',Class:'span5',UI:{Html:'<div class="control-group">   <label class="control-label" for="txtPriorityCreate">   Priority   </label>   <div class="controls"><input type="number" id="txtPriorityCreate" class="m-wrap span10" value="">   </div></div>'}},
                    {Type:'ui',Class:'span10',UI:{Type:'textarea',Id:'txtDescriptionCreate',Title:'Description',Class:'m-wrap span12'},Required:false}
                    //{Type:'ui',Class:'span11',UI:{Type:'text',Id:'txtPriorityCreate',Title:'Priority',Class:'m-wrap span10'},Required:false}

                ]
            ],
            Button:[
                { Id: "btnResetCreate", Class: "btn blue", Value: 0, Icon: "icon-refresh", Text: "Refresh", Attribute: [{ Name: "title", Value: "Reset to default"}] },
                { Id: "btnCreateGroup", Class: "btn blue", Value: 0, Icon: "icon-save", Text: "Add new", Attribute: [{ Name: "title", Value: "Add new a group"},{ Name: "data-id", Value: "0"}] }
            ]
        });
        $(html).appendTo($('#divGroupForCreate'));
    },
    build_edit_view: function (leaderArray) {
        var html= syshtml.raw({
            Type:'modal',
            Head:{Title:'Edit group' },
            Box:{
                Class:'overflow-visible'
            },
            Body:[
                [
                    {Type:'ui',Class:'span11 hide',UI:{Type:'label',Id:'lbEmpty'},Required:true },
                    {Type:'ui',Class:'span5',UI:{Type:'text',Id:'txtGroupNameEdit',Title:'Group name',Class:'m-wrap span10'},Required:true},
                    {Type:'ui',Class:'span5',UI:{Type:'text',Id:'txtGroupAliasEdit',Title:'Group Alias',Class:'m-wrap span10'},Required:true},
                    {Type:'ui',Class:'span5',UI:{Type:'select',Option:leaderArray,Id:'ddlLeaderEdit',Title:'Leader',Class:'m-wrap span10 chosen'},Required:false},
                    {Type:'html',Class:'span5',UI:{Html:'<div class="control-group">   <label class="control-label" for="txtPriorityEdit">   Priority   </label>   <div class="controls"><input type="number" id="txtPriorityEdit" class="m-wrap span10" value="">   </div></div>'}},
                    {Type:'ui',Class:'span10',UI:{Type:'textarea',Id:'txtDescriptionEdit',Title:'Description',Class:'m-wrap span12'},Required:false}
                    //{Type:'ui',Class:'span11',UI:{Type:'text',Id:'txtPriorityEdit',Title:'Priority',Class:'m-wrap span10'},Required:false}

                ]
            ],
            Button:[
                { Id: "btnResetEdit", Class: "btn blue", Value: 0, Icon: "icon-refresh", Text: "Refresh", Attribute: [{ Name: "title", Value: "Reset to default"}] },
                { Id: "btnSaveGroup", Class: "btn blue", Value: 0, Icon: "icon-save", Text: "Save", Attribute: [{ Name: "title", Value: "Edit info of group"},{ Name: "data-id", Value: "0"}] }
            ]
        });
        $(html).appendTo($('#divGroupForEdit'));
    },
    search: function () {
        var option={
            controller:'Group',
            action:'Search',
            data:{},
            dataType:'html',
            callback: function () {
                group_list.action();
            },
            element:$('#divListGroup')
        };
        sysrequest.send(option);
    }
};
var group_list={
    action:function(){
        var divListGroup = $('#divListGroup');
        $('a[name=btnEditGroup]',divListGroup ).each(function () {
            $(this).off('click').on('click', function () {
                var data = $(this).data();
                var option = {
                    controller:'Group',
                    action:'Edit',
                    data:{
                        id:data.gid
                    },
                    callback: function (response) {
                        if(response.Data != null){
                            var divGroupForEdit = $('#divGroupForEdit');
                            $('#txtGroupNameEdit',divGroupForEdit).val(response.Data.Name);
                            $('#txtGroupAliasEdit',divGroupForEdit).val(response.Data.Alias);
                            $('#ddlLeaderEdit',divGroupForEdit).val(response.Data.Leader);
                            $('#ddlLeaderEdit',divGroupForEdit).trigger('liszt:updated');
                            $('#txtDescriptionEdit',divGroupForEdit).val(response.Data.Description);
                            $('#txtPriorityEdit',divGroupForEdit).val(response.Data.Priority);
                            $('#btnSaveGroup',divGroupForEdit).data('id',response.Data.Id);
                        }
                    }
                };
                sysrequest.send(option);
            });
        });
    }
}