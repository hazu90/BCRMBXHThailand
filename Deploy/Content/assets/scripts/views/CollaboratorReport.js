﻿var collaborator_index = {
    init: function () {
        app.initPortletTools($('#divSearchSaler'));
        app.initDateTimePickers($('#frmSearchCollaborator'));
        app.initChoosenSelect($('#frmSearchCollaborator'));
        collaborator_index.action();
        collaborator_index.search(1);
    },
    action: function () {
        var frm_search = $('#frmSearchCollaborator');
        $('#btnSearch',frm_search).off('click').on('click', function () {
            collaborator_index.search(1);
        });

        $('#btnReset',frm_search).off('click').on('click', function () {
            $('#txtFilterText',frm_search).val('');
            $('#txtStartDate',frm_search).val('');
            $('#txtEndDate',frm_search).val('');
        });
    },
    search: function (page_index) {
        var frm_search = $('#frmSearchCollaborator');
        var txtStartDate = $('#txtStartDate',frm_search);
        if(txtStartDate.val() == null || txtStartDate.val()=='' ){
            sysmess.warning('You must chose a start date');
            return;
        }

        var txtEndDate = $('#txtEndDate',frm_search);
        if(txtEndDate.val() == null || txtEndDate.val() ==''){
            sysmess.warning('You must chose a end date');
            return;
        }

        var txtFilterText = $('#txtFilterText',frm_search);
        var selGroup = $('#selGroup',frm_search);
        var option = {
            controller:'CollaboratorReport',
            action:'Search',
            data:{
                FilterKeyword:txtFilterText.val(),
                GroupId:selGroup.val(),
                FromDate:sysformat.date($('#txtStartDate',frm_search).val()),
                ToDate:sysformat.date($('#txtEndDate',frm_search).val()),
                Status:$('#selStatus').val(),
                PageIndex:1,
                PageSize:25
            },
            dataType:'html',
            callback: function () {

            },
            element:$('#divListSaler')
        };
        sysrequest.send(option);
    }
}