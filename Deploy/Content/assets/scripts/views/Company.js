﻿var company_index = {
    init: function(lst_city) {
        if($('#wrapCompanyForCreate').length >0){
            company_index.build_create_view(lst_city);
            app.initChoosenSelect($('#wrapCompanyForCreate'));
            $('#dllCityId_chzn',$('#wrapCompanyForCreate')).addClass('span10');
            $('#dllCityId_chzn',$('#wrapCompanyForCreate')).attr('style','');
            $('#dllCityId_chzn .chzn-drop',$('#wrapCompanyForCreate')).addClass('span12');
            $('#dllCityId_chzn .chzn-drop',$('#wrapCompanyForCreate')).attr('style','left: -9000px; top: 33px;');
            $('#dllCityId_chzn .chzn-drop input',$('#wrapCompanyForCreate')).addClass('span12');
            $('#dllCityId_chzn .chzn-drop input',$('#wrapCompanyForCreate')).attr('style','');

            $('#dllDistrictId_chzn',$('#wrapCompanyForCreate')).addClass('span10');
            $('#dllDistrictId_chzn',$('#wrapCompanyForCreate')).attr('style','');
            $('#dllDistrictId_chzn .chzn-drop',$('#wrapCompanyForCreate')).addClass('span12');
            $('#dllDistrictId_chzn .chzn-drop',$('#wrapCompanyForCreate')).attr('style','left: -9000px; top: 33px;');
            $('#dllDistrictId_chzn .chzn-drop input',$('#wrapCompanyForCreate')).addClass('span12');
            $('#dllDistrictId_chzn .chzn-drop input',$('#wrapCompanyForCreate')).attr('style','');

            company_index.create_action_get_district_by_city_id();
        }
        company_index.action();
        company_index.search(1);
    },
    clear_error: function (frm) {
        $("span.help-inline", frm).each(function () {
            $(this).parent("div").parent("div").removeClass("error");
            $(this).remove();
        });
    },
    action: function () {
        var wrapFilterConditionCompany = $('#wrapFilterConditionCompany');
        $('button[name=search]',wrapFilterConditionCompany).off('click').on('click', function () {
            company_index.search(1);
        });
        $('button[name=reset]',wrapFilterConditionCompany).off('click').on('click', function () {
            $('textarea[name=FilterKeyword]',wrapFilterConditionCompany).val('');
        });

        $('select[name=city_id]',wrapFilterConditionCompany).off('change').on('change', function () {
            company_index.search_get_district_by_city_id();
        });

        var wapListCompany = $('#wapListCompany');
        $('button[name=add_company]',wapListCompany).off('click').on('click', function () {
            company_index.clear_error($('#wrapCompanyForCreate'));
            company_index.create_action_reset();
            $('.modal-header h3',$('#wrapCompanyForCreate')).html('Create company');
            $('#create_company_save',$('#wrapCompanyForCreate')).html('<i class="icon-save"></i>  Add new company');
        });

        // wrap create div
        var wrapCompanyForCreate = $('#wrapCompanyForCreate');
        $('select[name=city_id]',wrapCompanyForCreate).off('change').on('change', function () {
            company_index.create_action_get_district_by_city_id();
        });
        $('#create_company_reset',wrapCompanyForCreate).off('click').on('click', function () {
            company_index.create_action_reset();
        });
        $('#create_company_save',wrapCompanyForCreate).off('click').on('click', function () {
            company_index.create_action_save();
        });
        //$('input[name=phone_number]',wrapCompanyForCreate).off('keydown').on('keydown', function (e) {
        //    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        //            // Allow: Ctrl/cmd+A
        //        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        //            // Allow: Ctrl/cmd+C
        //        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        //            // Allow: Ctrl/cmd+X
        //        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        //            // Allow: home, end, left, right
        //        (e.keyCode >= 35 && e.keyCode <= 39)) {
        //        // let it happen, don't do anything
        //            return;
        //    }
        //    // Ensure that it is a number and stop the keypress
        //    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        //        e.preventDefault();
        //    }
        //});
    },
    search: function (page) {
        var wrapFilterConditionCompany = $('#wrapFilterConditionCompany');
        var filterKeyword = $('textarea[name=FilterKeyword]',wrapFilterConditionCompany).val();
        var cityId = $('select[name=city_id]',wrapFilterConditionCompany).val();
        var districtId = $('select[name=district_id]',wrapFilterConditionCompany).val();
        if(page==null){
            page = 1;
        }
        var option = {
            controller :'Company',
            action:'Search',
            data:{
                FilterKeyword : filterKeyword,
                CityId : cityId,
                DistrictId : districtId,
                PageIndex : page,
                PageSize : 25
            },
            dataType:'html',
            element:$('#divListCompany'),
            callback:function(){
                company_list.action();
            }
        };
        sysrequest.send(option);
    },
    search_get_district_by_city_id: function () {
        var wrapFilterConditionCompany = $('#wrapFilterConditionCompany');
        var cityId = $('select[name=city_id]',wrapFilterConditionCompany).val();
        if(cityId == null || cityId == ''){
            cityId = 0;
        }
        var option = {
            controller:'Company',
            action:'GetDictrictByCityId',
            data:{
                cityId:cityId
            },
            callback: function (response) {
                var html = '<option value="">--District--</option>';
                if(response.Data != null && response.Data.length >0){
                    for(var index = 0;index < response.Data.length;index++){
                        html +='<option value="'+ response.Data[index].Value +'">' + response.Data[index].Text + '</option>'
                    }
                }
                $('select[name=district_id]',wrapFilterConditionCompany).html(html);
                $('select[name=district_id]',wrapFilterConditionCompany).trigger('liszt:updated');
            }
        };
        sysrequest.send(option);
    },
    create_action_get_district_by_city_id: function () {
        var wrapCompanyForCreate = $('#wrapCompanyForCreate');
        var cityId = $('select[name=city_id]',wrapCompanyForCreate).val();
        var option = {
            controller:'Company',
            action:'GetDictrictByCityId',
            data:{
                cityId:cityId
            },
            callback: function (response) {
                var html = '';
                if(response.Data != null && response.Data.length >0){
                    for(var index = 0;index < response.Data.length;index++){
                        html +='<option value="'+ response.Data[index].Value +'">' + response.Data[index].Text + '</option>'
                    }
                }
                $('select[name=district_id]',wrapCompanyForCreate).html(html);
                $('select[name=district_id]',wrapCompanyForCreate).trigger('liszt:updated');
            }
        };
        sysrequest.send(option);
    },
    create_action_reset: function () {
        var wrapCompanyForCreate = $('#wrapCompanyForCreate');
        $('input[name=company_name]',wrapCompanyForCreate).val('');
        $('input[name=address]',wrapCompanyForCreate).val('');
        $('input[name=phone_number]',wrapCompanyForCreate).val('');
        $('select[name=city_id]',wrapCompanyForCreate).val($('select[name=city_id] option:first',wrapCompanyForCreate).val());
        company_index.create_action_get_district_by_city_id();
    },
    create_action_validate: function () {
        var wrapCompanyForCreate = $('#wrapCompanyForCreate');
        var is_valid = true ;
        is_valid =company_index.validate_input_text_required($('input[name=company_name]',wrapCompanyForCreate),'You have to text the company name !');
        is_valid = company_index.validate_input_text_required($('input[name=address]',wrapCompanyForCreate),'You have to text the company address !');
        is_valid = company_index.validate_input_text_required($('input[name=phone_number]',wrapCompanyForCreate),'You have to text the phone number !');
        return is_valid;
    },
    create_action_save: function () {
        var wrapCompanyForCreate = $('#wrapCompanyForCreate');
        company_index.clear_error(wrapCompanyForCreate);
        if(company_index.create_action_validate()){
            var company_name = $('input[name=company_name]',wrapCompanyForCreate).val();
            var company_address = $('input[name=address]',wrapCompanyForCreate).val();
            var phone_number = $('input[name=phone_number]',wrapCompanyForCreate).val();
            var city_id = $('select[name=city_id]',wrapCompanyForCreate).val();
            var district_id = $('select[name=district_id]',wrapCompanyForCreate).val();
            var id = $('#create_company_reset',wrapCompanyForCreate).data('id');
            var option = {
                controller:'Company',
                action : 'Save',
                data:{
                    Name:company_name,
                    Address:company_address,
                    PhoneNumber : phone_number,
                    CityId:city_id,
                    DistrictId:district_id,
                    Id : id
                },
                callback: function (response) {
                    $('button.close',wrapCompanyForCreate).trigger('click');
                    if(response.Code == ResponseCode.Success ){
                        sysmess.info(response.Message);
                        company_index.search(1);
                    }
                    else if(response.Code == ResponseCode.Error){
                        sysmess.error(response.Message);
                    }
                    else{
                        sysmess.warning(response.Message);
                    }
                }
            };

            sysrequest.send(option);
        }
    },
    validate_input_text_required: function (ctrl,error_msg) {
        var is_valid = true;
        if(ctrl.val() == null || ctrl.val()==''){
            ctrl.parent("div").parent("div").addClass("error");
            ctrl.parent("div").append('<span class=\"help-inline no-left-padding\">'+error_msg+'</span>');
            is_valid = false;
        }
        return is_valid;
    },
    build_create_view: function (arr_city) {
        var html= syshtml.raw({
            Type:'modal',
            Head:{Title:'Create company' },
            Box:{
                Class:'overflow-visible'
            },
            Body:[
                [
                    {Type:'ui',Class:'span11 hide',UI:{Type:'label',Id:'lbEmpty'},Required:true },
                    {Type:'ui',Class:'span12 mgl0',UI:{Type:'text',Name:'company_name',Title:'Company name',Class:'m-wrap span10'},Required:true},
                    {Type:'ui',Class:'span6 mgl0',UI:{Type:'text',Name:'address',Title:'Address',Class:'m-wrap span10'},Required:true},
                    {Type:'ui',Class:'span6 mgl0',UI:{Type:'text',Name:'phone_number',Title:'Phone number',Class:'m-wrap span10'},Required:true},
                    {Type:'ui',Class:'span6 mgl0',UI:{Type:'select',Id:'dllCityId',Option:arr_city,Name:'city_id',Title:'City',Class:'m-wrap span10 chosen'},Required:false},
                    {Type:'ui',Class:'span6 mgl0',UI:{Type:'select',Id:'dllDistrictId',Name:'district_id',Title:'District',Class:'m-wrap span10 chosen'},Required:false}
                ]
            ],
            Button:[
                { Id: "create_company_reset", Class: "btn blue", Value: 0, Icon: "icon-refresh", Text: "Refresh", Attribute: [{ Name: "title", Value: "Reset to default"}] },
                { Id: "create_company_save", Class: "btn blue", Value: 0, Icon: "icon-save", Text: "Add new company", Attribute: [{ Name: "title", Value: "Add new a company"},{ Name: "data-id", Value: "0"}] }
            ]
        });
        $(html).appendTo($('#wrapCompanyForCreate'));
    }
};
var company_list = {
    init: function () {
        company_list.action();
    },
    action: function () {
        var divbodyCompany = $('#divbodyCompany');
        $('a[name=edit]',divbodyCompany).each(function () {
            var  id = $(this).data('id');
            $(this).off('click').on('click', function () {
                var option = {
                    controller:'Company',
                    action:'Edit',
                    data:{
                        id:id
                    },
                    callback: function (response) {
                        if(response.Data != null ){

                            $('button[name=add_company]',$('#divbodyCompany')).trigger('click');
                            var wrapCompanyForCreate = $('#wrapCompanyForCreate');
                            $('.modal-header h3',wrapCompanyForCreate).html('Edit company');
                            $('#create_company_save',wrapCompanyForCreate).html('<i class="icon-save"></i> Edit company');
                            $('input[name=company_name]',wrapCompanyForCreate).val(response.Data.Name);
                            $('input[name=address]',wrapCompanyForCreate).val(response.Data.Address);
                            $('input[name=phone_number]',wrapCompanyForCreate).val(response.Data.PhoneNumber);
                            $('select[name=city_id]',wrapCompanyForCreate).val(response.Data.CityId);
                            $('select[name=city_id]',wrapCompanyForCreate).trigger('liszt:updated');
                            $('#create_company_reset',wrapCompanyForCreate).data('id',response.Data.Id);
                            var districtId = response.Data.DistrictId;
                            var option = {
                                controller:'Company',
                                action:'GetDictrictByCityId',
                                data:{
                                    cityId:response.Data.CityId
                                },
                                callback: function (response) {
                                    var html = '';
                                    if(response.Data != null && response.Data.length >0){
                                        for(var index = 0;index < response.Data.length;index++){
                                            html +='<option value="'+ response.Data[index].Value +'">' + response.Data[index].Text + '</option>'
                                        }
                                    }
                                    $('select[name=district_id]',wrapCompanyForCreate).html(html);
                                    $('select[name=district_id]',wrapCompanyForCreate).val(districtId);
                                    $('select[name=district_id]',wrapCompanyForCreate).trigger('liszt:updated');
                                }
                            };
                            sysrequest.send(option);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }) ;
        });

        $('a[name=delete]',divbodyCompany).each(function () {
            var id = $(this).data('id');
            $(this).off('click').on('click', function () {
                var option = {
                    controller:'Company',
                    action:'Delete',
                    data:{
                        id:id
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.info(response.Message);
                            company_index.search(1);
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysmess.confirm('Dou you want to delete this company ?', function () {
                   sysrequest.send(option);
                });
            });
        });
    }
}