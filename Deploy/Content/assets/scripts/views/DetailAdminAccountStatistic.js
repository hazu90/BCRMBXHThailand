﻿var  detail_admin_account_statistic_index = {
    init: function () {
        $(document).ready(function () {
            detail_admin_account_statistic_index.action();
            detail_admin_account_statistic_index.search();
        });
    },
    action: function () {
        var frm_search = $('#frmSearchDetailAdminAccountStatistic');
        $('button[name=search]').off('click').on('click', function () {
            detail_admin_account_statistic_index.search();
        });
        $('button[name=reset]').off('click').on('click', function () {
            $('select[name=ActivedStatus]',frm_search).val('0');
            $('input[name=TimeToCreationFrom]',frm_search).val('');
            $('input[name=TimeToCreationTo]',frm_search).val('');
        });
        $('select[name=GroupId]',frm_search).off('change').on('change', function () {
            detail_admin_account_statistic_index.get_user_by_groupid();
        });
    },
    search : function () {
        var frm_search = $('#frmSearchDetailAdminAccountStatistic');
        var dllActivedStatus = $('select[name=ActivedStatus]',frm_search);
        var txtTimeToCreationFrom = $('input[name=TimeToCreationFrom]',frm_search);
        var txtTimeToCreationTo = $('input[name=TimeToCreationTo]',frm_search);
        var dllGroupId = $('select[name=GroupId]',frm_search);
        var dllAssignTo = $('select[name=AssignTo]',frm_search);

        if(txtTimeToCreationFrom.val() == null || txtTimeToCreationFrom.val() ==''){
            sysmess.warning('You must enter both of creation date !');
            return;
        }

        if(txtTimeToCreationTo.val() == null || txtTimeToCreationTo.val() ==''){
            sysmess.warning('You must enter both of creation date !');
            return;
        }

        var option = {
            controller:'DetailAdminAccountStatistic',
            action:'Search',
            data:{
                ActiveStatus : dllActivedStatus.val(),
                TimeToCreationFrom : sysformat.date(txtTimeToCreationFrom.val()),
                TimeToCreationTo : sysformat.date(txtTimeToCreationTo.val()),
                GroupId: dllGroupId.val(),
                CreatedBy : dllAssignTo.val()
            },
            dataType:'html',
            element:$('#divListDetailAdminAccountStatistic'),
            callback: function () {

            }
        };
        sysrequest.send(option);
    },
    get_user_by_groupid: function () {
        var frm_search = $('#frmSearchDetailAdminAccountStatistic');
        var groupId =$('select[name=GroupId]',frm_search).val();
        var option = {
            controller:'DetailAdminAccountStatistic',
            action : 'GetUserByGroupId',
            data:{
                groupId : groupId
            },
            callback: function (response) {
                var dllAssignTo = $('select[name=AssignTo]',frm_search);
                var html = '<option value="">--All users --</option>';
                if( response.Code == ResponseCode.Success ){
                    if(response.Data != null && response.Data.length > 0   ){
                        for(var index = 0 ;index < response.Data.length ; index++ ){
                            html += '<option value="'+ response.Data[index].Key +'">' + response.Data[index].Value +'</option>';
                        }
                    }
                    dllAssignTo.html(html);
                }
                else{
                    dllAssignTo.html(html);
                }
                dllAssignTo.trigger('liszt:updated');
            }
        };
        sysrequest.send(option);
    }
}