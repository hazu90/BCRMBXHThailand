﻿var customer_index = {
    init: function (lbl_all_city , lbl_all_district) {
        $(document).ready(function () {
            app.initChoosenSelect($('#divCreateCustomer'));
            $('#ddlRegion_chzn',$('#divCreateCustomer')).addClass('span12');
            $('#ddlRegion_chzn',$('#divCreateCustomer')).attr('style','');
            $('#ddlRegion_chzn .chzn-drop',$('#divCreateCustomer')).addClass('span12');
            $('#ddlRegion_chzn .chzn-drop',$('#divCreateCustomer')).attr('style','left: -9000px; top: 33px;');
            $('#ddlRegion_chzn .chzn-drop input',$('#divCreateCustomer')).addClass('span12');
            $('#ddlRegion_chzn .chzn-drop input',$('#divCreateCustomer')).attr('style','');

            $('#ddlCity_chzn',$('#divCreateCustomer')).addClass('span12');
            $('#ddlCity_chzn',$('#divCreateCustomer')).attr('style','');
            $('#ddlCity_chzn .chzn-drop',$('#divCreateCustomer')).addClass('span12');
            $('#ddlCity_chzn .chzn-drop',$('#divCreateCustomer')).attr('style','left: -9000px; top: 33px;');
            $('#ddlCity_chzn .chzn-drop input',$('#divCreateCustomer')).addClass('span12');
            $('#ddlCity_chzn .chzn-drop input',$('#divCreateCustomer')).attr('style','');

            $('#ddlDistrict_chzn',$('#divCreateCustomer')).addClass('span12');
            $('#ddlDistrict_chzn',$('#divCreateCustomer')).attr('style','');
            $('#ddlDistrict_chzn .chzn-drop',$('#divCreateCustomer')).addClass('span12');
            $('#ddlDistrict_chzn .chzn-drop',$('#divCreateCustomer')).attr('style','left: -9000px; top: 33px;');
            $('#ddlDistrict_chzn .chzn-drop input',$('#divCreateCustomer')).addClass('span12');
            $('#ddlDistrict_chzn .chzn-drop input',$('#divCreateCustomer')).attr('style','');

            $('#ddlCompany_chzn',$('#divCreateCustomer')).addClass('span12');
            $('#ddlCompany_chzn',$('#divCreateCustomer')).attr('style','');
            $('#ddlCompany_chzn .chzn-drop',$('#divCreateCustomer')).addClass('span12');
            $('#ddlCompany_chzn .chzn-drop',$('#divCreateCustomer')).attr('style','left: -9000px; top: 33px;');
            $('#ddlCompany_chzn .chzn-drop input',$('#divCreateCustomer')).addClass('span12');
            $('#ddlCompany_chzn .chzn-drop input',$('#divCreateCustomer')).attr('style','');

            app.initChoosenSelect($('#divEditCustomer'));
            $('#ddlRegionEdit_chzn',$('#divEditCustomer')).addClass('span12');
            $('#ddlRegionEdit_chzn',$('#divEditCustomer')).attr('style','');
            $('#ddlRegionEdit_chzn .chzn-drop',$('#divEditCustomer')).addClass('span12');
            $('#ddlRegionEdit_chzn .chzn-drop',$('#divEditCustomer')).attr('style','left: -9000px; top: 33px;');
            $('#ddlRegionEdit_chzn .chzn-drop input',$('#divEditCustomer')).addClass('span12');
            $('#ddlRegionEdit_chzn .chzn-drop input',$('#divEditCustomer')).attr('style','');

            $('#ddlCityEdit_chzn',$('#divEditCustomer')).addClass('span12');
            $('#ddlCityEdit_chzn',$('#divEditCustomer')).attr('style','');
            $('#ddlCityEdit_chzn .chzn-drop',$('#divEditCustomer')).addClass('span12');
            $('#ddlCityEdit_chzn .chzn-drop',$('#divEditCustomer')).attr('style','left: -9000px; top: 33px;');
            $('#ddlCityEdit_chzn .chzn-drop input',$('#divEditCustomer')).addClass('span12');
            $('#ddlCityEdit_chzn .chzn-drop input',$('#divEditCustomer')).attr('style','');

            $('#ddlDistrictEdit_chzn',$('#divEditCustomer')).addClass('span12');
            $('#ddlDistrictEdit_chzn',$('#divEditCustomer')).attr('style','');
            $('#ddlDistrictEdit_chzn .chzn-drop',$('#divEditCustomer')).addClass('span12');
            $('#ddlDistrictEdit_chzn .chzn-drop',$('#divEditCustomer')).attr('style','left: -9000px; top: 33px;');
            $('#ddlDistrictEdit_chzn .chzn-drop input',$('#divEditCustomer')).addClass('span12');
            $('#ddlDistrictEdit_chzn .chzn-drop input',$('#divEditCustomer')).attr('style','');

            $('#ddlCompanyEdit_chzn',$('#divEditCustomer')).addClass('span12');
            $('#ddlCompanyEdit_chzn',$('#divEditCustomer')).attr('style','');
            $('#ddlCompanyEdit_chzn .chzn-drop',$('#divEditCustomer')).addClass('span12');
            $('#ddlCompanyEdit_chzn .chzn-drop',$('#divEditCustomer')).attr('style','left: -9000px; top: 33px;');
            $('#ddlCompanyEdit_chzn .chzn-drop input',$('#divEditCustomer')).addClass('span12');
            $('#ddlCompanyEdit_chzn .chzn-drop input',$('#divEditCustomer')).attr('style','');

            var form = $("#frmSearchCustomer");
            $("#selCity", form).off("change").on("change", function () {
                var cityId = $(this).val();
                if (cityId > 0) {
                    var option = {
                        controller: "City",
                        action: "GetDistrictByCity",
                        data: {
                            cityId: cityId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#selDistrict", form).empty();
                                if (response.Data.length > 0) {
                                    $("#selDistrict", form).append('<option value="">'+lbl_all_district +'</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#selDistrict", form).append(html);
                                $('#selDistrict', form).trigger('liszt:updated');

                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });
            customer_index.search();
            $("#btnReset", form).off("click").on("click", function () {
                customer_index.reset();
            });
            $("#btnSearch", form).off("click").on("click", function () {
                $('#lnkRemoveCreateCustomer').trigger('click');
                customer_index.search();
            });

            $('#btnExportExcel',form).off('click').on('click', function () {
                var startDate = $('#txtStartDate',form).val();
                if(startDate==null  || startDate ==''){
                    startDate ='';
                }
                $('#hiddStartDate',form).val(sysformat.date(startDate,'mm/dd/yyyy'));
                var endDate = $('#txtEndDate',form).val();
                if(endDate == null || endDate == ''){
                    endDate = '';
                }
                $('#hiddEndDate',form).val(sysformat.date(endDate,'mm/dd/yyyy'));
                $('#frmSearchCustomer').submit();
            } );

            customer_index.action();
            form.off('keydown').on('keydown', function (e) {
                customer_index.hitting_enter(e);
            });
            // init in assigning form
            if($('#wrapAssignTo').length >0 ){
                var wrapAssignTo = $('#wrapAssignTo');
                var groupId = $('select[name=group]',wrapAssignTo).val();
                var option = {
                    controller:'Customer',
                    action:'GetSaleByGroupId',
                    data:{
                        groupId:groupId
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success ){
                            var html = '<option value="">--Choose a saler--</option>';
                            if(response.Data != null && response.Data.length >0 ){
                                for(var index = 0;index < response.Data.length;index++ ){
                                    html += '<option value="'+ response.Data[index].UserName +'">'+ response.Data[index].UserName +'</option>';
                                }
                            }
                            $('select[name=saler]',wrapAssignTo).html(html);
                        }
                        else{
                            $('button[name=close]',wrapAssignTo).trigger('click');
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });
    },
    reset: function () {
        try {
            var form = $('#frmSearchCustomer');
            $("#txtFilterKeyword", form).val('');
            $("#selAssignTo", form).val('-1');
            $("#selAssignTo", form).trigger('liszt:updated');
            $("#selStatus", form).val("");
            $("#selStatus", form).trigger('liszt:updated');
            $('#selCity', form).val("");
            $('#selCity', form).trigger('liszt:updated');
            $('#selDistrict', form).val("");
            $('#selDistrict', form).trigger('liszt:updated');
            $('#selSort', form).val("-1");
            $('#selSort', form).trigger('liszt:updated');
            $('#selCustomerType', form).val("");
            $('#selCustomerType', form).trigger('liszt:updated');
            $('#divStartDate input', form).val("");
            $('#divEndDate input', form).val("");
        }
        catch (e) {
            sysmess.error('Error javascript! <br />' + e);
        }
    },
    search: function (page) {
        var form = $('#frmSearchCustomer');
        var cityId = -1;

        if ($("#selCity", form).val() > 0) {
            cityId = $("#selCity", form).val();
        }
        var districtId = -1;
        if ($("#selDistrict", form).val() > 0) {
            districtId = $("#selDistrict", form).val();
        }
        var assignTo = $("#selAssignTo", form).val();
        if(assignTo == null ){
            assignTo = '';
        }

        var options = {
            controller: 'Customer',
            action: 'GetListCustomer',
            dataType: 'html',
            data: {
                FilterKeyword: $("#txtFilterKeyword", form).val(),
                StartDate: sysformat.date( $("#txtStartDate", form).val()),
                EndDate: sysformat.date($("#txtEndDate", form).val()),
                CityId: cityId,
                DistrictId: districtId,
                Status: $("#selStatus", form).val(),
                AssignTo: assignTo,
                Type: $("#selCustomerType", form).val(),
                Sort: $("#selSort", form).val(),
                Creator : $('#selCreator',form).val(),
                PageIndex: page,
                PageSize: 25
            },
            element: $('div.form-inline', $('#divListCustomer')),
            blockUI: $('div#main-content'),
            scrollUI: $('#divListCustomer').parent(),
            callback: function () {
                customer_list.init();
            }
        };
        sysrequest.send(options);
    },
    action: function () {

        // action in assigning to saler form
        var wrapAssignTo = $('#wrapAssignTo');
        $('select[name=group]',wrapAssignTo).off('change').on('change', function () {
            var groupId = $(this).val();
            var option = {
                controller:'Customer',
                action:'GetSaleByGroupId',
                data:{
                    groupId:groupId
                },
                callback: function (response) {
                    if(response.Code == ResponseCode.Success ){
                        var html = '<option value="">--Choose a saler--</option>';
                        if(response.Data != null && response.Data.length >0 ){
                            for(var index = 0;index < response.Data.length;index++ ){
                                html += '<option value="'+ response.Data[index].UserName +'">'+ response.Data[index].UserName +'</option>';
                            }
                        }
                        $('select[name=saler]',wrapAssignTo).html(html);
                    }
                    else{
                        $('button[name=close]',wrapAssignTo).trigger('click');
                        sysmess.warning(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        });
        $('button[name=save]',wrapAssignTo).off('click').on('click', function () {
            var cid = $(this).attr('cid');
            var select_saler = $('select[name=saler]',wrapAssignTo);
            if(select_saler.val() == null || select_saler.val() == ''){
                select_saler.parent().parent().addClass('error');
                select_saler.parent().parent().append('<span class=\"help-inline no-left-padding\">You have to choose a saler before assigning !</span>');
                return;
            }
            var option={
                controller:'Customer',
                action :'AssignTo',
                data:{
                    customerId:cid,
                    userName: select_saler.val()
                },
                callback: function (response) {
                    $('button[name=close]',wrapAssignTo).trigger('click');
                    if(response.Code == ResponseCode.Success){
                        sysmess.info(response.Message);
                        customer_index.search(customer_list.get_current_page());
                    }
                    else if(response.Code == ResponseCode.Error){
                        sysmess.error(response.Message);
                    }
                    else{
                        sysmess.warning(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        });

        // action in note form
        var wrapNote = $('#wrapNote');
        $('button[name=save]',wrapNote).off('click').on('click', function () {
            var id = $(this).attr('cid');
            var text_note = $('textarea[name=note]',wrapNote);
            if(text_note.val() == null || text_note.val() == ''){
                text_note.parent().parent().addClass('error');
                text_note.parent().parent().append('<span class=\"help-inline no-left-padding\">You have to text a note !</span>');
                return;
            }
            var note = text_note.val();
            var option ={
                controller:'Customer',
                action:'Note',
                data:{
                    id:id,
                    note:note
                },
                callback: function (response) {
                    $('button[name=close]',wrapNote).trigger('click');
                    if(response.Code == ResponseCode.Success ){
                        sysmess.info(response.Message);
                    }
                    else if ( response.Code == ResponseCode.Error ){
                        sysmess.error(response.Message);
                    }
                    else{
                        sysmess.warning(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        });
    },
    hitting_enter: function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $(this).blur();
            customer_index.search(1);
        }
    }
};
var customer_list = {
    init: function () {
        $(document).ready(function () {
            var area = $("#divListCustomer");
            $("#btnAddNew", area).off("click").on("click", function () {
                customer_create.clear_error($('#frmCreateCustomer'));
                $("#divBodyListCustomer").addClass("hide");
                $("#divCreateCustomer").removeClass("hide");
                customer_create.init();
                customer_create.reset();
            });

            $("a.edit", area).each(function () {
                var curr = $(this);
                curr.off("click").on("click", function () {
                    var custId = $(this).attr("cid");
                    $("#divBodyListCustomer").addClass("hide");
                    $("#divEditCustomer").removeClass("hide");
                    customer_edit.init();
                    customer_edit.getById(custId);
                });
            });

            $("a.lock", area).each(function () {
                var curr = $(this);
                curr.off("click").on("click", function () {
                    var tr_element = $(this).parent().parent().parent().parent().parent();
                    var custId = $(this).attr("cid");
                    var option = {
                        controller: "Customer",
                        action: "Block",
                        data: { customerId: custId },
                        callback: function (response) {
                            if(response.Code == ResponseCode.Success){
                                customer_list.change_row_data(tr_element,custId);
                                sysmess.info("Successfully!");
                            }
                            else if(response.Code == ResponseCode.Error ){

                            }
                            else{

                            }
                            //switch (response.Code) {
                            //    case ResponseCode.Success:
                            //        sysmess.info("Successfully!");
                            //        customer_index.search(1);
                            //        break;
                            //    case ResponseCode.Error:
                            //        sysmess.error("Error: " + response.Message);
                            //        sysmess.log(response.Message);
                            //        break;
                            //}
                        }
                    };
                    sysmess.confirm('Do you want block this file customer?', function () {
                        sysrequest.send(option);
                    });
                });
            });

            $("a.unlock", area).each(function () {
                var curr = $(this);
                curr.off("click").on("click", function () {
                    var custId = $(this).attr("cid");
                    var option = {
                        controller: "Customer",
                        action: "UnBlock",
                        data: { customerId: custId },
                        callback: function (response) {
                            switch (response.Code) {
                                case ResponseCode.Success:
                                    sysmess.info("Successfully!");
                                    customer_index.search(1);
                                    break;
                                case ResponseCode.Error:
                                    sysmess.error("Error: " + response.Message);
                                    sysmess.log(response.Message);
                                    break;
                            }
                        }
                    };
                    sysmess.confirm('Do you want active this file customer?', function () {
                        sysrequest.send(option);
                    });
                });
            });

            $("a.phonenumber", area).each(function () {
                var curr = $(this);
                curr.off("click").on("click", function () {
                    var custId = $(this).attr("cid");
                    var options = {
                        controller: 'Profile',
                        action: 'GetListByCustomer',
                        dataType: 'html',
                        data: {
                            customerId: custId
                        },
                        element: $('#divListProfile'),
                        blockUI: $('div#main-content'),
                        scrollUI: $('#divListProfile').parent(),
                        callback: function () {
                            profile_list.init();
                        }
                    };
                    sysrequest.send(options);
                });
            });

            $('a.assignto',area).each(function () {
                $(this).off('click').on('click', function () {
                    var cusId = $(this).attr('cid');
                    $('button[name=save]',$('#wrapAssignTo')).attr('cid',cusId);
                });
            });

            $('a.blacklist',area).each(function () {
               $(this).off('click').on('click', function () {
                   var cusId = $(this).attr('cid');
                   var option = {
                       controller:'Customer',
                       action:'Blacklist',
                       data:{
                           customerId : cusId
                       },
                       callback: function (response) {
                           if(response.Code == ResponseCode.Success){
                                sysmess.info(response.Message);
                               customer_index.search(customer_list.get_current_page() );
                           }
                           else if(response.Code == ResponseCode.Error){
                                sysmess.error(response.Message);
                           }
                           else{
                                sysmess.warning(response.Message);
                           }
                       }
                   };
                   sysmess.confirm('Do you want to blacklist this customer ?', function () {
                      sysrequest.send(option);
                   });
               });
            });

            $('a.unblacklist',area).each(function () {
                $(this).off('click').on('click', function () {
                    var cid = $(this).attr('cid');
                    var option = {
                        controller:'Customer',
                        action:'UnBlacklist',
                        data:{
                            customerId : cid
                        },
                        callback: function (response) {
                            if(response.Code == ResponseCode.Success){
                                sysmess.info(response.Message);
                                customer_index.search(customer_list.get_current_page() );
                            }
                            else if(response.Code == ResponseCode.Error){
                                sysmess.error(response.Message);
                            }
                            else{
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    sysmess.confirm('Do you want to unblacklist this customer ?', function () {
                        sysrequest.send(option);
                    });
                });
            });

            $('a.note',area).each(function () {
                var cid = $(this).attr('cid');
                $(this).off('click').on('click', function () {
                    $('textarea[name=note]',$('#wrapNote')).val('');
                    $('button[name=save]',$('#wrapNote')).attr('cid',cid);
                });
            });

            $('a.detail',area).each(function () {
                var cid = $(this).attr('cid');
                $(this).off('click').on('click', function () {
                   var option = {
                       controller : 'Customer',
                       action:'Detail',
                       data:{
                           customerId : cid
                       },
                       dataType:'html',
                       element:$('#wrapCustomerDetail'),
                       callback: function () {
                           $('#divBodyListCustomer').addClass('hide');
                           var wrapCustomerDetail = $('#wrapCustomerDetail');
                           $('#wrapCustomerDetail').removeClass('hide');
                           app.initPortletTools();
                           // action in detail form
                           $('button[name=close]',wrapCustomerDetail).off('click').on('click', function () {
                               $('#lnkRemoveCustomerActivityHistory',wrapCustomerDetail).trigger('click');
                               $('#lnkRemoveCustomerDetail',wrapCustomerDetail).trigger('click');
                               $('#divBodyListCustomer').removeClass('hide');
                           });
                       }
                   };
                   sysrequest.send(option);
                });
            });

            $('a.receive',area).each(function () {
                var cid = $(this).attr('cid');
                $(this).off('click').on('click', function () {
                    var option = {
                        controller:'Customer',
                        action:'Receive',
                        data:{
                            customerId : cid
                        },
                        callback: function (response) {
                            if(response.Code == ResponseCode.Success){
                                sysmess.info(response.Message);
                                customer_index.search(customer_list.get_current_page());
                            }
                            else if(response.Code == ResponseCode.Error){
                                sysmess.error(response.Message);
                            }
                            else{
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    sysmess.confirm('Do you want to receive this customer ?', function () {
                        sysrequest.send(option);
                    });
                });
            });

            $('a[name=return]',area).each(function () {
                $(this).off('click').on('click', function () {
                    var customerId = $(this).attr('cid');
                    var assigner = $(this).attr('assname');
                    var option = {
                        controller:'Customer',
                        action :'Return',
                        data:{
                            customerId : customerId,
                            assginer   : assigner
                        },
                        callback: function (response) {
                            if(response.Code == ResponseCode.Success){
                                sysmess.info(response.Message);
                                customer_index.search(customer_list.get_current_page());
                                //customer_index.search(customer_index.get);
                            }
                            else if(response.Code == ResponseCode.Error){
                                sysmess.error(response.Message);
                            }
                            else{
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    sysmess.confirm('Do you want to return this customer ?', function () {
                        sysrequest.send(option);
                    } );
                });
            });

        });
    },
    change_row_data : function(tr_element,customerId ){
        var index = $('td[name=wrapIndex]',tr_element).text();
        var option = {
            controller:'Customer',
            action:'GetViewByCustomerId',
            data:{
                index : index,
                customerId : customerId
            },
            dataType:'html',
            element : tr_element,
            callback: function () {
            }
        };
        sysrequest.send(option);
    },
    get_current_page: function () {
        var curr_Page = 1;
        $('.dataTables_paginate li.active a').each(function () {
            var id_html = $(this).text().trim();
            if($.isNumeric(id_html)){
                curr_Page = parseInt(id_html);
            }
        });
        return curr_Page;
    }
};
var customer_create = {
    init: function () {
        $(document).ready(function () {
            var form = $('#frmCreateCustomer');
            if ($('#txaDescription', form).length > 0 && $("#cke_txaDescription", form).length == 0) {
                syseditor.create('txaDescription');
                CKEDITOR.addCss('.cke_editable { height:160px; }');
            }
            $("#ddlRegion", form).off("change").on("change", function () {
                var regionId = $(this).val();
                var cityId = 0;
                if(regionId > 0){
                    var option = {
                        controller: "City",
                        action: "GetCityByRegion",
                        data: {
                            regionId: regionId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#ddlCity", form).empty();
                                if (response.Data.length > 0) {
                                    cityId = response.Data[0].Id;
                                    $("#ddlCity", form).append('<option value="">--City--</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#ddlCity", form).append(html);
                                $("#ddlCity", form).trigger('liszt:updated');
                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
                if (cityId > 0) {
                    var option = {
                        controller: "City",
                        action: "GetDistrictByCity",
                        data: {
                            cityId: cityId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#ddlDistrict", form).empty();
                                if (response.Data.length > 0) {
                                    $("#ddlDistrict", form).append('<option value="">--District--</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#ddlDistrict", form).append(html);
                                $("#ddlDistrict", form).trigger('liszt:updated');
                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });

            $("#ddlCity", form).off("change").on("change", function () {
                var cityId = $(this).val();
                if (cityId > 0) {
                    var option = {
                        controller: "City",
                        action: "GetDistrictByCity",
                        data: {
                            cityId: cityId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#ddlDistrict", form).empty();
                                if (response.Data.length > 0) {
                                    $("#ddlDistrict", form).append('<option value="">--District--</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#ddlDistrict", form).append(html);
                                $("#ddlDistrict", form).trigger('liszt:updated');
                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });

            $("#btnAddNewCustomer", form).off("click").on("click", function () {
                $("span.help-inline", form).each(function () {
                    $(this).remove();
                });
                $('.control-group',form).each(function () {
                    $(this).removeClass('error');
                });

                var checkValue = customer_create.valid_data();
                if (checkValue == true) {
                    var ddlDistrict = $('#ddlDistrict',form);
                    var districtId = 0;
                    if(ddlDistrict.val() != null && ddlDistrict.val() != ''){
                        districtId = ddlDistrict.val();
                    }
                    var data = {
                        FullName: $("#txtFullName", form).val(),
                        PhoneNumber: $("#txtPhoneNumber", form).val(),
                        Type: $("#ddlType", form).val(),
                        Email: $("#txtEmail", form).val(),
                        Description: syseditor.get_data('txaDescription'),
                        CityId: $("#ddlCity", form).val(),
                        DistrictId: districtId,
                        RegionId: $("#ddlRegion", form).val(),
                        CompanyId: $('#ddlCompany',form).val(),
                        Address:$('#txtAddress',form).val()
                    };
                    var option = {
                        controller: "Customer",
                        action: "CreateAction",
                        data: JSON.stringify(data) ,
                        callback: function (response) {
                            if(response.Code == ResponseCode.Success){
                                customer_index.search(1);
                                $("#divBodyListCustomer").removeClass("hide");
                                $("#divCreateCustomer").addClass("hide");
                            }
                            else if(response.Code == ResponseCode.Error){
                                sysmess.error("Error: " + response.Message);
                            }
                            else{
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });

            $("#btnCloseCustInput", form).off("click").on("click", function () {
                $("#divBodyListCustomer").removeClass("hide");
                $("#divCreateCustomer").addClass("hide");
            });
        });
    },
    get_city_district_when_change_region : function () {
        var form = $('#frmCreateCustomer');
        var regionId = $("#ddlRegion", form).val();
        var cityId = 0;
        if(regionId > 0){
            var option = {
                controller: "City",
                action: "GetCityByRegion",
                data: {
                    regionId: regionId
                },
                callback: function (response) {
                    if (response.Code == ResponseCode.Success) {
                        $("#ddlCity", form).empty();
                        if (response.Data.length > 0) {
                            cityId = response.Data[0].Id;
                            $("#ddlCity", form).append('<option value="">--City--</option>');
                        }
                        var html = "";
                        for (var i = 0; i < response.Data.length; i++) {
                            html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                        }
                        $("#ddlCity", form).append(html);
                        $("#ddlCity", form).trigger('liszt:updated');
                    } else {
                        sysmess.error(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        }
        if (cityId > 0) {
            var option = {
                controller: "City",
                action: "GetDistrictByCity",
                data: {
                    cityId: cityId
                },
                callback: function (response) {
                    if (response.Code == ResponseCode.Success) {
                        $("#ddlDistrict", form).empty();
                        if (response.Data.length > 0) {
                            $("#ddlDistrict", form).append('<option value="">--District--</option>');
                        }
                        var html = "";
                        for (var i = 0; i < response.Data.length; i++) {
                            html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                        }
                        $("#ddlDistrict", form).append(html);
                        $("#ddlDistrict", form).trigger('liszt:updated');
                    } else {
                        sysmess.error(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        }
    },
    get_district_when_change_city: function () {
        var form = $('#frmCreateCustomer');
        var cityId = $("#ddlCity", form).val();
        if (cityId > 0) {
            var option = {
                controller: "City",
                action: "GetDistrictByCity",
                data: {
                    cityId: cityId
                },
                callback: function (response) {
                    if (response.Code == ResponseCode.Success) {
                        $("#ddlDistrict", form).empty();
                        if (response.Data.length > 0) {
                            $("#ddlDistrict", form).append('<option value="">--District--</option>');
                        }
                        var html = "";
                        for (var i = 0; i < response.Data.length; i++) {
                            html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                        }
                        $("#ddlDistrict", form).append(html);
                        $("#ddlDistrict", form).trigger('liszt:updated');
                    } else {
                        sysmess.error(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        }
    },
    valid_data: function () {
        var valid = true;
        try {
            var focus = null;
            var form = $('#frmCreateCustomer');

            var fullName = $.trim($("#txtFullName", form).val());
            if (fullName == null || fullName == "") {
                focus = $("#txtFullName", form);
                $("#txtFullName", form).parent("div").parent("div").addClass("error");
                $("#txtFullName", form).next('span.help-inline').remove();
                $("#txtFullName", form).parent("div").append("<span class=\"help-inline no-left-padding\">FullName is required.</span>");
                valid = false;
            }

            var phoneNumber = $.trim($("#txtPhoneNumber", form).val());
            if (phoneNumber == null || phoneNumber == "") {
                $("#txtPhoneNumber", form).parent("div").parent("div").addClass("error");
                $("#txtPhoneNumber", form).next('span.help-inline').remove();
                $("#txtPhoneNumber", form).parent("div").append("<span class=\"help-inline no-left-padding\">Phone Number is required.</span>");
                valid = false;
            }

            //var custStatus = $.trim($("#ddlCustomerStatus", form).val());
            //if (custStatus == null || custStatus == '') {
            //    if (focus == null) {
            //        focus = $("#ddlCustomerStatus", form);
            //    }
            //    $("#ddlCustomerStatus", form).parent("div").parent("div").addClass("error");
            //    $("#ddlCustomerStatus", form).next('span.help-inline').remove();
            //    $("#ddlCustomerStatus", form).parent("div").append("<span class=\"help-inline no-left-padding\>Enter Status.</span>");
            //    valid = false;
            //}

            var custType = $.trim($("#ddlType", form).val());
            if (custType == null || custType == "") {
                if (focus == null) {
                    focus = $("#ddlType", form);
                }
                $("#ddlType", form).parent("div").parent("div").addClass("error");
                $("#ddlType", form).next('span.help-inline').remove();
                $("#ddlType", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter Type Of Customer.</span>");
                valid = false;
            }
            
            var city = $.trim($("#ddlCity", form).val());
            if (city == null || city == "") {
                if (focus == null) {
                    focus = $("#ddlCity", form);
                }
                $("#ddlCity", form).parent("div").parent("div").addClass("error");
                $("#ddlCity", form).next('span.help-inline').remove();
                $("#ddlCity", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter Province.</span>");
                valid = false;
            }

            var email = $("#txtEmail", form).val();
            if(email != null && email != ''){
                email = $.trim(email);
                if(!sysvalid.email(email) ){
                    if (focus == null) {
                        focus = $("#txtEmail", form);
                    }
                    $("#txtEmail", form).parent("div").parent("div").parent("div").addClass("error");
                    $("#txtEmail", form).parent("div").next('span.help-inline').remove();
                    $("#txtEmail", form).parent("div").append(" <span class=\"help-inline no-left-padding\">Invalid Email Address.</span>");
                    valid = false;
                }
            }
            //else{
            //    if (focus == null) {
            //        focus = $("#txtEmail", form);
            //    }
            //    valid = false;
            //    $("#txtEmail", form).parent("div").parent("div").parent("div").addClass("error");
            //    $("#txtEmail", form).parent("div").next('span.help-inline').remove();
            //    $("#txtEmail", form).parent("div").append(" <span class=\"help-inline no-left-padding\">Email is required.</span>");
            //}

            var address = $('#txtAddress',form).val();
            if(address == null  || address==''){
                if (focus == null) {
                    focus = $("#txtAddress", form);
                }
                $("#txtAddress", form).parent("div").parent("div").addClass("error");
                $("#txtAddress", form).parent("div").next('span.help-inline').remove();
                $("#txtAddress", form).parent("div").append(" <span class=\"help-inline no-left-padding\">Address is not empty.</span>");
                valid = false;
            }

            if (focus != null) {
                focus.focus();
                syscommon.scroll(focus);
            }
        } catch (e) {
            sysmess.error('Error javascript!<br />' + e);
            valid = false;
        }
        return valid;
    },
    clear_error: function (form) {
        $("span.help-inline", form).each(function () {
            $(this).parent("div").parent("div").removeClass("error");
            $(this).parent("div").parent("div").parent("div").removeClass("error");
            $(this).remove();
        });
    },
    reset : function () {
        var form = $('#frmCreateCustomer');
        $('#txtFullName',form).val('');
        $('#txtPhoneNumber',form).val('');
        $('#txtEmail',form).val('');
        $('#ddlType',form).val($('#ddlType option:first',form).val());
        $('#ddlRegion',form).val($('#ddlRegion option:first',form).val() );
        $('#ddlRegion',form).trigger('liszt:updated');
        $('#txtAddress',form).val('');
        $('#ddlCompany',form).val($('#ddlCompany option:first',form).val() );
        customer_create.get_city_district_when_change_region();
        customer_create.get_district_when_change_city();
        syseditor.set_data('txaDescription','');
    }
};
var customer_edit = {
    init: function(){
        $(document).ready(function () {
            var form = $('#frmUpdateCustomer');
            if ($('#txaDescriptionUpdate', form).length > 0 && $("#cke_txaDescriptionUpdate", form).length == 0) {
                syseditor.create('txaDescriptionUpdate');
            }
        });
    },
    getById: function (custId) {
        var form = $("#frmUpdateCustomer");
        customer_create.clear_error(form);
        var option = {
            controller: "Customer",
            action: "GetById",
            data: { customerId: custId },
            callback: function (response) {
                switch (response.Code) {
                    case ResponseCode.Success:
                        $('#txtPhoneNumber',form).val(response.Data.PhoneNumber);
                        $("#hidCustomerId", form).val(response.Data.Id);
                        $("#txtFullName", form).val(response.Data.FullName);
                        $('#ddlCustomerStatus', form).val(response.Data.Status);
                        $("#ddlType", form).val(response.Data.Type);
                        $("#txtEmail", form).val(response.Data.Email);                        
                        syseditor.set_data('txaDescriptionUpdate', response.Data.Description);
                        $("#ddlRegionEdit", form).val(response.Data.RegionId);
                        $("#ddlRegionEdit", form).trigger('liszt:updated');
                        $('#txtAddress',form).val(response.Data.Address);
                        $('#ddlCompanyEdit',form).val(response.Data.CompanyId);
                        $('#ddlCompanyEdit',form).trigger('liszt:updated');
                        var option2 = {
                            controller: "City",
                            action: "GetCityByRegion",
                            data: {
                                regionId: response.Data.RegionId
                            },
                            callback: function (response2) {
                                if (response2.Code == ResponseCode.Success) {
                                    $("#ddlCityEdit", form).empty();
                                    if (response2.Data.length > 0) {
                                        $("#ddlCityEdit", form).append('<option value="">--City--</option>');
                                    }
                                    var html = "";
                                    for (var i = 0; i < response2.Data.length; i++) {
                                        if (response2.Data[i].Id == response.Data.CityId) {
                                            html += '<option value="' + response2.Data[i].Id + '" selected>' + response2.Data[i].Name + '</option>';
                                        }
                                        else {
                                            html += '<option value="' + response2.Data[i].Id + '">' + response2.Data[i].Name + '</option>';
                                        }
                                    }
                                    $("#ddlCityEdit", form).append(html);
                                    $("#ddlCityEdit", form).trigger('liszt:updated');
                                } else {
                                    sysmess.error(response1.Message);
                                }
                            }
                        };
                        sysrequest.send(option2);

                        var option1 = {
                            controller: "City",
                            action: "GetDistrictByCity",
                            data: {
                                cityId: response.Data.CityId
                            },
                            callback: function (response1) {
                                if (response1.Code == ResponseCode.Success) {
                                    $("#ddlDistrictEdit", form).empty();
                                    if (response1.Data.length > 0) {
                                        $("#ddlDistrictEdit", form).append('<option value="">--District--</option>');
                                    }
                                    var html = "";
                                    for (var i = 0; i < response1.Data.length; i++) {
                                        if (response1.Data[i].Id == response.Data.DistrictId)
                                        {
                                            html += '<option value="' + response1.Data[i].Id + '" selected>' + response1.Data[i].Name + '</option>';
                                        }
                                        else {
                                            html += '<option value="' + response1.Data[i].Id + '">' + response1.Data[i].Name + '</option>';
                                        }                                        
                                    }
                                    $("#ddlDistrictEdit", form).append(html);
                                    $("#ddlDistrictEdit", form).trigger('liszt:updated');
                                } else {
                                    sysmess.error(response1.Message);
                                }
                            }
                        };
                        sysrequest.send(option1);
                        customer_edit.update();
                        break;
                    case ResponseCode.Error:
                        sysmess.error("Error: " + response.Message);
                        sysmess.log(response.Message);
                        break;
                }
            }
        };
        sysrequest.send(option);
    },
    update: function () {
        $(document).ready(function () {
            var form = $('#frmUpdateCustomer');
            $("#ddlRegionEdit", form).off("change").on("change", function () {
                var regionId = $(this).val();
                var cityId = 0;
                if (regionId > 0) {
                    var option = {
                        controller: "City",
                        action: "GetCityByRegion",
                        data: {
                            regionId: regionId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#ddlCityEdit", form).empty();
                                if (response.Data.length > 0) {
                                    cityId = response.Data[0].Id;
                                    $("#ddlCityEdit", form).append('<option value="">--City--</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#ddlCityEdit", form).append(html);
                                $("#ddlCityEdit", form).trigger('liszt:updated');
                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
                if (cityId > 0) {
                    var option = {
                        controller: "City",
                        action: "GetDistrictByCity",
                        data: {
                            cityId: cityId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#ddlDistrictEdit", form).empty();
                                if (response.Data.length > 0) {
                                    $("#ddlDistrictEdit", form).append('<option value="0">--District--</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#ddlDistrictEdit", form).append(html);
                                $("#ddlDistrictEdit", form).trigger('liszt:updated');
                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });
            $("#ddlCityEdit", form).off("change").on("change", function () {
                var cityId = $(this).val();
                if (cityId > 0) {
                    var option = {
                        controller: "City",
                        action: "GetDistrictByCity",
                        data: {
                            cityId: cityId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#ddlDistrictEdit", form).empty();
                                if (response.Data.length > 0) {
                                    $("#ddlDistrictEdit", form).append('<option value="0">--District--</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#ddlDistrictEdit", form).append(html);
                                $("#ddlDistrictEdit", form).trigger('liszt:updated');
                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });
            $("#btnUpdateCustomer", form).off("click").on("click", function () {
                $("span.help-inline", form).each(function () {
                    $(this).parent("div").parent("div").removeClass("error");
                    $(this).remove();
                });
                var checkValue = customer_edit.valid_data();
                if (checkValue == true) {
                    var ddlDistrict = $('#ddlDistrictEdit',form);
                    var districtId = 0;
                    if(ddlDistrict.val() != null && ddlDistrict.val() != ''){
                        districtId = ddlDistrict.val();
                    }

                    var data = {
                        Id: $("#hidCustomerId", form).val(),
                        FullName: $("#txtFullName", form).val(),
                        PhoneNumber : $('#txtPhoneNumber',form).val(),
                        Type: $("#ddlType", form).val(),
                        Email: $("#txtEmail", form).val(),
                        Description: syseditor.get_data('txaDescriptionUpdate'),
                        CityId: $("#ddlCityEdit", form).val(),
                        DistrictId: districtId,
                        RegionId: $("#ddlRegionEdit", form).val(),
                        CompanyId: $('#ddlCompanyEdit',form).val(),
                        Address:$('#txtAddress',form).val()
                    };
                    var option = {
                        controller: "Customer",
                        action: "EditAction",
                        data: JSON.stringify(data),
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                customer_index.search(customer_list.get_current_page());
                                $("#divBodyListCustomer").removeClass("hide");
                                $("#divEditCustomer").addClass("hide");
                            }
                            else if (response.Code == ResponseCode.Error) {
                                sysmess.error("Error: " + response.Message);
                            }
                            else {
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });
            $("#btnCloseInput", form).off("click").on("click", function () {
                $("#divBodyListCustomer").removeClass("hide");
                $("#divEditCustomer").addClass("hide");
            });
            $('#ddlType',form).off('change').on('change', function () {
                var type= $(this).val();
                if(type == '0'){
                    $('#txtAddress',form).parent().parent().parent().removeClass('hide');
                }
                else{
                    $('#txtAddress',form).parent().parent().parent().addClass('hide');
                }
            });
        });
    },
    valid_data: function () {
        var valid = true;
        try {
            var focus = null;
            var form = $('#frmUpdateCustomer');

            var fullName = $.trim($("#txtFullName", form).val());
            if (fullName == null || fullName == "") {
                focus = $("#txtFullName", form);
                $("#txtFullName", form).parent("div").parent("div").addClass("error");
                $("#txtFullName", form).next('span.help-inline').remove();
                $("#txtFullName", form).parent("div").append("<span class=\"help-inline no-left-padding\">FullName is required.</span>");
                valid = false;
            }

            //var custStatus = $.trim($("#ddlCustomerStatus", form).val());
            //if (custStatus == null || custStatus == '') {
            //    if (focus == null) {
            //        focus = $("#ddlCustomerStatus", form);
            //    }
            //    $("#ddlCustomerStatus", form).parent("div").parent("div").addClass("error");
            //    $("#ddlCustomerStatus", form).next('span.help-inline').remove();
            //    $("#ddlCustomerStatus", form).parent("div").append("<span class=\"help-inline no-left-padding\>Enter Status.</span>");
            //    valid = false;
            //}

            var custType = $.trim($("#ddlType", form).val());
            if (custType == null || custType == "") {
                if (focus == null) {
                    focus = $("#ddlType", form);
                }
                $("#ddlType", form).parent("div").parent("div").addClass("error");
                $("#ddlType", form).next('span.help-inline').remove();
                $("#ddlType", form).parent("div").append("<span class=\"help-inline no-left-padding\">Choose Type Of Customer.</span>");
                valid = false;
            }

            var city = $.trim($("#ddlCityEdit", form).val());
            if (city == null || city == "") {
                if (focus == null) {
                    focus = $("#ddlCityEdit", form);
                }
                $("#ddlCityEdit", form).parent("div").parent("div").addClass("error");
                $("#ddlCityEdit", form).next('span.help-inline').remove();
                $("#ddlCityEdit", form).parent("div").append("<span class=\"help-inline no-left-padding\">Province/City is required.</span>");
                valid = false;
            }

            //var district = $.trim($("#ddlDistrict", form).val());
            //if (district == null || district == "") {
            //    if (focus == null) {
            //        focus = $("#ddlDistrict", form);
            //    }
            //    $("#ddlDistrict", form).parent("div").parent("div").addClass("error");
            //    $("#ddlDistrict", form).next('span.help-inline').remove();
            //    $("#ddlDistrict", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter Kota/Kab.</span>");
            //    valid = false;
            //}

            var email = $.trim($("#txtEmail", form).val());
            //if (email == null || email == "") {
            //    $("#txtEmail", form).parent("div").parent("div").parent("div").addClass("error");
            //    $("#txtEmail", form).parent("div").next('span.help-inline').remove();
            //    $("#txtEmail", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter Email Address.</span>");
            //    if (focus == null) {
            //        focus = $("#txtEmail", form);
            //    }
            //    valid = false;
            //}else
            if (email != '' && !sysvalid.email(email)) {
                if (focus == null) {
                    focus = $("#txtEmail", form);
                }
                $("#txtEmail", form).parent("div").parent("div").parent("div").addClass("error");
                $("#txtEmail", form).parent("div").next('span.help-inline').remove();
                $("#txtEmail", form).parent("div").append(" <span class=\"help-inline no-left-padding\">Invalid Email Address.</span>");
                valid = false;
            }

            var address = $('#txtAddress',form).val();
            if(address == null  || address==''){
                if (focus == null) {
                    focus = $("#txtAddress", form);
                }
                $("#txtAddress", form).parent("div").parent("div").addClass("error");
                $("#txtAddress", form).parent("div").next('span.help-inline').remove();
                $("#txtAddress", form).parent("div").append(" <span class=\"help-inline no-left-padding\">Address is not empty.</span>");
                valid = false;
            }

            if (focus != null) {
                focus.focus();
                syscommon.scroll(focus);
            }
        } catch (e) {
            sysmess.error('Lỗi javascript!<br />' + e);
            valid = false;
        }
        return valid;
    }
};
var profile_create = {
    init: function () {
        $(document).ready(function () {
            var form = $("#frmCreateProfile");
            $('.number').inputmask("decimal", {
                autoGroup: true,
                groupSeparator: ",",
                groupSize: 13,
                rightAlignNumerics: false,
                repeat: 13
            });
            $("#lnkAddProfile", form).off("click");
            $("#lnkAddProfile", form).on("click", function () {
                var html = "";
                html += '<tr>';
                html += '<td>';
                html += '<input name="Name" type="text" class="m-wrap span12" value="" placeholder="--Name--"/>';
                html += '</td>';
                html += '<td>';
                html += '<input name="PhoneNumber" type="number" class="m-wrap span12 phone-number" value="" placeholder="--PhoneNumber-"/>';
                html += '</td>';
                html += '<td>';
                html += '<input name="PhoneAlias" type="text" class="m-wrap span12" value="" placeholder="--PhoneAlias--" />';
                html += '</td>';
                html += '<td>';
                html += '<input name="Email" type="text" class="m-wrap span12" value="" placeholder="--Email--" />';
                html += '</td>';
                html += '<td>';
                html += '<input type=\"hidden\" value="0"/>';
                html += '<a href="javascript:;" class=\"btn red icn-only\" title=\"Cancel\"><i class=\"icon-remove icon-white\"></i></a>';
                html += '</td>';
                html += '</tr>';
                $("#tblProfile tbody").append(html);
                $('.number').inputmask("decimal", {
                    autoGroup: true,
                    groupSeparator: ",",
                    groupSize: 13,
                    rightAlignNumerics: false,
                    repeat: 13
                });
                profile_create.push_action();
            });

            $("#btnSaveProfile", form).off("click").on("click", function () {
                var phoneNumber = "";
                var arrProfile = new Array();
                $('#tblProfile tbody tr').each(function () {
                    arrProfile.push({
                        Name: $('td:eq(0) input', $(this)).val(),
                        PhoneNumber: $('td:eq(1) input', $(this)).val(),
                        PhoneAlias: $('td:eq(2) input', $(this)).val(),
                        Email: $('td:eq(3) input', $(this)).val()
                    });

                    if (phoneNumber == "") {
                        phoneNumber = $('td:eq(1) input', $(this)).val() + "(" + $('td:eq(0) input', $(this)).val() + ")";
                    }
                    else {
                        phoneNumber = phoneNumber + "; " + $('td:eq(1) input', $(this)).val() + "(" + $('td:eq(0) input', $(this)).val() + ")";
                    }
                });
                if (profile_create.validate(arrProfile, form)) {
                    var data = {
                        ListProfile: arrProfile,
                        CustomerId: 0
                    }
                    var option = {
                        controller: "Profile",
                        action: "CheckPhoneNumber",
                        data: JSON.stringify(data),
                        callback: function (response) {
                            if (response.Code == ResponseCode.ErrorExist) {
                                $("#tblProfile", form).attr('style', 'border-color: #b94a48; border-left: 1px solid #b94a48');
                                $("#tblProfile", form).parent("div").append(" <span class=\"help-inline no-left-padding w300\" style=\"color: #b94a48;\">Phone Number "+ response.Data +" exist in other file customer.</span>");
                            }
                            else
                            {
                                $("button.close", form).trigger("click");
                                if (phoneNumber != "") {
                                    $("#txtPhoneNumber", $("#frmCreateCustomer")).val(phoneNumber);
                                }                                
                            }
                        }
                    };
                    sysrequest.send(option);                                      
                }
            });
        });
    },
    push_action: function () {
        $('#tblProfile tbody tr').each(function () {
            var current = $(this);
            $("a", current).off("click");
            $("a", current).on("click", function () {
                var a = $(this);
                a.parent().parent().remove();
            });
        });
    },
    validate: function (data, form) {
        var result = true;
        var focus = null;
        if (data.length > 0) {
            $('#tblProfile tbody tr', form).each(function () {
                if ($('td:eq(0) input', $(this)).val() == '' || $('td:eq(0) input', $(this)).val() == null) {
                    result = false;
                    $('td:eq(0) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(0)', $(this)).append(" <span class=\"help-inline no-left-padding w150\" style=\"color: #b94a48\">Enter Customer Name.</span>");
                    focus = $('td:eq(0) input', $(this));
                }
                if ($('td:eq(1) input', $(this)).val() == '' || $('td:eq(1) input', $(this)).val() == null) {
                    result = false;
                    $('td:eq(1) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(1)', $(this)).append(" <span class=\"help-inline no-left-padding w160\" style=\"color: #b94a48\">Enter Phone Number.</span>");
                    if (focus == null) {
                        focus = $('td:eq(1) input', $(this));
                    }
                }
                var email = $.trim($('td:eq(3) input', $(this)).val());
                if (email != '' && !sysvalid.email(email)) {
                    $('td:eq(3) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(3)', $(this)).append(" <span class=\"help-inline no-left-padding w160\" style=\"color: #b94a48\">Invalid Email Address.</span>");
                    result = false;
                    if (focus == null) {
                        focus = $('td:eq(3) input', $(this));
                    }
                }
            });
        }
        else {
            result = false;
            $("#tblProfile", form).attr('style', 'border-color: #b94a48; border-left: 1px solid #b94a48');
            $("#tblProfile", form).parent("div").append(" <span class=\"help-inline no-left-padding w300\" style=\"color: #b94a48;\">Enter Infomation Phone Number.</span>");
        }
        if (focus != null) {
            focus.focus();
            syscommon.scroll(focus);
        }
        return result;
    },
    clear_error: function (form) {
        $("#tblProfile", form).removeAttr('style');
        $('#tblProfile', form).next('span.help-inline').remove();

        $("#tblProfile tbody tr").each(function () {
            $("td span.help-inline", $(this)).remove();
            $("td input[type='text']", $(this)).each(function () {
                $(this).removeAttr('style');
            });
        });
    }
};
var profile_list = {
    init: function(){
        $(document).ready(function () {
            var form = $("#frmManageProfile");
            profile_list.push_action();
            profile_list.clear_error(form);
            $('.number').inputmask("decimal", {
                autoGroup: true,
                groupSeparator: ",",
                groupSize: 13,
                rightAlignNumerics: false,
                repeat: 13
            });
            $("#lnkAddNew", form).off("click");
            $("#lnkAddNew", form).on("click", function () {
                var html = "";
                html += '<tr>';
                html += '<td>';
                html += '<input name="Name" type="text" class="m-wrap span12" value="" placeholder="--Name--"/>';
                html += '</td>';
                html += '<td>';
                html += '<input name="PhoneNumber" type="number" class="m-wrap span12 phone-number" value="" placeholder="--PhoneNumber-"/>';
                html += '</td>';
                html += '<td>';
                html += '<input name="PhoneAlias" type="text" class="m-wrap span12" value="" placeholder="--PhoneAlias--" />';
                html += '</td>';
                html += '<td>';
                html += '<input name="Email" type="text" class="m-wrap span12" value="" placeholder="--Email--" />';
                html += '</td>';
                html += '<td>';
                html += '<input type=\"hidden\" value="0"/>';
                html += '<a href="javascript:;" class=\"btn red icn-only\" title=\"Cancel\"><i class=\"icon-remove icon-white\"></i></a>';
                html += '</td>';
                html += '</tr>';
                $("#tblManageProfile tbody").append(html);
                $('.number').inputmask("decimal", {
                    autoGroup: true,
                    groupSeparator: ",",
                    groupSize: 13,
                    rightAlignNumerics: false,
                    repeat: 13
                });
            });

            $("#btnSaveManageProfile", form).off("click").on("click", function () {
                var phoneNumber = "";
                var arrProfile = new Array();
                $('#tblManageProfile tbody tr').each(function () {
                    arrProfile.push({
                        Id: $('td:eq(4) input', $(this)).val(),
                        Name: $('td:eq(0) input', $(this)).val(),
                        PhoneNumber: $('td:eq(1) input', $(this)).val(),
                        PhoneAlias: $('td:eq(2) input', $(this)).val(),
                        Email: $('td:eq(3) input', $(this)).val()
                    });
                    if (phoneNumber == "") {
                        phoneNumber = $('td:eq(1) input', $(this)).val() + "(" + $('td:eq(0) input', $(this)).val() + ")";
                    }
                    else {
                        phoneNumber = phoneNumber + "; " + $('td:eq(1) input', $(this)).val() + "(" + $('td:eq(0) input', $(this)).val() + ")";
                    }
                });
                if (profile_list.validate(arrProfile, form)) {
                    if (phoneNumber != "") {
                        var data = {
                            CustomerId: $("#hiddCustomerId", form).val(),
                            PhoneNumber: phoneNumber,
                            ListProfile: arrProfile
                        }
                        var option = {
                            controller: "Profile",
                            action: "Save",
                            data: JSON.stringify(data),
                            callback: function (response) {
                                $("button.close", form).trigger("click");
                                //switch (response.Code) {
                                //    case ResponseCode.Success:
                                //        sysmess.info("Successfully!");
                                //        customer_index.search(1);
                                //        break;
                                //    case ResponseCode.Error:
                                //        sysmess.error("Error: " + response.Message);
                                //        sysmess.log(response.Message);
                                //        break;
                                //    case ResponseCode.Error:
                                //        sysmess.warning("Invalid Data");
                                //        sysmess.log(response.Message);
                                //        break;
                                //    case ResponseCode.NotValid:
                                //        sysmess.warning(response.Message);
                                //        sysmess.log(response.Message);
                                //        break;
                                //}
                                if(response.Code == ResponseCode.Success ){
                                    sysmess.info(response.Message);
                                }
                                else if(response.Code == ResponseCode.Error){
                                    sysmess.error(response.Message);
                                }
                                else{
                                    sysmess.warning(response.Message);
                                }
                            }
                        };
                        sysrequest.send(option);
                    }                    
                }
            });
        });
    },
    push_action: function () {
        $('#tblManageProfile tbody tr').each(function () {
            var current = $(this);
            $("a", current).off("click");
            $("a", current).on("click", function () {
                var a = $(this);
                a.parent().parent().remove();
            });
        });
    },
    validate: function (data, form) {
        var result = true;
        var focus = null;
        if (data.length > 0) {
            $('#tblManageProfile tbody tr', form).each(function () {
                if ($('td:eq(0) input', $(this)).val() == '' || $('td:eq(0) input', $(this)).val() == null) {
                    result = false;
                    $('td:eq(0) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(0)', $(this)).append(" <span class=\"help-inline no-left-padding w150\" style=\"color: #b94a48\">Enter Customer Name.</span>");
                    focus = $('td:eq(0) input', $(this));
                }
                if ($('td:eq(1) input', $(this)).val() == '' || $('td:eq(1) input', $(this)).val() == null) {
                    result = false;
                    $('td:eq(1) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(1)', $(this)).append(" <span class=\"help-inline no-left-padding w160\" style=\"color: #b94a48\">Enter Phone Number.</span>");
                    if (focus == null) {
                        focus = $('td:eq(1) input', $(this));
                    }
                }
                var email = $.trim($('td:eq(3) input', $(this)).val());
                if (email != '' && !sysvalid.email(email)) {
                    $('td:eq(3) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(3)', $(this)).append(" <span class=\"help-inline no-left-padding w160\" style=\"color: #b94a48\">Invalid Email Address.</span>");
                    result = false;
                    if (focus == null) {
                        focus = $('td:eq(3) input', $(this));
                    }
                }
            });
        }
        else {
            result = false;
            $("#tblManageProfile", form).attr('style', 'border-color: #b94a48; border-left: 1px solid #b94a48');
            $("#tblManageProfile", form).parent("div").append(" <span class=\"help-inline no-left-padding w300\" style=\"color: #b94a48;\">Enter Infomation Phone Number.</span>");
        }
        if (focus != null) {
            focus.focus();
            syscommon.scroll(focus);
        }
        return result;
    },
    clear_error: function (form) {
        $("#tblManageProfile", form).removeAttr('style');
        $('#tblManageProfile', form).next('span.help-inline').remove();

        $("#tblManageProfile tbody tr").each(function () {
            $("td span.help-inline", $(this)).remove();
            $("td input[type='text']", $(this)).each(function () {
                $(this).removeAttr('style');
            });
        });
    }
};
