﻿var account_logon = {
    init: function (status) {
        $(document).ready(function () {
            $(".alert-error").addClass("hide");
            $(".alert-error span").html("Enter your account infomation.");
            if (status == "False") {
                $(".alert-error").removeClass("hide");
                $(".alert-error span").html("Username or Pasword is not correct.");
            }
            $("#btnLogOn").off("click");
            $("#btnLogOn").on("click", function () {
                $(".alert-error").addClass("hide");
                $(".alert-error span").html("Enter your account infomation.");
                $("#frmLogOn").submit();
            });
        });
    }
}
 