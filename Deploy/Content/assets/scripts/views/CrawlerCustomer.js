﻿var crawler_customer_index = {
    curr_tr_element:null,
    init: function () {
        $(document).ready(function () {
            crawler_customer_index.action();
            crawler_customer_index.note_customer_action();
            crawler_customer_index.search(1);
        });
    },
    search: function (page) {
        var frm_search = $('#frmSearchCrawlerCustomer');
        var text_filterkeyword = $('textarea[name=FilterKeyword]',frm_search);
        var select_assignto = $('select[name=AssignTo]',frm_search);
        var assignTo = select_assignto.val();
        if(assignTo == null ){
            assignTo = '';
        }
        var select_status = $('select[name=Status]',frm_search);
        var date_dateofcreationfrom = $('input[name=DateOfCreationFrom]',frm_search);
        var date_dateofcreationto = $('input[name=DateOfCreationTo]',frm_search);

        if(page == null){
            page =1;
        }
        var option = {
            controller : 'CrawlerCustomer',
            action:'Search',
            data:{
                FilterKeyword : text_filterkeyword.val(),
                Status : select_status.val(),
                DateOfCreationFrom :date_dateofcreationfrom.val() ,
                DateOfCreationTo : date_dateofcreationto.val(),
                AssignTo : assignTo,
                PageIndex : page,
                PageSize : 25
            },
            dataType:'html',
            callback: function () {
                crawler_customer_list.action();
            },
            element:$('#divListCrawlerCustomer')
        };
        sysrequest.send(option);
    },
    action: function () {
        var frm_search = $('#frmSearchCrawlerCustomer');
        $('button[name=search]',frm_search).off('click').on('click', function () {
            crawler_customer_index.search(1);
        });
        $('button[name=reset]',frm_search).off('click').on('click', function () {
            $('textarea[name=FilterKeyword]',frm_search).val('');
            $('select[name=AssignTo]',frm_search).val('-1');
            $('select[name=AssignTo]',frm_search).trigger('liszt:updated');
            $('select[name=Status]',frm_search).val('-1');
            $('select[name=Status]',frm_search).trigger('liszt:updated');
            $('input[name=DateOfCreationFrom]',frm_search).val('');
            $('input[name=DateOfCreationTo]',frm_search).val('');
        });
        frm_search.off('keydown').on('keydown', function (e) {
            crawler_customer_index.hitting_enter(e);
        });
    },
    hitting_enter: function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $(this).blur();
            crawler_customer_index.search(1);
        }
    },
    success_contact_action: function () {
        // wrapContactSuccess
        var wrapContactSuccess = $('#wrapContactSuccess');
        $('select[name=RegionId]',wrapContactSuccess).off('change').on('change', function () {
            var regionId = $(this).val();
            var cityId = 0;
            if(regionId > 0){
                var option = {
                    controller: "City",
                    action: "GetCityByRegion",
                    data: {
                        regionId: regionId
                    },
                    callback: function (response) {
                        if (response.Code == ResponseCode.Success) {
                            $("select[name=CityId]", wrapContactSuccess).empty();
                            if (response.Data.length > 0) {
                                cityId = response.Data[0].Id;
                                $("select[name=CityId]", wrapContactSuccess).append('<option value="">--City--</option>');
                            }
                            var html = "";
                            for (var i = 0; i < response.Data.length; i++) {
                                html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                            }
                            $("select[name=CityId]", wrapContactSuccess).append(html);

                        } else {
                            sysmess.error(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
            if (cityId > 0) {
                var option = {
                    controller: "City",
                    action: "GetDistrictByCity",
                    data: {
                        cityId: cityId
                    },
                    callback: function (response) {
                        if (response.Code == ResponseCode.Success) {
                            $("select[name=DistrictId]", wrapContactSuccess).empty();
                            if (response.Data.length > 0) {
                                $("select[name=DistrictId]", wrapContactSuccess).append('<option value="">--District--</option>');
                            }
                            var html = "";
                            for (var i = 0; i < response.Data.length; i++) {
                                html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                            }
                            $("select[name=DistrictId]", wrapContactSuccess).append(html);

                        } else {
                            sysmess.error(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });
        $("select[name=CityId]", wrapContactSuccess).off("change").on("change", function () {
            var cityId = $(this).val();
            if (cityId > 0) {
                var option = {
                    controller: "City",
                    action: "GetDistrictByCity",
                    data: {
                        cityId: cityId
                    },
                    callback: function (response) {
                        if (response.Code == ResponseCode.Success) {
                            $("select[name=DistrictId]", wrapContactSuccess).empty();
                            if (response.Data.length > 0) {
                                $("select[name=DistrictId]", wrapContactSuccess).append('<option value="">--District--</option>');
                            }
                            var html = "";
                            for (var i = 0; i < response.Data.length; i++) {
                                html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                            }
                            $("select[name=DistrictId]", wrapContactSuccess).append(html);

                        } else {
                            sysmess.error(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });

        $('button[name=save]',wrapContactSuccess).off('click').on('click', function () {
            $("span.help-inline", wrapContactSuccess).each(function () {
                $(this).parent("div").parent("div").removeClass("error");
                $(this).remove();
            });

            var checkValue = crawler_customer_index.success_contact_valid_data();

            if(checkValue){
                var data = {
                    FullName: $("input[name=FullName]", wrapContactSuccess).val(),
                    PhoneNumber: $("select[name=PhoneNumber]", wrapContactSuccess).val(),
                    Type: $("input[name=Type]", wrapContactSuccess).val(),
                    Email: $("input[name=Email]", wrapContactSuccess).val(),
                    Description: $('#txaDescription',wrapContactSuccess).val(),
                    CityId: $("select[name=CityId]", wrapContactSuccess).val(),
                    DistrictId: $("select[name=DistrictId]", wrapContactSuccess).val(),
                    RegionId: $("select[name=RegionId]", wrapContactSuccess).val(),
                    CompanyId: $('select[name=CompanyId]',wrapContactSuccess).val(),
                    Address:$('input[name=Address]',wrapContactSuccess).val(),
                    CrawlerId : $(this).data('adminid'),
                    CareHistoryId : $(this).data('carehistoryid')
                };

                var option = {
                    controller: "CrawlerCustomer",
                    action: "ContactSuccess",
                    data: data,
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success){
                            $("#divBodyListCrawlerCustomer").removeClass("hide");
                            $("#wrapContactSuccess").addClass("hide");
                            crawler_customer_index.search(1);
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error("Error: " + response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });
        $('button[name=close]',wrapContactSuccess).off('click').on('click', function () {
            $('#divBodyListCrawlerCustomer').removeClass('hide');
            $('#wrapContactSuccess').addClass('hide');
        });
    },
    success_contact_valid_data: function () {
        var valid = true;
        try {
            var focus = null;
            var form = $('#frmSuccessContact');

            var textFullName = $('input[name=FullName]', form);

            var fullName = $.trim(textFullName.val());
            if (fullName == null || fullName == "") {
                focus = textFullName;
                textFullName.parent("div").parent("div").addClass("error");
                textFullName.next('span.help-inline').remove();
                textFullName.parent("div").append("<span class=\"help-inline no-left-padding\">Enter FullName of customer.</span>");
                valid = false;
            }

            var textPhoneNumber =$("select[name=PhoneNumber]", form);

            var phoneNumber = $.trim(textPhoneNumber.val());
            if (phoneNumber == null || phoneNumber == "") {
                textPhoneNumber.parent("div").parent("div").addClass("error");
                textPhoneNumber.next('span.help-inline').remove();
                textPhoneNumber.parent("div").append("<span class=\"help-inline no-left-padding\">Enter Phone Number.</span>");
                valid = false;
            }

            var selectType = $("select[name=Type]", form);

            var custType = $.trim(selectType.val());
            if (custType == null || custType == "") {
                if (focus == null) {
                    focus = $("#ddlType", form);
                }
                selectType.parent("div").parent("div").addClass("error");
                selectType.next('span.help-inline').remove();
                selectType.parent("div").append("<span class=\"help-inline no-left-padding\">Enter Type Of Customer.</span>");
                valid = false;
            }

            var selectCity = $("select[name=CityId]", form);
            var city = $.trim(selectCity.val());
            if (city == null || city == "") {
                if (focus == null) {
                    focus = $("#ddlCity", form);
                }
                selectCity.parent("div").parent("div").addClass("error");
                selectCity.next('span.help-inline').remove();
                selectCity.parent("div").append("<span class=\"help-inline no-left-padding\">Enter Province.</span>");
                valid = false;
            }

            var textEmail = $("input[name=Email]", form);

            var email = textEmail.val();
            if(email != null && email != ''){
                email = $.trim(email);
                if(email != '' && !sysvalid.email(email) ){
                    if (focus == null) {
                        focus = textEmail;
                    }
                    textEmail.parent("div").parent("div").parent("div").addClass("error");
                    textEmail.parent("div").next('span.help-inline').remove();
                    textEmail.parent("div").append(" <span class=\"help-inline no-left-padding\">Invalid Email Address.</span>");
                    valid = false;
                }
            }
            //else{
            //    if (focus == null) {
            //        focus = textEmail;
            //    }
            //    valid = false;
            //    textEmail.parent("div").parent("div").parent("div").addClass("error");
            //    textEmail.parent("div").next('span.help-inline').remove();
            //    textEmail.parent("div").append(" <span class=\"help-inline no-left-padding\">Email is required.</span>");
            //}

            var textAddress = $('input[name=Address]',form);

            var address = textAddress.val();
            if(address == null  || address==''){
                if (focus == null) {
                    focus = textAddress;
                }
                textAddress.parent("div").parent("div").addClass("error");
                textAddress.parent("div").next('span.help-inline').remove();
                textAddress.parent("div").append(" <span class=\"help-inline no-left-padding\">Address is not empty.</span>");
                valid = false;
            }

            //if (focus != null) {
            //    focus.focus();
            //    syscommon.scroll(focus);
            //}
        } catch (e) {
            sysmess.error('Error javascript!<br />' + e);
            valid = false;
        }
        return valid;
    },
    note_customer_action: function () {
        var wrapNote = $('#wrapNote');
        $('button[name=save]',wrapNote).off('click').on('click', function () {
            $("span.help-inline", wrapNote).each(function () {
                $(this).parent("div").parent("div").removeClass("error");
                $(this).remove();
            });

            var careHistoryId = $(this).data('carehistoryid');
            var textNote = $('textarea[name=note]',wrapNote);

            var note = textNote.val();
            if(note == null  || note == ''){
                textNote.parent().parent().addClass('error');
                textNote.next('span.help-inline').remove();
                textNote.parent("div").append("<span class=\"help-inline no-left-padding\">Note is required.</span>");
                return;
            }

            var option = {
                controller:'CrawlerCustomer',
                action:'Note',
                data:{
                    careHistoryId : careHistoryId,
                    note : note
                },
                callback: function (response) {
                    if(response.Code == ResponseCode.Success){
                        sysmess.info(response.Message);
                        $('td[name=wrapNoted]',crawler_customer_index.curr_tr_element).html(note);
                    }
                    else if(response.Code == ResponseCode.Error){
                        sysmess.error(response.Message);
                    }
                    else{
                        sysmess.warning(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        });
    }
};
var crawler_customer_list = {
    action: function () {
        var divBodyListCrawlerCustomer = $('#divBodyListCrawlerCustomer');
        $('a[name=receive]',divBodyListCrawlerCustomer).each(function () {
            var adminId = $(this).data('adminid');
            $(this).off('click').on('click', function () {
                var tr = $(this).parent().parent().parent().parent().parent();

                var option = {
                    controller:'CrawlerCustomer',
                    action:'Receive',
                    data:{
                        crawlerAdminId : adminId
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.info(response.Message);
                            $('td[name=wrapAssignTo]',tr).html(response.Data.Receiver );
                            var actionHtml   = '<div class="btn-group none-bottom">';
                                actionHtml  += '    <button class="btn w80 blue mini dropdown-toggle" data-toggle="dropdown">';
                                actionHtml  += '        <i class="icon-cog"></i>Manager<i class="icon-angle-down"></i>';
                                actionHtml  += '    </button>';
                                actionHtml  += '    <ul class="dropdown-menu pull-right">';
                                actionHtml  += '        <li>';
                                actionHtml  += '            <a name="contactsuccess" data-adminid="'+response.Data.CrawlerAdminId +'" data-carehistoryid="'+response.Data.CareHistoryId +'" title="Confirm customer to contact success">';
                                actionHtml  += '                <i class="icon-ok"></i> Contact success';
                                actionHtml  += '            </a>';
                                actionHtml  += '        </li>';
                                actionHtml  += '        <li>';
                                actionHtml  += '            <a name="contactfailure" data-adminid="'+response.Data.CrawlerAdminId +'" data-carehistoryid="'+response.Data.CareHistoryId +'" title="Confirm customer to contact failure">';
                                actionHtml  += '                <i class="icon-remove"></i> Contact failure';
                                actionHtml  += '            </a>';
                                actionHtml  += '        </li>';
                                actionHtml  += '        <li>';
                                actionHtml  += '            <a name="return" data-adminid="'+response.Data.CrawlerAdminId +'" data-carehistoryid="'+response.Data.CareHistoryId +'" title="Confirm customer to contact failure">';
                                actionHtml  += '                <i class="icon-hand-right"></i> Return Customer';
                                actionHtml  += '            </a>';
                                actionHtml  += '        </li>';
                                actionHtml  += '        <li>';
                                actionHtml  += '            <a name="note" data-adminid="'+response.Data.CrawlerAdminId +'" data-carehistoryid="'+response.Data.CareHistoryId +'" href="#wrapNote" data-toggle="modal" title="note">';
                                actionHtml  += '                <i class="icon-book"></i> Note';
                                actionHtml  += '            </a>';
                                actionHtml  += '        </li>';
                                actionHtml  += '    </ul>';
                                actionHtml  += '</div>';
                            $('td[name=wrapAction]',tr).html(actionHtml);
                            crawler_customer_list.action();
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysmess.confirm('Do you want to receive this customer ?', function () {
                    sysrequest.send(option);
                });
            });
        });
        $('a[name=contactsuccess]',divBodyListCrawlerCustomer).each(function () {
            $(this).off('click').on('click', function () {
                $('#divBodyListCrawlerCustomer').addClass('hide');
                $('#wrapContactSuccess').removeClass('hide');
                var tr = $(this).parent().parent().parent().parent().parent();
                var fullName = $('span[name=wrapFullName]',tr).text();
                var email = $('span[name=wrapEmail]',tr).text();
                var phoneNumber = $('td[name=wrapPhoneNumber]',tr).text();
                var address = $.trim($('span[name=wrapAddress]',tr).text()) ;
                var crawlerAdminId = $(this).data('adminid');
                var careHistoryId = $(this).data('carehistoryid');
                var option = {
                    controller:'CrawlerCustomer',
                    action:'ConfirmSuccessContact',
                    data:{
                        CrawlerId : crawlerAdminId,
                        FullName  : fullName,
                        PhoneNumber : phoneNumber,
                        Email : email,
                        CareHistoryId : careHistoryId,
                        Address : address
                    },
                    dataType:'html',
                    callback: function () {
                        crawler_customer_index.success_contact_action();
                    },
                    element:$('#wrapContactSuccess')
                };
                sysrequest.send(option);
            });
        });
        $('a[name=contactfailure]',divBodyListCrawlerCustomer).each(function () {
            $(this).off('click').on('click', function () {
                var tr = $(this).parent().parent().parent().parent().parent();

                var option = {
                    controller:'CrawlerCustomer',
                    action:'ContactFailure',
                    data:{
                        crawlerId : $(this).data('adminid'),
                        careHistoryId : $(this).data('carehistoryid')
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.info(response.Message);
                            var statusHtml  = '<span class="label label-important status">';
                                statusHtml += '  <i class="icon-ban-circle"></i>';
                                statusHtml += '  Reject   ';
                                statusHtml += '</span>';
                            $('td[name=wrapStatus]',tr).html(statusHtml);
                            $('a[name=contactsuccess]',tr).parent().remove();
                            $('a[name=contactfailure]',tr).parent().remove();
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysmess.confirm('Did this customer contact failure ?', function () {
                    sysrequest.send(option);
                });
            });
        });
        $('a[name=note]',divBodyListCrawlerCustomer).each(function () {
              $(this).off('click').on('click', function () {
                  var crawlerId = $(this).data('adminid');
                  var careHistoryId = $(this).data('carehistoryid');
                  $('button[name=save]',$('#wrapNote')).data('adminid',crawlerId);
                  $('button[name=save]',$('#wrapNote')).data('carehistoryid',careHistoryId);
                  $('textarea[name=note]',$('#wrapNote')).val('');
                  crawler_customer_index.curr_tr_element = $(this).parent().parent().parent().parent().parent();
              });
        });
        $('a[name=return]',divBodyListCrawlerCustomer).each(function () {
            $(this).off('click').on('click', function () {
                var tr = $(this).parent().parent().parent().parent().parent();
                var careHistoryId = $(this).data('carehistoryid');
                var option = {
                    controller:'CrawlerCustomer',
                    action:'Return',
                    data:{
                        careHistoryId :careHistoryId
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.info(response.Message);
                            $('td[name=wrapAssignTo]',tr).html('' );
                            var actionHtml   = '<div class="btn-group none-bottom">';
                            actionHtml  += '    <button class="btn w80 blue mini dropdown-toggle" data-toggle="dropdown">';
                            actionHtml  += '        <i class="icon-cog"></i>Manager<i class="icon-angle-down"></i>';
                            actionHtml  += '    </button>';
                            actionHtml  += '    <ul class="dropdown-menu pull-right">';
                            actionHtml  += '        <li>';
                            actionHtml  += '            <a name="receive" data-adminid="'+response.Data.CrawlerAdminId +'" data-carehistoryid="'+response.Data.CareHistoryId +'" title="Receive Crawler Customer">';
                            actionHtml  += '                <i class="icon-edit"></i> Receive';
                            actionHtml  += '            </a>';
                            actionHtml  += '        </li>';
                            actionHtml  += '    </ul>';
                            actionHtml  += '</div>';
                            $('td[name=wrapAction]',tr).html(actionHtml);
                            crawler_customer_list.action();
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysmess.confirm('Do you want to return this customer ?', function () {
                    sysrequest.send(option);
                });
            });
        });
    }
};