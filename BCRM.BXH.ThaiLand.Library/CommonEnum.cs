﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCRM.BXH.ThaiLand.Library
{
    public enum UserStatus
    {
        Avaiable = 1,
        NotAvaiable = 0
    }
    public enum CustomerStatus
    {
        /// <summary>
        /// Chưa sale
        /// </summary>
        Waiting = 1,
        /// <summary>
        /// Xem xét
        /// </summary>
        Indecivise = 2,
        /// <summary>
        /// Thành công
        /// </summary>
        Success = 3,
        /// <summary>
        /// Từ chối
        /// </summary>
        Rejected = 4
    }
    public enum OpportunityStatus
    {
        /// <summary>
        /// None
        /// </summary>
        None = 0,
        /// <summary>
        /// Đang tiến hành
        /// </summary>
        Running = 1,
        /// <summary>
        /// Hoàn Thành
        /// </summary>
        Success = 2,
        /// <summary>
        /// Từ chối
        /// </summary>
        Rejected = 3,
        /// <summary>
        /// Chờ xác nhận
        /// </summary>
        Waiting = 4
    }
    public enum TypeOfPayment
    {
        /// <summary>
        /// Chuyển khoản
        /// </summary>
        Banking = 1,
        /// <summary>
        /// Thu phí trực tiếp
        /// </summary>
        DirectMoney = 2,
        /// <summary>
        /// Nhắn tin sms
        /// </summary>
        Sms = 3,
        /// <summary>
        /// Thu phí bàn giao
        /// </summary>
        Handed = 4,
        /// <summary>
        /// Thẻ cào
        /// </summary>
        Card = 5,
        /// <summary>
        /// Bảo Kim
        /// </summary>
        BaoKim = 6
    }
    public enum TypeOfCustomer : int
    {
        /// <summary>
        /// Salon
        /// </summary>
        Showroom = 0,
        /// <summary>
        /// Môi giới
        /// </summary>
        Dealer = 1,
        /// <summary>
        /// Chính chủ
        /// </summary>
        Private = 2        
    }
    /// <summary>
    /// Trạng thái gọi điện
    /// </summary>
    public enum PhoningStatus
    {
        /// <summary>
        /// Chưa liên lạc
        /// </summary>
        NoContact = 0,
        /// <summary>
        /// Liên lạc không thành công 
        /// </summary>
        Failure = 1,
        /// <summary>
        /// Liên lạc thành công
        /// </summary>
        Success = 2
    }

    public enum ContactStatus
    {
        NotCalled = 0,
        Accepted = 1,
        Refused = 2
    }
    /// <summary>
    /// Trạng thái kích hoạt của tài khoản khách hàng
    /// </summary>
    public enum CustomerAccountActiveStatus
    {
        Nonactived = 0,
        Actived = 1
    }
}
