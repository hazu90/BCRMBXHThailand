﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCRM.BXH.ThaiLand.Library
{
    public static partial class Helper
    {
        private static readonly List<KeyValuePair<int, string>> CustomerStatusName = new List<KeyValuePair<int, string>> 
        {  
            new KeyValuePair<int, string>(CustomerStatus.Waiting.GetHashCode(), "Waiting"),
            new KeyValuePair<int, string>(CustomerStatus.Indecivise.GetHashCode(), "Indecivise"),
            new KeyValuePair<int, string>(CustomerStatus.Success.GetHashCode(), "Success"),
            new KeyValuePair<int, string>(CustomerStatus.Rejected.GetHashCode(), "Rejected")
        };

        private static readonly List<KeyValuePair<int, string>> TypeOfCustomerName = new List<KeyValuePair<int, string>> 
        {  
            new KeyValuePair<int, string>(TypeOfCustomer.Showroom.GetHashCode(), "Brand"),
            new KeyValuePair<int, string>(TypeOfCustomer.Dealer.GetHashCode(), "Broker"),
            new KeyValuePair<int, string>(TypeOfCustomer.Private.GetHashCode(), "Owner")            
        };

        private static readonly List<KeyValuePair<int, string>> TypeOfSystemRole = new List<KeyValuePair<int, string>> 
        {
            new KeyValuePair<int, string>(6, "Sale")
           ,new KeyValuePair<int, string>(7, "Manager")
           ,new KeyValuePair<int, string>(8, "Admin")
           ,new KeyValuePair<int, string>(9, "Coordinate")
           ,new KeyValuePair<int, string>(10, "TeamLeader")
        };

        #region CustomerStatusName
        public static string ToCustomerStatusName(this CustomerStatus status)
        {
            var name = (from n in CustomerStatusName where n.Key == status.GetHashCode() select n.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(name))
            {
                return name;
            }
            else
            {
                return string.Empty;
            }
        }

        public static List<KeyValuePair<int, string>> ListCustomerStatusName()
        {
            return CustomerStatusName;
        }
        #endregion

        #region TypeOfCustomerName
        public static string ToTypeOfCustomerName(this TypeOfCustomer type)
        {
            var name = (from n in TypeOfCustomerName where n.Key == type.GetHashCode() select n.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(name))
            {
                return name;
            }
            else
            {
                return string.Empty;
            }
        }

        public static List<KeyValuePair<int, string>> ListTypeOfCustomerName()
        {
            return TypeOfCustomerName;
        }
        #endregion

        #region PermissionName
        public static string ToPermissionName(this int permission)
        {
            var name = (from n in TypeOfSystemRole where n.Key == permission select n.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(name))
            {
                return name;
            }
            else
            {
                return string.Empty;
            }
        }

        public static List<KeyValuePair<int, string>> ListPermissionName()
        {
            return TypeOfSystemRole;
        }
        #endregion
    }
}
