﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.ThaiLand.Library
{
    public class JsonModelBinder : ActionFilterAttribute
    {
        public JsonModelBinder()
        {
        }

        public Type ActionParameterType { get; set; }
        public string ActionParameterName { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpRequestBase request = filterContext.HttpContext.Request;
            Stream stream = request.InputStream;
            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);
            var json = HttpUtility.HtmlDecode(reader.ReadToEnd());
            var obj = Activator.CreateInstance(ActionParameterType);
            if (!string.IsNullOrEmpty(json) && Common.IsJsonFormat(json))
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject(json, ActionParameterType);
                filterContext.ActionParameters[ActionParameterName] = obj;
            }
        }
    }
    public static class Common
    {
        /// <summary>
        /// Auto mapping data from IDataReader to entity object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <param name="row"></param>
        public static void AutoMappingColumn<T>(T item, IDataReader row)
        {
            Type type = typeof(T);
            object objInstanceClass = Activator.CreateInstance(type);
            Type objInstanceMethod = objInstanceClass.GetType();
            var lstProperty = objInstanceMethod.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var property in lstProperty)
            {
                for (int i = 0; i < row.FieldCount; i++)
                {
                    if (property.Name.ToLower().Equals(row.GetName(i).ToLower()))
                    {
                        var obj = row[property.Name];
                        if (obj != null)
                        {
                            if (property.PropertyType.GetGenericArguments() != null &&
                                property.PropertyType.GetGenericArguments().Length > 0 &&
                                property.PropertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                            {
                                var genericType = property.PropertyType.GetGenericArguments()[0];
                                property.SetValue(item, Convert.ChangeType(obj, genericType), null);
                            }
                            else
                            {
                                if (property.PropertyType.IsEnum)
                                {
                                    property.SetValue(item, Enum.Parse(property.PropertyType, obj.ToString()), null);
                                }
                                else
                                {
                                    property.SetValue(item, Convert.ChangeType(obj, property.PropertyType), null);
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Wrap phone number
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static string WrapPhoneNumber(this string phone)
        {
            if (!string.IsNullOrEmpty(phone))
            {
                var lstPhoneNumber = phone.Split(';').Select(s => s.Trim());
                return string.Join("; ", lstPhoneNumber.ToList().FindAll(s => !string.IsNullOrWhiteSpace(s)));
            }
            else
            {
                return phone;
            }
        }

        /// <summary>
        /// Deserialize json string to Object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T ToDeserialize<T>(this string json)
        {
            var ret = default(T);
            if (!string.IsNullOrEmpty(json))
            {
                try
                {
                    ret = JsonConvert.DeserializeObject<T>(json);
                }
                catch
                {
                    return ret;
                }
            }
            return ret;
        }

        /// <summary>
        /// Convert object dynamic to ExpandoObject
        /// </summary>
        /// <param name="anonymousObject"></param>
        /// <returns></returns>
        public static ExpandoObject ToExpando(this object anonymousObject)
        {
            IDictionary<string, object> anonymousDictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(anonymousObject);
            IDictionary<string, object> expando = new ExpandoObject();
            foreach (var item in anonymousDictionary)
                expando.Add(item);
            return (ExpandoObject)expando;
        }

        /// <summary>
        /// Extension convert datetime to int
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int ToInt32(this DateTime dt)
        {
            return int.Parse(string.Format("{0:yyyyMMdd}", dt));
        }

        /// <summary>
        /// Detect a string be a json format
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static bool IsJsonFormat(string json)
        {
            if (string.IsNullOrEmpty(json)) return false;

            var reg1 = new Regex("[[][{].*?[}][]]", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            var reg2 = new Regex("[^[]*[{].*?[}][^]]*", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            if (reg1.IsMatch(json) || reg2.IsMatch(json)) return true;
            else return false;
        }

        /// <summary>
        /// Convert long value to money format
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToMoneyFormat(this long value)
        {
            if (Math.Abs(value) < 1000)
            {
                return value.ToString();
            }
            else
            {
                return value.ToString("#,###,###");
            }
        }

        /// <summary>
        /// Convert int value to money format
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToMoneyFormat(this int value)
        {
            return ((long)value).ToMoneyFormat();
        }

        /// <summary>
        /// Render partial view to string
        /// </summary>
        /// <param name="context"></param>
        /// <param name="viewName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string RenderPartialViewToString(ControllerContext context, string viewName, object model)
        {
            context.Controller.ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(context, viewName);
                ViewContext viewContext = new ViewContext(context, viewResult.View, context.Controller.ViewData, context.Controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.ToString();
            }
        }

        /// <summary>
        /// Render view to string
        /// </summary>
        /// <param name="context"></param>
        /// <param name="viewName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string RenderViewToString(ControllerContext context, string viewName, object model)
        {
            context.Controller.ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindView(context, viewName, null);
                ViewContext viewContext = new ViewContext(context, viewResult.View, context.Controller.ViewData, context.Controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.ToString();
            }
        }
        public static string GetEnumDescription(Enum value)
        {
            try
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());
                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes != null && attributes.Length > 0)
                    return attributes[0].Description;
                else
                    return value.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }
        //Created by Thuyetnt
        public static string MakePostRequest(string url, string data, string contentType = null, bool keepAlive = true)
        {
            try
            {
                Logger.GetInstance().Write(url);

                byte[] byteArray = Encoding.UTF8.GetBytes(data);
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = string.IsNullOrEmpty(contentType) ? "application/x-www-form-urlencoded" : contentType;
                request.ContentLength = byteArray.Length;
                request.KeepAlive = keepAlive;
                request.Timeout = Timeout.Infinite;
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                var dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                //Get response
                WebResponse response = request.GetResponse();
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromApi = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                return responseFromApi;
            }
            catch (Exception ex)
            {
                Logger.GetInstance().Write(url);
                Logger.GetInstance().Write(string.Format("Make Post Request: {0}", ex.Message));
                Logger.GetInstance().Write(string.Format("Make Post Request: {0}", ex.StackTrace));
                return string.Empty;
            }
        }
    }
}
