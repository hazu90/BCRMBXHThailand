﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BCRM.BXH.ThaiLand.Library
{
    public class EntityBase
    {
        private readonly Dictionary<string, object> _extendedProperties = new Dictionary<string, object>();

        public object this[string propertyName]
        {
            get
            {
                return (_extendedProperties.ContainsKey(propertyName) ? _extendedProperties[propertyName] : null);
            }
            set
            {
                if (_extendedProperties.ContainsKey(propertyName))
                {
                    _extendedProperties[propertyName] = value;
                }
                else
                {
                    _extendedProperties.Add(propertyName, value);
                }
            }
        }

        public static bool SetObjectValue<T>(IDataRecord reader, ref T entity) where T : class
        {
            Type type = typeof(T);

            if (entity != null)
            {
                for (var i = 0; i < reader.FieldCount; i++)
                {
                    var fieldName = reader.GetName(i);
                    try
                    {

                        var t = type.GetProperties().FirstOrDefault();
                        var propertyInfo =
                            type.GetProperties().FirstOrDefault(
                                info => info.Name.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase));
                        if (propertyInfo != null)
                        {
                            if ((reader[i] != null) && (reader[i] != DBNull.Value))
                            {
                                propertyInfo.SetValue(entity, reader[i], null);
                            }
                            else
                            {
                                if (propertyInfo.PropertyType == typeof(DateTime) ||
                                    propertyInfo.PropertyType == typeof(DateTime?))
                                {
                                    propertyInfo.SetValue(entity, DateTime.MinValue, null);
                                }
                                else if (propertyInfo.PropertyType == typeof(string))
                                {
                                    propertyInfo.SetValue(entity, string.Empty, null);
                                }
                                else if (propertyInfo.PropertyType == typeof(bool) ||
                                    propertyInfo.PropertyType == typeof(bool?))
                                {
                                    propertyInfo.SetValue(entity, false, null);
                                }
                                else if (propertyInfo.PropertyType == typeof(decimal) ||
                                    propertyInfo.PropertyType == typeof(decimal?))
                                {
                                    propertyInfo.SetValue(entity, decimal.Zero, null);
                                }
                                else if (propertyInfo.PropertyType == typeof(double) ||
                                propertyInfo.PropertyType == typeof(double?))
                                {
                                    propertyInfo.SetValue(entity, double.Parse("0"), null);
                                }
                                else if (propertyInfo.PropertyType == typeof(float) ||
                           propertyInfo.PropertyType == typeof(float?))
                                {
                                    propertyInfo.SetValue(entity, 0, null);
                                }
                                else
                                {
                                    propertyInfo.SetValue(entity, 0, null);
                                }
                            }
                        }
                        else
                        {
                            (entity as EntityBase)[fieldName] = reader[i];
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }        
    }

    public static class Ultility
    {
        /// <summary>
        /// Convert object to <c>int</c> value if exception occur return default value(if defaultValue == null return 0)
        /// </summary>
        /// <param name="obj">obj value to convert</param>
        /// <param name="defaultValue">default return value</param>
        /// <returns></returns>
        public static int ToInt(this object obj, int? defaultValue)
        {
            int ret = defaultValue ?? 0;
            try
            {
                ret = Convert.ToInt32(obj);
            }
            catch
            {
                ;
            }

            return ret;
        }

        /// <summary>
        /// Convert object to <c>long</c> value if exception occur return default value(if defaultValue == null return 0)
        /// </summary>
        /// <param name="obj">obj value to convert</param>
        /// <param name="defaultValue">default return value</param>
        /// <returns></returns>
        public static long ToLong(this object obj, long? defaultValue)
        {
            long ret = defaultValue ?? 0;
            try
            {
                ret = Convert.ToInt64(obj);
            }
            catch
            {
                ;
            }

            return ret;
        }

        public static DateTime? FromVnDateTimeToUtc(this string vnDateTime, char splitChar, DateTime? defaultValue, params int[] hour)
        {
            if (!string.IsNullOrEmpty(vnDateTime))
            {
                var arrDateTime = vnDateTime.Split(splitChar);
                DateTime utcDateTime;
                try
                {
                    utcDateTime = hour.Length == 2 ?
                                        new DateTime(int.Parse(arrDateTime[2]), int.Parse(arrDateTime[1]), int.Parse(arrDateTime[0]), hour[0], hour[1], 0)
                                        : new DateTime(int.Parse(arrDateTime[2]), int.Parse(arrDateTime[1]), int.Parse(arrDateTime[0]));
                }
                catch
                {
                    return defaultValue;
                }
                return utcDateTime;
            }
            return null;
        }

        public static List<string> DetectPhoneNumber(string input)
        {
            var lstPhone = new List<string>();
            if (string.IsNullOrEmpty(input)) return lstPhone;
            input = Regex.Replace(input.Trim(), "(\\+84)([0-9]+)", delegate(Match m)
            {
                return "0" + m.Groups[2].Value;
            }, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            input = Regex.Replace(input, "[.,]", string.Empty, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            var reg = new Regex("([0-9]+)", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            MatchCollection matches = reg.Matches(input);
            foreach (Match m in matches)
            {
                var phone = m.Groups[0].Value;
                if (!string.IsNullOrEmpty(phone) && phone.Length > 6)
                {
                    var one = phone.Substring(0, 1);
                    var two = phone.Substring(1, 1);
                    if ("0".Equals(one))
                    {
                        if ("9".Equals(two) || "1".Equals(two))
                        {
                            if (phone.Length > 9 && !lstPhone.Contains(phone))
                            {
                                lstPhone.Add(phone);
                            }
                        }
                        else
                        {
                            if (phone.Length > 8 && !lstPhone.Contains(phone))
                            {
                                lstPhone.Add(phone);
                            }
                        }
                    }
                    else
                    {
                        if (!lstPhone.Contains(phone))
                        {
                            lstPhone.Add(phone);
                        }
                    }
                }
            }
            return lstPhone;
        }

        public static string GetMd5x2(string str)
        {
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            bytes = provider.ComputeHash(bytes);
            StringBuilder builder = new StringBuilder();
            foreach (byte num in bytes)
            {
                builder.Append(num.ToString("x2").ToLower());
            }
            return builder.ToString();
        }
    }
}
