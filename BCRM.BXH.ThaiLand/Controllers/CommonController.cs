﻿using BCRM.BXH.ThaiLand.Filter;
using BCRM.BXH.ThaiLand.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.ThaiLand.Controllers
{
    public class CommonController : BaseController
    {
        [Localization]
        public ActionResult Header()
        {
            return View(UserContext);
        }
        [Localization]
        public ActionResult EmptyHeader()
        {
            return View();
        }
        [Localization]
        public ActionResult Paging(Pager pager)
        {
            return View(pager);
        }
        [Localization]
        public ActionResult ErrorPage(HandleErrorInfo err)
        {
            return View(err);
        }
    }
}