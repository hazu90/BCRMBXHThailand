﻿using BCRM.BXH.ThaiLand.DAL;
using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Filter;
using BCRM.BXH.ThaiLand.Library;
using BCRM.BXH.ThaiLand.Models;
using DVS.Algorithm;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.ThaiLand.Controllers
{
    public class DetailAdminAccountStatisticController : BaseController
    {
        [FilterAuthorize]
        [Localization]
        public ActionResult Index()
        {
            // Lấy ra danh sách mà nhân viên có quyền 
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            var groupLayer = new GroupLayer();
            var isSearchAll = false;
            var lstGroup = new List<Group>();
            var lstUsers = new List<UserSearchModel>();
            if (UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
            {
                isSearchAll = true;
                var allGroups = groupLayer.GetAll();
                lstGroup = lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                                ? allGroups : allGroups.FindAll(o => lstRoledGroup.Contains(o.Id));
                var groupIds = string.Join(",", lstGroup.Select(o => o.Id).ToList());
                var userLayer = new UserLayer();
                lstUsers = userLayer.GetList(groupIds);
            }
            else
            {
                var groupInfo = groupLayer.GetById(UserContext.GroupId);
                lstGroup = new List<Group>() { groupInfo };
                lstUsers = new List<UserSearchModel>() { new UserSearchModel() { UserName = UserContext.UserName } };
            }
            var model = new DetailAdminAccountStatisticIndexModel();
            if (DateTime.Now.Day <= 10)
            {
                var beforeMonth = DateTime.Now.AddMonths(-1);
                model.TimeToCreationFrom = new DateTime(beforeMonth.Year, beforeMonth.Month, 1);
                model.TimeToCreationTo = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).AddDays(-1);
            }
            else
            {
                var afterMonth = DateTime.Now.AddMonths(1);
                model.TimeToCreationFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                model.TimeToCreationTo = (new DateTime(afterMonth.Year, afterMonth.Month, 1)).AddDays(-1);
            }
            model.SetListGroup(isSearchAll, lstGroup);
            model.SetListUsers(isSearchAll, lstUsers);
            return View(model);
        }
        [FilterAuthorize]
        [Localization]
        public ActionResult Search(DetailAdminAccountStatisticIndexModel model)
        {
            var lstGroupId = new List<int>();
            var createdBy = "";
            if (string.IsNullOrEmpty(model.CreatedBy))
            {
                // Lấy ra danh sách mà nhân viên có quyền 
                var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
                if(lstRoledGroup.Contains(DefaultSystemParameter.AllGroup))
                {
                    if(model.GroupId >0)
                    {
                        lstGroupId.Add(model.GroupId);
                    }
                    else
                    {
                        var groupLayer = new GroupLayer();
                        var allGroup = groupLayer.GetAll();
                        lstGroupId.AddRange(allGroup.Select(o => o.Id).ToList());
                    }
                }
                else
                {
                    if(model.GroupId == 0)
                    {
                        lstGroupId.AddRange(lstRoledGroup);
                    }
                    else
                    {
                        if(lstRoledGroup.Contains(model.GroupId))
                        {
                            lstGroupId.Add(model.GroupId);
                        }
                    }
                }
            }
            else
            {
                var userLayer = new UserLayer();
                var userInfo = userLayer.GetByUserName(model.CreatedBy);
                lstGroupId.Add(userInfo.GroupId);
                createdBy = model.CreatedBy ;
            }
            var customerLayer = new CustomerLayer();
            var reportResult = customerLayer.GetReportDetail(lstGroupId,createdBy,  model.TimeToCreationFrom, model.TimeToCreationTo);

            if(reportResult.Count >0)
            {
                var synBCRMNAdminLayer = new SyncBcrmBxhNAdminLayer();
                var strCustomerIds = string.Join(",", reportResult.Select(o => o.CustomerId).ToList());
                var lstAdminInfo = synBCRMNAdminLayer.GetByListCustomerId(strCustomerIds);

                // Gọi API  để lấy ra các tin đăng
                //var response = new Response();
                var response = CallAPIAdminGetPostingNews(lstAdminInfo.Select(o => o.AccountId).ToList());
                var lstSumPostingNews = new List<AdminPostingNewsModel>();
                if (response.Code == SystemCode.Success)
                {
                    lstSumPostingNews = JsonConvert.DeserializeObject<List<AdminPostingNewsModel>>(response.Data);
                }
                //var lstSumPostingNews = JsonConvert.DeserializeObject<List<AdminPostingNewsModel>>(response.Data);
                if(lstSumPostingNews.Count >0 )
                {
                    foreach (var customerItemInfo in reportResult)
                    {
                        var adminInfo = lstAdminInfo.Find(o => o.CustomerId == customerItemInfo.CustomerId);
                        if (adminInfo != null)
                        {
                            var adminSumPostingNews = ((List<AdminPostingNewsModel>)lstSumPostingNews).Find(o => o.AdminId == adminInfo.AccountId);
                            if (adminSumPostingNews != null)
                            {
                                customerItemInfo.NumberOfPostingNews = adminSumPostingNews.SumPostingNews;
                            }
                        }
                    }
                }

                // Gọi API để lấy ra các tài khoản active
                response = CallAPIAdminGetActivedAccount(lstAdminInfo.Select(o => o.AccountId).ToList(), model.TimeToCreationFrom, model.TimeToCreationTo);
                var lstActived = new List<long>();
                if (response.Code == SystemCode.Success)
                {
                    lstActived = JsonConvert.DeserializeObject<List<long>>((string)response.Data);
                }
                
                if(lstActived.Count > 0)
                {
                    foreach (var customerItemInfo in reportResult)
                    {
                        var adminInfo = lstAdminInfo.Find(o => o.CustomerId == customerItemInfo.CustomerId);
                        if (adminInfo != null)
                        {
                            if (lstActived.Contains(adminInfo.AccountId))
                            {
                                customerItemInfo.ActiveStatus = CustomerAccountActiveStatus.Actived.GetHashCode();
                            }
                        }
                    }
                }
                if(model.ActiveStatus == 1)
                {
                    reportResult = reportResult.FindAll(o => o.ActiveStatus == CustomerAccountActiveStatus.Actived.GetHashCode());
                }
                else if(model.ActiveStatus == 2)
                {
                    reportResult = reportResult.FindAll(o => o.ActiveStatus == CustomerAccountActiveStatus.Nonactived.GetHashCode());
                }

                
            }
            return View(reportResult);
        }
        [FilterAuthorize]
        public JsonResult GetUserByGroupId(int groupId)
        {
            var lstShowedAssignTo = new List<KeyValuePair<string, string>>();
            if (!UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
            {
                lstShowedAssignTo.Add(new KeyValuePair<string, string>(UserContext.UserName, UserContext.UserName));
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = lstShowedAssignTo
                }, JsonRequestBehavior.AllowGet);
            }

            var groupLayer = new GroupLayer();
            var userLayer = new UserLayer();
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            if (groupId == 0)
            {
                var allGroups = groupLayer.GetAll();
                var lstGroup = lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                                ? allGroups : allGroups.FindAll(o => lstRoledGroup.Contains(o.Id));
                var groupIds = string.Join(",", lstGroup.Select(o => o.Id).ToList());
                var lstUser = userLayer.GetList(groupIds);
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = lstUser.Select(o => new KeyValuePair<string, string>(o.UserName, o.UserName)).ToList()
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // Lấy danh sách người dùng thuộc nhóm đó
                var lstUser = userLayer.GetByGroupId(groupId);
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = lstUser.Select(o => new KeyValuePair<string, string>(o.UserName, o.UserName)).ToList()
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [NonActionAttribute]
        public Response CallAPIAdminGetPostingNews(List<long> lstAdminId)
        {
            // Gọi api thêm mới thông tin tài khoản khách hàng trên site
            var url = string.Format("{0}/BCRMService.svc/GetCustomerPostingNumber", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var data = new
            {
                Token = Security.CreateKey(),
                ListAdminId = lstAdminId
            };
            var res = BCRM.BXH.ThaiLand.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(data), "application/json");

            if (string.IsNullOrEmpty(res))
            {
                return new Response()
                {
                    Code = SystemCode.ErrorConnect,
                    Message = "Error in process , Contact IT for detail !"
                };
            }
            var hrmResponse = JsonConvert.DeserializeObject<Response>(res);

            return hrmResponse;
        }
        [NonActionAttribute]
        public Response CallAPIAdminGetActivedAccount(List<long> lstAdminId,DateTime fromDate ,DateTime toDate)
        {
            // Gọi api thêm mới thông tin tài khoản khách hàng trên site
            var url = string.Format("{0}/BCRMService.svc/CustomerCheckActive", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var data = new
            {
                Token = Security.CreateKey(),
                ListAdminId = lstAdminId,
                FromDate =  fromDate.ToString("dd/MM/yyyy"),
                ToDate = toDate.ToString("dd/MM/yyyy")
            };
            var res = BCRM.BXH.ThaiLand.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(data), "application/json");

            if (string.IsNullOrEmpty(res))
            {
                return new Response()
                {
                    Code = SystemCode.ErrorConnect,
                    Message = "Error in process , Contact IT for detail !"
                };
            }
            var hrmResponse = JsonConvert.DeserializeObject<Response>(res);
            return hrmResponse;
        }

    }
}