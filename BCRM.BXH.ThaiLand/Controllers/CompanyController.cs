﻿using BCRM.BXH.ThaiLand.DAL;
using BCRM.BXH.ThaiLand.Filter;
using BCRM.BXH.ThaiLand.Library;
using BCRM.BXH.ThaiLand.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.ThaiLand.Controllers
{
    public class CompanyController : BaseController
    {
        [FilterAuthorize]
        [Localization]
        public ActionResult Index()
        {
            var model = new CompanyIndexModel();
            // Kiểm tra xem người dùng đăng nhập có quyền admin hay không
            if (UserContext.HasRole(RoleSystem.Admin))
            {
                model.IsAdmin = true;
            }
            var cityLayer = new CityLayer();
            model.SetCityList(cityLayer.GetAll());
            return View(model);
        }
        [FilterAuthorize]
        [Localization]
        public ActionResult Search(CompanyIndexModel model)
        {
            var companyLayer = new CompanyLayer();
            // Kiểm tra xem người dùng có tìm kiếm theo số điện thoại hay không
            var lstPhone = new List<string>();
            if (!string.IsNullOrEmpty(model.FilterKeyword) )
            {
                lstPhone = Ultility.DetectPhoneNumber(model.FilterKeyword);
            }
            var result = new List<CompanySearchModel>();
            var totalRecord = 0;
            if(lstPhone.Count >0)
            {
                var companyInfo = companyLayer.SearchByPhoneNumber(lstPhone[0]);
                if(companyInfo != null)
                {
                    result.Add(companyInfo);
                    totalRecord = 1;
                }
            }
            else
            {
                result = companyLayer.Search(model, out totalRecord);
            }
            ViewBag.Pager = new Pager()
            {
                ActionScript = "company_index.search",
                Data = result,
                CurrentPage = model.PageIndex,
                PageSize = model.PageSize,
                TotalItem = totalRecord
            };
            // Kiểm tra xem người dùng có quyền admin hay không
            ViewBag.IsAdmin = UserContext.HasRole(RoleSystem.Admin) ? true : false;
            return View(result);
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin)]
        public JsonResult Save(CompanySaveModel model)
        {
            var companyLayer = new CompanyLayer();
            if(model.Id ==0)
            {
                if(companyLayer.IsExistedPhonenumber(0,model.PhoneNumber ))
                {
                    return Json(new Response()
                    {
                        Code = SystemCode.ErrorExist,
                        Message = "Phone number existed in another company ! !"
                    }, JsonRequestBehavior.DenyGet);
                }

                if (companyLayer.IsExistedNameAndAddress(0, model.Name,model.Address))
                {
                    return Json(new Response()
                    {
                        Code = SystemCode.ErrorExist,
                        Message = "Company existed !"
                    }, JsonRequestBehavior.DenyGet);
                }

                companyLayer.Create(model, UserContext.UserName);
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Message = "You have created a new company successfully !"
                }, JsonRequestBehavior.DenyGet);
            }
            else
            {
                if (companyLayer.IsExistedPhonenumber(model.Id, model.PhoneNumber))
                {
                    return Json(new Response()
                    {
                        Code = SystemCode.ErrorExist,
                        Message = "Phone number existed in another company ! !"
                    }, JsonRequestBehavior.DenyGet);

                }

                if (companyLayer.IsExistedNameAndAddress(model.Id, model.Name, model.Address))
                {
                    return Json(new Response()
                    {
                        Code = SystemCode.ErrorExist,
                        Message = "Company existed !"
                    }, JsonRequestBehavior.DenyGet);
                }

                companyLayer.Update(model, UserContext.UserName);
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Message = "You have updated the company info successfully !"
                }, JsonRequestBehavior.DenyGet);
            }
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin)]
        public JsonResult Delete(int id)
        {
            var companyLayer = new CompanyLayer();
            var companyInfo = companyLayer.GetById(id);
            var response = new Response();
            if (companyInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull ,
                    Message = "We're sorry . Your company searched is not found ! "
                }, JsonRequestBehavior.DenyGet);
            }
            companyLayer.Delete(id);
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Message = "Deleted successfully !"
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public JsonResult GetDictrictByCityId(int cityId)
        {
            var cityLayer = new CityLayer();
            var districtLayer = new DistrictLayer();
            var lstDistrict = districtLayer.GetByCityId(cityId);
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = lstDistrict.Select(o => new SelectListItem() { Value = o.Id.ToString(), Text = o.Name }).ToList()
            }, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult Edit(int id)
        {
            var companyLayer = new CompanyLayer();
            var companyInfo = companyLayer.GetById(id);
            if (companyInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull,
                    Message = "We're sorry . We cannot find your company searching !"
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = companyInfo
            }, JsonRequestBehavior.AllowGet);
        }
    }
}