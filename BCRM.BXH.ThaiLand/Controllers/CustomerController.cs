﻿using BCRM.BXH.ThaiLand.Cache;
using BCRM.BXH.ThaiLand.DAL;
using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Filter;
using BCRM.BXH.ThaiLand.Language;
using BCRM.BXH.ThaiLand.Library;
using BCRM.BXH.ThaiLand.Models;
using DVS.Algorithm;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.ThaiLand.Controllers
{
    public class CustomerController : BaseController
    {
        [FilterAuthorize]
        [Localization]
        public ActionResult Index(CustomerIndexModel indexModel)
        {
            // Lưu session thông tin tìm kiếm của người dùng
            if (indexModel.IsNull)
            {
                indexModel = Session["viewModel"] as CustomerIndexModel ??
                             new CustomerIndexModel { AssignTo = UserContext.UserName };
            }
            var groupLayer = new GroupLayer();
            // Lấy ra danh sách mà nhân viên có quyền 
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            var groupids = "";
            if (!lstRoledGroup.Contains(DefaultSystemParameter.AllGroup))
            {
                groupids = string.Join(",", lstRoledGroup);
            }
            else
            {
                var allGroup = groupLayer.GetAll();
                groupids = string.Join(",", allGroup.Select(o => o.Id).ToList());
            }
            
            if (UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
            {
                if (lstRoledGroup.Contains(DefaultSystemParameter.AllGroup))
                {
                    indexModel.LstGroup = groupLayer.GetAll();
                }
                else
                {
                    indexModel.LstGroup = groupLayer.GetAll().FindAll(o => lstRoledGroup.Contains(o.Id));
                }
            }

            var userLayer = new UserLayer();
            ViewBag.ListUser = string.IsNullOrEmpty(groupids) ? new List<UserSearchModel>() { new UserSearchModel() { UserName = UserContext.UserName }  } : userLayer.GetList(groupids);
            var cityLayer = new CityLayer();
            ViewBag.ListCity = cityLayer.GetAll();
            ViewBag.AssignTo = UserContext.UserName;
            Session["viewModel"] = indexModel;
            ViewBag.CurrentUser = UserContext;
            return View(indexModel);
        }
        [HttpPost]
        [FilterAuthorize]
        [Localization]
        public ActionResult GetListCustomer(CustomerIndexModel model)
        {
            var date = DateTime.Now;
            if (string.IsNullOrEmpty(model.Creator))
            {
                model.Creator = "";
            }

            ViewBag.CurrentUser = UserContext;
            // Kiểm tra xem người dùng có quyền admin hoặc manager hay không ?
            var hasPermit = UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager);
            // Kiểm tra xem người dùng có tìm kiếm theo số điện thoại hay không
            var lstPhone = new List<string>();
            if (!string.IsNullOrEmpty(model.FilterKeyword))
            {
                lstPhone = Ultility.DetectPhoneNumber(model.FilterKeyword);
            }
            var custLayer = new CustomerLayer();
            var pager = new Pager();
            if (lstPhone.Count > 0)
            {
                model.ListCustomer = custLayer.SelectByPhoneNumber(lstPhone.Select(p => p).ToArray());
                pager.CurrentPage = model.PageIndex;
                pager.PageSize = model.PageSize;
                pager.TotalItem = model.ListCustomer.Count;
            }
            else
            {
                var lstCus = new List<CustomerForIndex>();
                var total = 0;
                // TH tìm tất cả các khách hàng
                if (model.AssignTo.Equals("-1"))
                {
                    // Bao gồm việc tìm kiếm các khách hàng được giao cho người dùng đang đăng nhập
                    model.AssignTo = UserContext.UserName;
                    // Lấy ra danh sách nhóm mà người dùng có quyền xem
                    var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
                    var groupLayer = new GroupLayer();
                    if(lstRoledGroup.Contains(DefaultSystemParameter.AllGroup))
                    {
                        lstRoledGroup.Remove(DefaultSystemParameter.AllGroup);
                        lstRoledGroup.AddRange(groupLayer.GetAll().Select(o => o.Id).ToList());
                    }
                    
                    // Bổ sung điều kiện tìm kiếm các khách 
                    // hàng không thuộc nhóm nào
                    lstRoledGroup.Add(DefaultSystemParameter.CustomerNotBelongToGroup);
                    
                    lstCus = custLayer.SearchAll(model.Sort,
                                           model.Status.HasValue ? model.Status.Value : -1,
                                           model.Type.HasValue ? model.Type.Value : -1,
                                           model.AssignTo,
                                           model.CityId.HasValue ? model.CityId.Value : -1,
                                           model.DistrictId.HasValue ? model.DistrictId.Value : -1,
                                           model.StartDate.Value,
                                           model.EndDate.Value,
                                           model.FilterKeyword ?? string.Empty,
                                           lstRoledGroup,
                                           model.PageIndex,
                                           model.PageSize,
                                           hasPermit,
                                           model.Creator,
                                           out total);
                }
                // TH tìm các khách hàng chưa được gán cho sale
                else if (model.AssignTo.Equals("0"))
                {
                    lstCus = custLayer.SearchUnAssign(model.Sort,
                                           model.Status.HasValue ? model.Status.Value : -1,
                                           model.Type.HasValue ? model.Type.Value : -1,
                                           model.CityId.HasValue ? model.CityId.Value : -1,
                                           model.DistrictId.HasValue ? model.DistrictId.Value : -1,
                                           model.StartDate.Value,
                                           model.EndDate.Value,
                                           model.FilterKeyword ?? string.Empty,
                                           model.PageIndex,
                                           model.PageSize,
                                           hasPermit,
                                           model.Creator,
                                           out total);
                }
                // TH tìm kiếm theo từng sale cụ thể
                else
                {
                    var assignTo = UserContext.UserName;
                    var cacheUser = new IISUser();
                    var sale = cacheUser.GetUserInfo(model.AssignTo);
                    if (sale.Status == UserStatus.NotAvaiable.GetHashCode()
                        || (UserContext.HasRole(RoleSystem.Admin))
                        || (UserContext.HasRole(RoleSystem.TeamLeader, RoleSystem.Manager)
                            && UserContext.ListRoleGroup.Select(r => r.Key == RoleSystem.TeamLeader.GetHashCode() 
                                                                  && r.Value.Contains(sale.GroupId)) != null))
                    {
                        assignTo = model.AssignTo;
                    }

                    lstCus = custLayer.Search(model.Sort,
                                               model.Status.HasValue ? model.Status.Value : -1,
                                               model.Type.HasValue ? model.Type.Value : -1,
                                               assignTo,
                                               model.CityId.HasValue ? model.CityId.Value : -1,
                                               model.DistrictId.HasValue ? model.DistrictId.Value : -1,
                                               model.StartDate.Value,
                                               model.EndDate.Value,
                                               model.FilterKeyword ?? string.Empty,
                                               model.PageIndex,
                                               model.PageSize,
                                               hasPermit,
                                               model.Creator,
                                               out total);
                }
                model.ListCustomer = lstCus;
                pager.CurrentPage = model.PageIndex;
                pager.PageSize = model.PageSize;
                pager.TotalItem = total;
            }
            // Lấy ra danh sách các tài khoản tương ứng với 

            if(model.ListCustomer.Count >0)
            {
                var synBCRMNAdminLayer = new SyncBcrmBxhNAdminLayer();
                var strCustomerIds = string.Join(",", model.ListCustomer.Select(o => o.Id).ToList());
                var lstAdminInfo = synBCRMNAdminLayer.GetByListCustomerId(strCustomerIds);

                // Gọi API  để lấy ra các tin đăng
                var response = CallAPIAdminGetPostingNews(lstAdminInfo.Select(o => o.AccountId).ToList());

                var lstSumPostingNews = new List<AdminPostingNewsModel>();
                if(response.Code == SystemCode.Success)
                {
                    lstSumPostingNews = JsonConvert.DeserializeObject<List<AdminPostingNewsModel>>(response.Data);
                }
                
                foreach (var customerItemInfo in model.ListCustomer)
                {
                    var adminInfo = lstAdminInfo.Find(o => o.CustomerId == customerItemInfo.Id);
                    if (adminInfo != null)
                    {
                        var adminSumPostingNews = ((List<AdminPostingNewsModel>)lstSumPostingNews).Find(o => o.AdminId == adminInfo.AccountId);
                        if (adminSumPostingNews != null)
                        {
                            customerItemInfo.SumPostingNews = adminSumPostingNews.SumPostingNews;
                        }
                    }
                }
            }
            ViewBag.Pager = pager;
            Session["viewModel"] = model;
            return View(model);
        }
        [FilterAuthorize]
        [Localization]
        public ActionResult GetViewByCustomerId(int index, int customerId)
        {
            var custLayer = new CustomerLayer();
            var model = custLayer.SearchByCustomerId(customerId);
            ViewBag.Index = index;
            ViewBag.CurrentUser = UserContext;
            return View(model);
        }
        [FilterAuthorize]
        [Localization]
        public ActionResult Create()
        {
            var customer = new Customer();
            var cityLayer = new CityLayer();
            var regionLayer = new RegionLayer();
            var companyLayer = new CompanyLayer();
            var lstRegion = regionLayer.GetAll();
            ViewBag.ListCity = lstRegion.Count > 0 ? cityLayer.GetByRegionId(lstRegion.FirstOrDefault().Id) : new List<City>();
            ViewBag.ListRegion = lstRegion;
            ViewBag.CurrentUser = UserContext;
            ViewBag.ListCompany = companyLayer.GetAll();
            return View(customer);
        }
        [FilterAuthorize]
        [JsonModelBinder(ActionParameterType = typeof(CustomerRequest), ActionParameterName = "model")]
        [Localization]
        public JsonResult CreateAction(CustomerRequest model)
        {
            var response = new Response();
            try
            {
                var customerLayer = new CustomerLayer();
                // Kiểm tra số điện thoại của khách hàng có trùng với danh sách khách hàng crawler hay không
                if (customerLayer.IsExistedPhoneNumber(0, model.PhoneNumber))
                {
                    return Json(new Response() {
                                Code = SystemCode.NotValid,
                        //Message = "Phonenumber " + model.PhoneNumber + " exist in other file customer. Please check again!"
                        Message = LanguageRes.Msg_Existed_Phonenumber
                    }, JsonRequestBehavior.DenyGet);
                }
                // Kiểm tra email của khách hàng có năm trong danh sách khách hàng của hệ thống hay không
                if(!string.IsNullOrEmpty(model.Email) && customerLayer.IsExistedEmail(0,model.Email))
                {
                    return Json(new Response()
                    {
                        Code = SystemCode.ErrorExist,
                        //Message = "Email " +model.Email + " existed in other customer file. Please check again!"
                        Message = LanguageRes.Msg_Existed_Email
                    },JsonRequestBehavior.DenyGet );
                }

                #region Kiểm tra xem số điện thoại khai báo có nằm trong tập danh sách khách hàng crawler hay không
                var apiCheckExistPhoneNumberResponse = CallAPICheckExistPhoneNumberInCrawlerCustomer(model.PhoneNumber);
                if (apiCheckExistPhoneNumberResponse.Code != SystemCode.Success)
                {
                    return Json(apiCheckExistPhoneNumberResponse, JsonRequestBehavior.DenyGet);
                }
                var resModel = JsonConvert.DeserializeObject<CrawlerCustomerIndexModel>((string)apiCheckExistPhoneNumberResponse.Data);
                if (resModel.ListData.Count > 0)
                {
                    return Json(new Response()
                    {
                        Code = SystemCode.NotPermitted,
                        Message = LanguageRes.Msg_Existed_Crawlerlist
                    }, JsonRequestBehavior.DenyGet);
                }
                #endregion

                // Gọi api thêm mới tài khoản bên site
                var apiResponse= CallAPIAdminInsertUpdateAccount(0, model.FullName, model.Email, model.PhoneNumber, model.Address, true);
                if(apiResponse.Code != SystemCode.Success)
                {
                    return Json(apiResponse, JsonRequestBehavior.DenyGet);
                }
                // Lấy thông tin tài khoản admin
                var adminAccountId = ((long)apiResponse.Data);
                //// Tạo mới file khách mời
                var customerId = customerLayer.Create(new Customer
                {
                    FullName = model.FullName,
                    PhoneNumber = model.PhoneNumber,
                    //Status = model.Status,
                    Address = model.Address,
                    Type = model.Type,
                    Email = model.Email,
                    Description = model.Description,
                    RegionId = model.RegionId,
                    CityId = model.CityId,
                    DistrictId = model.DistrictId,
                    AssignTo = UserContext.UserName,
                    GroupId = UserContext.GroupId,
                    CareDate = DateTime.Now,
                    ICareDate = DateTime.Now.ToInt32(),
                    StartCareDate = DateTime.Now,
                    CompanyId = model.CompanyId,
                    CreatedBy = UserContext.UserName,
                    CreatedDate = DateTime.Now,
                    LastModifiedBy = UserContext.UserName,
                    LastModifiedDate = DateTime.Now
                });
                //// Lưu vào bảng syncbcrmbxhnadmin để đồng bộ
                var syncBcrmBxhNAdminLayer = new SyncBcrmBxhNAdminLayer();
                if (adminAccountId > 0)
                {
                    syncBcrmBxhNAdminLayer.Create(customerId, adminAccountId, model.Email ,model.PhoneNumber );
                }
                // Lưu lịch sử thông tin thao tác khách hàng
                var customerHistoryLayer = new CustomerHistoryLayer();
                customerHistoryLayer.Create(new CustomerHistory()
                {
                    Action = "The customer is created",
                    OldValue = "",
                    NewValue = "",
                    Note = "",
                    CustomerId = customerId,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now
                });
                // Bổ sung thông tin nhận khách của sale để tra cứu thông tin 
                // hoạt động của sale đó
                var customerCareHistoryLayer = new CustomerCareHistoryLayer();
                var idCareHistory= customerCareHistoryLayer.Create(new CustomerCareHistory()
                {
                    AdminId = adminAccountId,
                    CrawlerAdminId = 0,
                    ReceivedDate = DateTime.Now,
                    ReceivedBy = UserContext.UserName,
                    UserId = UserContext.Id,
                    CustomerId = customerId,
                    Note = string.Empty
                });
                customerCareHistoryLayer.UpdateSuccessContact(idCareHistory, PhoningStatus.Success.GetHashCode(), DateTime.Now);
                response.Code = SystemCode.Success;
                //response.Message = "You inserted a new customer info successfully!";
                response.Message = LanguageRes.Msg_Insert_Successfully;
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult Edit()
        {
            var cityLayer = new CityLayer();
            var regionLayer = new RegionLayer();
            var companyLayer = new CompanyLayer();
            var lstRegion = regionLayer.GetAll();
            ViewBag.ListCity = lstRegion.Count > 0 ? cityLayer.GetByRegionId(lstRegion.FirstOrDefault().Id) : new List<City>();
            ViewBag.ListRegion = lstRegion;
            ViewBag.CurrentUser = UserContext;
            ViewBag.ListCompany = companyLayer.GetAll();
            var ediModel = new CustomerEditModel();
            if (UserContext.HasRole(RoleSystem.Admin,RoleSystem.Manager))
            {
                ediModel.IsAdmin = true;
            }
            //if(UserContext.HasRole(RoleSystem.Manager) )
            //{
            //    ediModel.IsManager = true;
            //}
            return View(ediModel);
        }
        [FilterAuthorize]
        [Localization]
        [JsonModelBinder(ActionParameterType = typeof(Customer), ActionParameterName = "model")]
        public JsonResult EditAction(Customer model)
        {
            var response = new Response();
            try
            {
                var customerLayer = new CustomerLayer();
                var oldCust = customerLayer.GetById(model.Id);
                if (oldCust.IsDeleted)
                {
                    response.Code = SystemCode.Locked;
                    //response.Message = "Customer File has blocked!";
                    response.Message = LanguageRes.Msg_Blocked;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                // Kiểm tra người dùng hiện thời có quyền Admin hay không
                // Nếu không thì giữ nguyên các thông tin
                // tên , email , số điện thoại cũ
                var isAdmin = UserContext.HasRole(RoleSystem.Admin,RoleSystem.Manager);
                if(!isAdmin)
                {
                    model.FullName = oldCust.FullName;
                    model.Email = oldCust.Email;
                    model.PhoneNumber = oldCust.PhoneNumber;
                }
                model.LastModifiedDate = DateTime.Now;
                model.LastModifiedBy = UserContext.UserName;
                
                // Kiểm tra xem có thông tin đồng bộ của file khách hàng với tài khoản admin site hay không ?
                var syncBcrmBxhNAdminLayer = new SyncBcrmBxhNAdminLayer();
                var syncModel = syncBcrmBxhNAdminLayer.GetByCustomerId(model.Id);
                if(syncModel != null)
                {
                    // Kiểm tra xem email có bị thay đổi hay ko ?
                    var email = model.Email.ToLower().Trim();
                    var isChangedEmail = false;
                    if (!syncModel.Email.Equals(email))
                    {
                        isChangedEmail = true;
                    }
                    var isChangedPhone = false;
                    var phoneNumber = model.PhoneNumber;
                    if (!syncModel.PhoneNumber.Equals(phoneNumber))
                    {
                        isChangedPhone = true;
                    }

                    var apiResponse = CallAPIAdminInsertUpdateAccount(syncModel.AccountId, model.FullName, email, model.PhoneNumber, model.Address, isChangedEmail);
                    if (apiResponse.Code != SystemCode.Success)
                    {
                        return Json(apiResponse, JsonRequestBehavior.DenyGet);
                    }

                    // Cập nhật lại bảng syncbcrmBXHNAdmin khi thay đổi thông tin email
                    if (isChangedEmail || isChangedPhone)
                    {
                        syncBcrmBxhNAdminLayer.Update(model.Id, model.Email,model.PhoneNumber);
                    }
                }
                else
                {
                    var email = model.Email.ToLower().Trim();
                    var apiResponse = CallAPIAdminInsertUpdateAccount(0, model.FullName, email, model.PhoneNumber, model.Address, true);
                    if (apiResponse.Code != SystemCode.Success)
                    {
                        return Json(apiResponse, JsonRequestBehavior.DenyGet);
                    }

                    syncBcrmBxhNAdminLayer.Create(model.Id, (long)apiResponse.Data, model.Email, model.PhoneNumber);
                }

                //// Cập nhật lại thông tin
                customerLayer.Update(model);

                // Lưu lịch sử cập nhật lại thông tin khách hàng
                var customerHistoryLayer = new CustomerHistoryLayer();
                var lstChanged = GetChangedCustomerInfo(oldCust, model);
                foreach(var changeItem in lstChanged)
                {
                    customerHistoryLayer.Create(changeItem);
                }

                response.Code = SystemCode.Success;
                //response.Message = "You updated customer info successfully!";
                response.Message = LanguageRes.Msg_Updated_Successfully;
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult GetById(int customerId)
        {
            var response = new Response();
            try
            {
                var customerLayer = new CustomerLayer();
                response.Data = customerLayer.GetById(customerId);
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        [Localization]
        public JsonResult Block(int customerId)
        {
            var response = new Response();
            try
            {
                var customerLayer = new CustomerLayer();
                var customer = customerLayer.GetById(customerId);
                if(customer == null)
                {
                    return Json(new Response()
                    {
                        Code = SystemCode.DataNull,
                        Message = "Customer info is not found !"
                    }, JsonRequestBehavior.DenyGet);
                }

                if (UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
                {
                    customerLayer.Block(customerId);
                    // Lưu lịch sử thông tin của người block khách hàng này
                    var customerHistoryLayer = new CustomerHistoryLayer();
                    customerHistoryLayer.Create(new CustomerHistory() {
                        Action =  "Blocking",
                        CustomerId = customerId,
                        UpdatedBy = UserContext.UserName,
                        UpdatedDate = DateTime.Now,
                        NewValue = "",
                        OldValue = "",
                        Note = ""
                    });

                    response.Code = SystemCode.Success;
                    response.Message = LanguageRes.Msg_Updated_Successfully;
                }
                else
                {
                    response.Code = SystemCode.NotPermitted;
                    response.Message = LanguageRes.Msg_NotPermitted;
                }
            }
            catch 
            {
                response.Code = SystemCode.Error;
                response.Message = LanguageRes.ErrorProcess;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        [Localization]
        public JsonResult UnBlock(int customerId)
        {
            var response = new Response();
            try
            {
                var customerLayer = new CustomerLayer();
                var customer = customerLayer.GetById(customerId);
                if (UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager) )
                {
                    customerLayer.UnBlock(customerId);
                    // Lưu lịch sử thông tin của người block khách hàng này
                    var customerHistoryLayer = new CustomerHistoryLayer();
                    customerHistoryLayer.Create(new CustomerHistory()
                    {
                        Action = "Unblocking",
                        CustomerId = customerId,
                        UpdatedBy = UserContext.UserName,
                        UpdatedDate = DateTime.Now,
                        NewValue = "",
                        OldValue = "",
                        Note = ""
                        //Note = string.Format("{0} has unblocked this customer !", UserContext.UserName)
                    });

                    response.Code = SystemCode.Success;
                    //response.Message = "You have unblocked this customer file successfully !";
                    response.Message = LanguageRes.Msg_Updated_Successfully;
                }
                else
                {
                    response.Code = SystemCode.NotPermitted;
                    //response.Message = "You don't have permission to process this action !";
                    response.Message = LanguageRes.Msg_NotPermitted;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            catch 
            {
                response.Code = SystemCode.Error;
                //response.Message = "Error in the process. Contact IT for details !";
                response.Message = LanguageRes.ErrorProcess;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult GetSaleByGroupId(int groupId)
        {
            var groupLayer = new GroupLayer();
            var lstAllGroup = groupLayer.GetAll();
            var groupInfo = lstAllGroup.Find(o => o.Id == groupId);
            if(groupInfo == null)
            {
                return Json( new Response()
                {
                    Code = SystemCode.DataNull,
                    Message = "The group you search is not found !"
                } , JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra xem người dùng có quyền với nhóm này
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            if(!lstRoledGroup.Contains(DefaultSystemParameter.AllGroup) && !lstRoledGroup.Contains(groupId))
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = "You don't have a permission with this group !"
                }, JsonRequestBehavior.AllowGet);
            }

            var userLayer = new UserLayer();
            var lstUser = userLayer.GetByGroupId(groupId);
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = lstUser
            }, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        [Localization]
        public JsonResult AssignTo(int customerId,string userName)
        {
            // Check authenticated user whether has admin,manager role or not
            if(!UserContext.HasRole(RoleSystem.Admin,RoleSystem.Manager))
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    //Message = "You don't have a permission to assign to any saler !"
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }
            // Check whether saler is existed or not
            var userLayer = new UserLayer();
            var userInfo = userLayer.GetByUserName(userName);
            if(userInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull,
                    Message = "saler is assigned to this customer is not found!"
                }, JsonRequestBehavior.DenyGet);
            }

            var customerLayer = new CustomerLayer();
            var customerInfo = customerLayer.GetById(customerId);

            customerLayer.UpdateAssignTo(customerId, userInfo.GroupId, userInfo.UserName);

            // Lưu lịch sử thông tin của người block khách hàng này
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "Assigning to",
                CustomerId = customerId,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                NewValue = userName,
                
                OldValue = customerInfo.AssignTo,
                Note = ""
            });

            var syncBCRMToAdminLayer =new SyncBcrmBxhNAdminLayer();
            var syncInfoModel = syncBCRMToAdminLayer.GetByCustomerId(customerId);

            var careHistoryLayer = new CustomerCareHistoryLayer();
            careHistoryLayer.Create(new CustomerCareHistory()
            {
                AdminId = syncInfoModel.AccountId,
                CustomerId = customerId,
                PhoningStatus = PhoningStatus.Success.GetHashCode(),
                ReceivedBy = customerInfo.AssignTo,
                ReceivedDate = DateTime.Now,
                SuccessContactDate = DateTime.Now,
                UserId = userInfo.Id,
                Note = ""
            });

            return Json(new Response()
            {
                Code = SystemCode.Success,
                //Message = "you assignned to saler successfully!"
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);

        }
        [FilterAuthorize]
        [Localization]
        public JsonResult Blacklist(int customerId)
        {
            var customerLayer = new CustomerLayer();
            customerLayer.UpdateBlacklist(customerId, true);
            // Lưu lịch sử thông tin của người block khách hàng này
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "Blacklisting",
                CustomerId = customerId,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                NewValue = "",
                OldValue = "",
                Note = ""
                //Note = string.Format("{0} has blacklisted this customer!", UserContext.UserName)
            });

            return Json(new Response()
            {
                Code = SystemCode.Success,
                //Message = "You blacklisted this customer successfully"
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        [Localization]
        public JsonResult UnBlacklist(int customerId)
        {
            var customerLayer = new CustomerLayer();
            customerLayer.UpdateBlacklist(customerId, false);

            // Lưu lịch sử thông tin của người block khách hàng này
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "Unblacklisting",
                CustomerId = customerId,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                NewValue = "",
                OldValue = "",
                Note = ""
                //Note = string.Format("{0} has unblacklisted this customer!", UserContext.UserName)
            });

            return Json(new Response()
            {
                Code = SystemCode.Success,
                //Message = "You unblacklisted this customer successfully !"
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        [Localization]
        public JsonResult Note(int id,string note)
        {
            var customerLayer = new CustomerLayer();
            var customerInfo = customerLayer.GetById(id);
            if(customerInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull,
                    Message = "Cannot find customer info !"
                }, JsonRequestBehavior.DenyGet);
            }
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory() {
                Action = "Note",
                CustomerId = id,
                OldValue = "",
                NewValue = "",
                Note = note,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now
            });

            return Json(new Response()
            {
                Code = SystemCode.Success,
                //Message = "You noted this customer successfully !"
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        [Localization]
        public ActionResult Detail(int customerId)
        {
            var customerLayer = new CustomerLayer();
            
            // Lấy thông tin khách hàng
            var customerInfo = customerLayer.GetById(customerId);
            if(customerInfo == null)
            {
                return View(new CustomerDetailModel()) ;
            }
            // Lấy thông tin vùng
            var regionLayer = new RegionLayer();
            var regionInfo = regionLayer.GetById(customerInfo.RegionId);
            // Lấy thông tin thành phố
            var cityLayer = new CityLayer();
            var cityInfo = cityLayer.GetById(customerInfo.CityId);
            // Lấy thông tin quận huyện
            var districtLayer = new DistrictLayer();
            var districtInfo = districtLayer.GetById(customerInfo.DistrictId);
            // Lấy thông tin công ty
            var companyLayer = new CompanyLayer();
            var companyInfo = companyLayer.GetById(customerInfo.CompanyId);

            var model = new CustomerDetailModel()
            {
                FullName =customerInfo.FullName,
                Address = customerInfo.Address,
                AssignTo = customerInfo.AssignTo,
                RegionName = regionInfo == null ? "" : regionInfo.Name,
                CityName = cityInfo == null ? "" : cityInfo.Name,
                DistrictName = districtInfo == null ? "" : districtInfo.Name,
                CompanyName = companyInfo == null ? "" : companyInfo.Name,
                Description = customerInfo.Description,
                Email = customerInfo.Email,
                PhoneNumber = customerInfo.PhoneNumber,
                LastModifiedBy = customerInfo.LastModifiedBy,
                LastModifiedDate = customerInfo.LastModifiedDate,
                TypeOfCustomerName = ((TypeOfCustomer)customerInfo.Type).ToTypeOfCustomerName()
            };
            // Lấy thông tin lịch sử thao tác của khách hàng đó
            var customerHistoryLayer = new CustomerHistoryLayer();
            model.LstActivityHistory = customerHistoryLayer.GetByCustomerId(customerId).OrderByDescending(o=>o.UpdatedDate).ToList();
            // Lấy thông tin chăm sóc khách hàng này
            var customerCareHistoryLayer = new CustomerCareHistoryLayer();
            var lstCareHistory = customerCareHistoryLayer.GetByCustomerId(customerId);
            // Tính số lần từ chối của khách hàng này
            model.SumRejectionTimes = lstCareHistory.FindAll(o => o.PhoningStatus == PhoningStatus.Failure.GetHashCode()).Count;
            return View(model);
        }
        [FilterAuthorize]
        [Localization]
        public JsonResult Receive(int customerId)
        {
            // Kiểm tra xem khách hàng này đã được nhận bởi ai hay chưa
            var customerLayer = new CustomerLayer();
            var customerInfo = customerLayer.GetById(customerId);
            if(customerInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = "This customer is cared of another saler !"
                }, JsonRequestBehavior.DenyGet);
            }
            customerLayer.UpdateAssignTo(customerId, UserContext.GroupId, UserContext.UserName);
            // Lưu lịch sử 
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "Receiving customer",
                CustomerId = customerId,
                OldValue ="",
                NewValue = UserContext.UserName,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                Note = ""
            });
            // Lấy thông tin adminid của tài khoản
            var syncBCRMBxhAdminLayer = new SyncBcrmBxhNAdminLayer();
            var syncInfo = syncBCRMBxhAdminLayer.GetByCustomerId(customerId);

            // Lưu lịch sử chăm sóc khách hàng
            var customerCareHistoryLayer = new CustomerCareHistoryLayer();
            customerCareHistoryLayer.Create(new CustomerCareHistory()
            {
                AdminId = syncInfo.AccountId,
                CrawlerAdminId = 0,
                CustomerId = customerId,
                Note = "",
                PhoningStatus = PhoningStatus.Success.GetHashCode(),
                ReceivedBy = UserContext.UserName,
                ReceivedDate = DateTime.Now,
                SuccessContactDate = DateTime.Now,
                UserId = UserContext.Id
            });

            return Json(new Response()
            {
                Code = SystemCode.Success,
                //Message = "You received successfully"
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        [Localization]
        public JsonResult Return(int customerId,string assginer)
        {
            var customerLayer = new CustomerLayer();
            var customerInfo = customerLayer.GetById(customerId);

            if(customerInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull,
                    Message = "The customer information is not find !"
                }, JsonRequestBehavior.DenyGet);
            }

            if( !UserContext.UserName.Equals(assginer) || !customerInfo.AssignTo.Equals(assginer) )
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = "You don't have a permission to execute this action !"
                }, JsonRequestBehavior.DenyGet);
            }

            var careHistoryLayer = new CustomerCareHistoryLayer();
            var lstCareHistory = careHistoryLayer.GetByCustomerId(customerId);
            var careHistory = lstCareHistory.Find(o => o.PhoningStatus == PhoningStatus.Success.GetHashCode()
                                                  && !o.IsReturned);
            if(careHistory != null)
            {
                careHistoryLayer.UpdateReturnStatus(careHistory.Id, true);
            }
            customerLayer.UpdateAssignTo(customerInfo.Id, 0, "");
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory() {
                Action= "Return customer",
                CustomerId = customerInfo.Id,
                OldValue = customerInfo.AssignTo,
                NewValue = "",
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                Note = ""
            });

            return Json(new Response()
            {
                Code = SystemCode.Success,
                //Message  = "You returned this customer successfully !"
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public ActionResult ExportExcel(CustomerIndexModel model)
        {
            using (var package = new ExcelPackage())
            {
                #region Dựng template cho file excel
                var worksheet = package.Workbook.Worksheets.Add("CUSTOMERS");
                worksheet.Cells["A1:A4"].Merge = true;
                worksheet.Cells["B1:B4"].Merge = true;
                worksheet.Cells["B1:B4"].Value = "DAIVIET GROUP";
                worksheet.Cells["B1:B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["B1:B4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                worksheet.Cells["B1:B4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["B1:B4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["B1:B4"].Style.Font.Bold = true;
                worksheet.Cells["B1:B4"].Style.Font.Size = 16;

                worksheet.Cells["C1:N4"].Merge = true;
                worksheet.Cells["C1:N4"].Value = "LIST OF CUSTOMERS";
                worksheet.Cells["C1:N4"].Style.Font.Bold = true;
                worksheet.Cells["C1:N4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["C1:N4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                worksheet.Cells["C1:N4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["C1:N4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["C1:N4"].Style.Font.Bold = true;
                worksheet.Cells["C1:N4"].Style.Font.Size = 16;

                worksheet.Cells["A5:A5"].Value = "#";
                worksheet.Cells["A5:A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["A5:A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["A5:A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A5:A5"].Style.Font.Size = 14;

                worksheet.Cells["B5:B5"].Value = "Fullname";
                worksheet.Cells["B5:B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["B5:B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["B5:B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["B5:B5"].Style.Font.Size = 14;

                worksheet.Cells["C5:C5"].Value = "Email";
                worksheet.Cells["C5:C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["C5:C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["C5:C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["C5:C5"].Style.Font.Size = 14;

                worksheet.Cells["D5:D5"].Value = "Phone number";
                worksheet.Cells["D5:D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["D5:D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["D5:D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["D5:D5"].Style.Font.Size = 14;

                worksheet.Cells["E5:E5"].Value = "Company";
                worksheet.Cells["E5:E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["E5:E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["E5:E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["E5:E5"].Style.Font.Size = 14;

                worksheet.Cells["F5:F5"].Value = "City";
                worksheet.Cells["F5:F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["F5:F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["F5:F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["F5:F5"].Style.Font.Size = 14;

                worksheet.Cells["G5:G5"].Value = "District";
                worksheet.Cells["G5:G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["G5:G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["G5:G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["G5:G5"].Style.Font.Size = 14;

                worksheet.Cells["H5:H5"].Value = "Number of posting news";
                worksheet.Cells["H5:H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["H5:H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["H5:H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["H5:H5"].Style.Font.Size = 14;

                worksheet.Cells["I5:I5"].Value = "Last modified date";
                worksheet.Cells["I5:I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["I5:I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["I5:I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["I5:I5"].Style.Font.Size = 14;

                worksheet.Cells["J5:J5"].Value = "Care date";
                worksheet.Cells["J5:J5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["J5:J5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["J5:J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["J5:J5"].Style.Font.Size = 14;

                worksheet.Cells["K5:K5"].Value = "Type of customer";
                worksheet.Cells["K5:K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["K5:K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["K5:K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["K5:K5"].Style.Font.Size = 14;

                worksheet.Cells["L5:L5"].Value = "Creator";
                worksheet.Cells["L5:L5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["L5:L5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["L5:L5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["L5:L5"].Style.Font.Size = 14;

                worksheet.Cells["M5:M5"].Value = "Date of creation";
                worksheet.Cells["M5:M5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["M5:M5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["M5:M5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["M5:M5"].Style.Font.Size = 14;

                worksheet.Cells["N5:N5"].Value = "Assign to";
                worksheet.Cells["N5:N5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["N5:N5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["N5:N5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["N5:N5"].Style.Font.Size = 14;

                worksheet.Column(1).Width = 10;
                worksheet.Column(2).Width = 30;
                worksheet.Column(3).Width = 20;
                worksheet.Column(4).Width = 20;
                worksheet.Column(5).Width = 20;
                worksheet.Column(6).Width = 20;
                worksheet.Column(7).Width = 20;
                worksheet.Column(8).Width = 20;
                worksheet.Column(9).Width = 20;
                worksheet.Column(10).Width = 20;
                worksheet.Column(11).Width = 20;
                worksheet.Column(12).Width = 20;
                worksheet.Column(13).Width = 20;
                worksheet.Column(14).Width = 20;

                
                worksheet.Row(5).Height = 30;
                #endregion

                #region Lấy dữ liệu từ các điều kiện tìm kiếm
                var lstExportedCustomer = GetListCustomerByFilterCondition(model);
                #endregion

                #region Đẩy thông tin dữ liệu vào file excel
                var countFilledRecord = 1;
                for(var index = 0;index < lstExportedCustomer.Count;index++ )
                {
                    worksheet.Cells[index + 6, 1].Value = countFilledRecord;
                    worksheet.Cells[index + 6, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[index + 6, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 1].Style.Font.Size = 12;

                    worksheet.Cells[index + 6, 2].Value = lstExportedCustomer[index].FullName;
                    worksheet.Cells[index + 6, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 2].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 2].Style.WrapText = true;

                    worksheet.Cells[index + 6, 3].Value = lstExportedCustomer[index].Email;
                    worksheet.Cells[index + 6, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 3].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 3].Style.WrapText = true;

                    worksheet.Cells[index + 6, 4].Value = lstExportedCustomer[index].PhoneNumber;
                    worksheet.Cells[index + 6, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 4].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 4].Style.WrapText = true;

                    worksheet.Cells[index + 6, 5].Value = lstExportedCustomer[index].CompanyName;
                    worksheet.Cells[index + 6, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 5].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 5].Style.WrapText = true;

                    worksheet.Cells[index + 6, 6].Value = lstExportedCustomer[index].CityName;
                    worksheet.Cells[index + 6, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 6].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 6].Style.WrapText = true;

                    worksheet.Cells[index + 6, 7].Value = lstExportedCustomer[index].DistrictName;
                    worksheet.Cells[index + 6, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 7].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 7].Style.WrapText = true;

                    worksheet.Cells[index + 6, 8].Value = lstExportedCustomer[index].SumPostingNews;
                    worksheet.Cells[index + 6, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 8].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 8].Style.WrapText = true;

                    worksheet.Cells[index + 6, 9].Value = lstExportedCustomer[index].LastModifiedDate == DateTime.MinValue ? "" : lstExportedCustomer[index].LastModifiedDate.ToString("dd/MM/yyyy");
                    worksheet.Cells[index + 6, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 9].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 9].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 9].Style.WrapText = true;

                    worksheet.Cells[index + 6, 10].Value = lstExportedCustomer[index].CareDate == DateTime.MinValue ? "" : lstExportedCustomer[index].CareDate.ToString("dd/MM/yyyy");
                    worksheet.Cells[index + 6, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 10].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 10].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 10].Style.WrapText = true;

                    worksheet.Cells[index + 6, 11].Value = ((TypeOfCustomer)lstExportedCustomer[index].Type).ToTypeOfCustomerName() ;
                    worksheet.Cells[index + 6, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 11].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 11].Style.WrapText = true;

                    worksheet.Cells[index + 6, 12].Value = lstExportedCustomer[index].CreatedBy;
                    worksheet.Cells[index + 6, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 12].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 12].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 12].Style.WrapText = true;

                    worksheet.Cells[index + 6, 13].Value = lstExportedCustomer[index].CreatedDate.ToString("dd/MM/yyyy HH:mm:ss");
                    worksheet.Cells[index + 6, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 13].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 13].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 13].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 13].Style.WrapText = true;

                    worksheet.Cells[index + 6, 14].Value = lstExportedCustomer[index].AssignTo;
                    worksheet.Cells[index + 6, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 14].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 14].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 14].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 14].Style.WrapText = true;

                    countFilledRecord++;
                }

                using (var range = worksheet.Cells[1, 1, countFilledRecord + 4, 14])
                {
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.WrapText = true;
                    range.Style.Fill.BackgroundColor.SetColor(Color.White);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Top.Color.SetColor(Color.Black);
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Color.SetColor(Color.Black);
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Color.SetColor(Color.Black);
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                    range.Style.ShrinkToFit = false;
                }

                worksheet.Cells["A5:N5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A5:N5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#4d90fe"));
                worksheet.Cells["A5:N5"].Style.Font.Color.SetColor(Color.White);
                #endregion

                var stream = new MemoryStream();
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                package.SaveAs(stream);
                stream.Position = 0;
                return File(stream, contentType, "CUSTOMERS-" + DateTime.Now.ToString("yyyy-MM-dd--hh-mm-ss") + ".xlsx");
            }
        }

        /// <summary>
        /// Gọi API bên Admin để thêm mới / cập nhật tài khoản đồng bộ với file khách hàng
        /// </summary>
        /// <param name="adminId"></param>
        /// <param name="fullName"></param>
        /// <param name="email"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="address"></param>
        /// <param name="isChangeEmail"></param>
        /// <returns></returns>
        [NonActionAttribute]
        public Response CallAPIAdminInsertUpdateAccount(long adminId,string fullName,string email,string phoneNumber ,string address , bool isChangeEmail)
        {
            // Gọi api thêm mới thông tin tài khoản khách hàng trên site
            var url = string.Format("{0}/BCRMService.svc/UpdateCustomerInfo", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var token = Security.CreateKey();

            var data = new
            {
                Token = Security.CreateKey(),
                AdminId = adminId,
                FullName = fullName,
                Email = string.IsNullOrEmpty(email) ? "" : email.ToLower().Trim(),
                PhoneNumber = phoneNumber,
                Address = address
                //,
                //IsChangeEmail = isChangeEmail
            };
            var res = BCRM.BXH.ThaiLand.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(data), "application/json");

            if (string.IsNullOrEmpty(res))
            {
                return new Response()
                {
                    Code = SystemCode.ErrorConnect,
                    Message = "Error in process , Contact IT for detail !"
                };
            }
            var hrmResponse = JsonConvert.DeserializeObject<Response>(res);
            return hrmResponse;
        }
        [NonActionAttribute]
        public Response CallAPIAdminGetPostingNews(List<long> lstAdminId)
        {
            // Gọi api thêm mới thông tin tài khoản khách hàng trên site
            var url = string.Format("{0}/BCRMService.svc/GetCustomerPostingNumber", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var data = new
            {
                Token = Security.CreateKey(),
                ListAdminId = lstAdminId
            };
            var res = BCRM.BXH.ThaiLand.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(data), "application/json");

            if (string.IsNullOrEmpty(res))
            {
                return new Response()
                {
                    Code = SystemCode.ErrorConnect,
                    Message = "Error in process , Contact IT for detail !"
                };
            }
            var hrmResponse = JsonConvert.DeserializeObject<Response>(res);
            
            return hrmResponse;
        }
        [NonActionAttribute]
        public Response CallAPICheckExistPhoneNumberInCrawlerCustomer(string phoneNumber)
        {
            string nullString = null;
            // Kiểm tra thông tin số điện thoại có tồn tại trong các khoản crawler hay không
            var url = string.Format("{0}/BCRMService.svc/GetListCrawlerCustomer", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var data = JsonConvert.SerializeObject(new
            {
                Token = Security.CreateKey(),
                FilterKeyword = phoneNumber,
                Status = -1,
                PageIndex = 1,
                PageSize = 25,
                DateOfCreationFrom = nullString,
                DateOfCreationTo = nullString,
                TotalRecord = 0,
                ListData = new List<CrawlerCustomerSearchModel>(),
                ListIgnoreAdminId = new List<long>(),
                ListFilterAdminId = new List<long>()
            });
            var res = BCRM.BXH.ThaiLand.Library.Common.MakePostRequest(url, data, "application/json");
            var result = JsonConvert.DeserializeObject<Response>(res);
            return result;
        }
        [NonActionAttribute]
        public List<CustomerHistory> GetChangedCustomerInfo(Customer oldCustomerInfo,Customer newCustomerInfo)
        {
            var lstChanged = new List<CustomerHistory>();
            // Kiểm tra có thay đổi tên khách hàng hay không
            if(!oldCustomerInfo.FullName.Equals(newCustomerInfo.FullName))
            {
                lstChanged.Add(new CustomerHistory() {
                    Action = "Fullname",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.FullName,
                    NewValue = newCustomerInfo.FullName,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            // Kiểm tra có thay đổi số điện thoại hay không
            if(!oldCustomerInfo.PhoneNumber.Equals(newCustomerInfo.PhoneNumber))
            {
                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Phone number",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.PhoneNumber,
                    NewValue = newCustomerInfo.PhoneNumber,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            // Kiểm tra có thay đổi email hay không
            if (!oldCustomerInfo.Email.Equals(newCustomerInfo.Email))
            {
                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Email",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.Email,
                    NewValue = newCustomerInfo.Email,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            // Kiểm tra có thay đổi loại khách hàng hay không
            if (!oldCustomerInfo.Type.Equals(newCustomerInfo.Type))
            {
                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Type of customer",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = ((TypeOfCustomer)oldCustomerInfo.Type).ToTypeOfCustomerName() ,
                    NewValue = ((TypeOfCustomer)newCustomerInfo.Type).ToTypeOfCustomerName(),
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }

            // Kiểm tra có thay đổi vùng miền hay không
            if (!oldCustomerInfo.RegionId.Equals(newCustomerInfo.RegionId))
            {
                var regionLayer = new RegionLayer();
                var oldRegion = regionLayer.GetById(oldCustomerInfo.RegionId);
                var oldRegionName = oldRegion == null ? "" : oldRegion.Name;
                var newRegion = regionLayer.GetById(newCustomerInfo.RegionId);
                var newRegionName = newRegion == null ? "" : newRegion.Name;

                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Region",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldRegionName,
                    NewValue = newRegionName,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }

            // Kiểm tra có thay đổi thành phố hay không
            if (!oldCustomerInfo.CityId.Equals(newCustomerInfo.CityId))
            {
                var cityLayer = new CityLayer();
                var oldCity = cityLayer.GetById(oldCustomerInfo.CityId);
                var oldCityName = oldCity == null ? "" : oldCity.Name;
                var newCity = cityLayer.GetById(newCustomerInfo.CityId);
                var newCityName = newCity == null ? "" : newCity.Name;

                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Province/City",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCityName,
                    NewValue = newCityName,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }

            // Kiểm tra có thay đổi quận huyện hay không
            if (!oldCustomerInfo.DistrictId.Equals(newCustomerInfo.DistrictId))
            {
                var districtLayer = new DistrictLayer();
                var oldDistrict = districtLayer.GetById(oldCustomerInfo.DistrictId);
                var oldDistrictName = oldDistrict == null ? "" : oldDistrict.Name;
                var newDistrict = districtLayer.GetById(newCustomerInfo.DistrictId);
                var newDistrictName = newDistrict == null ? "" : newDistrict.Name;

                lstChanged.Add(new CustomerHistory()
                {
                    Action = "District",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldDistrictName,
                    NewValue = newDistrictName,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            // Kiểm tra có thay đổi thông tin địa chỉ hay không
            if (!oldCustomerInfo.Address.Equals(newCustomerInfo.Address))
            {
                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Address",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.Address,
                    NewValue = newCustomerInfo.Address,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }

            // Kiểm tra có thay đổi thông tin miêu tả hay không
            if (!oldCustomerInfo.Description.Equals(newCustomerInfo.Description))
            {
                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Description",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.Description,
                    NewValue = newCustomerInfo.Description,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            return lstChanged;
        }
        [NonActionAttribute]
        public List<CustomerForIndex> GetListCustomerByFilterCondition( CustomerIndexModel model )
        {
            if (string.IsNullOrEmpty(model.Creator))
            {
                model.Creator = "";
            }
            // Kiểm tra xem người dùng có quyền admin hoặc manager hay không ?
            var hasPermit = UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager);
            // Kiểm tra xem người dùng có tìm kiếm theo số điện thoại hay không
            var lstPhone = new List<string>();
            if (!string.IsNullOrEmpty(model.FilterKeyword))
            {
                lstPhone = Ultility.DetectPhoneNumber(model.FilterKeyword);
            }
            var custLayer = new CustomerLayer();
            var pager = new Pager();
            var lstCus = new List<CustomerForIndex>();
            if (lstPhone.Count > 0)
            {
                lstCus = custLayer.SelectByPhoneNumber(lstPhone.Select(p => p).ToArray());
                return lstCus;
            }
            else
            {
                // TH tìm tất cả các khách hàng
                if (model.AssignTo.Equals("-1"))
                {
                    // Bao gồm việc tìm kiếm các khách hàng được giao cho người dùng đang đăng nhập
                    model.AssignTo = UserContext.UserName;
                    // Lấy ra danh sách nhóm mà người dùng có quyền xem
                    var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
                    var groupLayer = new GroupLayer();
                    if (lstRoledGroup.Contains(DefaultSystemParameter.AllGroup))
                    {
                        lstRoledGroup.Remove(DefaultSystemParameter.AllGroup);
                        lstRoledGroup.AddRange(groupLayer.GetAll().Select(o => o.Id).ToList());
                    }

                    // Bổ sung điều kiện tìm kiếm các khách 
                    // hàng không thuộc nhóm nào
                    lstRoledGroup.Add(DefaultSystemParameter.CustomerNotBelongToGroup);

                    lstCus = custLayer.SearchAllToExport(model.Sort,
                                           model.Status.HasValue ? model.Status.Value : -1,
                                           model.Type.HasValue ? model.Type.Value : -1,
                                           model.AssignTo,
                                           model.CityId.HasValue ? model.CityId.Value : -1,
                                           model.DistrictId.HasValue ? model.DistrictId.Value : -1,
                                           model.StartDate.Value,
                                           model.EndDate.Value,
                                           model.FilterKeyword ?? string.Empty,
                                           lstRoledGroup,
                                           hasPermit,
                                           model.Creator);
                }
                // TH tìm các khách hàng chưa được gán cho sale
                else if (model.AssignTo.Equals("0"))
                {
                    lstCus = custLayer.SearchUnAssignToExport(model.Sort,
                                           model.Status.HasValue ? model.Status.Value : -1,
                                           model.Type.HasValue ? model.Type.Value : -1,
                                           model.CityId.HasValue ? model.CityId.Value : -1,
                                           model.DistrictId.HasValue ? model.DistrictId.Value : -1,
                                           model.StartDate.Value,
                                           model.EndDate.Value,
                                           model.FilterKeyword ?? string.Empty,
                                           hasPermit,
                                           model.Creator);
                }
                // TH tìm kiếm theo từng sale cụ thể
                else
                {
                    var assignTo = UserContext.UserName;
                    var cacheUser = new IISUser();
                    var sale = cacheUser.GetUserInfo(model.AssignTo);
                    if (sale.Status == UserStatus.NotAvaiable.GetHashCode()
                        || (UserContext.HasRole(RoleSystem.Admin))
                        || (UserContext.HasRole(RoleSystem.TeamLeader, RoleSystem.Manager)
                            && UserContext.ListRoleGroup.Select(r => r.Key == RoleSystem.TeamLeader.GetHashCode()
                                                                  && r.Value.Contains(sale.GroupId)) != null))
                    {
                        assignTo = model.AssignTo;
                    }

                    lstCus = custLayer.SearchToExport(model.Sort,
                                               model.Status.HasValue ? model.Status.Value : -1,
                                               model.Type.HasValue ? model.Type.Value : -1,
                                               assignTo,
                                               model.CityId.HasValue ? model.CityId.Value : -1,
                                               model.DistrictId.HasValue ? model.DistrictId.Value : -1,
                                               model.StartDate.Value,
                                               model.EndDate.Value,
                                               model.FilterKeyword ?? string.Empty,
                                               hasPermit,
                                               model.Creator
                                               );
                }
            }
            // Lấy ra danh sách các tài khoản tương ứng với 

            if (lstCus.Count > 0)
            {
                var synBCRMNAdminLayer = new SyncBcrmBxhNAdminLayer();
                var strCustomerIds = string.Join(",", lstCus.Select(o => o.Id).ToList());
                var lstAdminInfo = synBCRMNAdminLayer.GetByListCustomerId(strCustomerIds);

                // Gọi API  để lấy ra các tin đăng
                var response = CallAPIAdminGetPostingNews(lstAdminInfo.Select(o => o.AccountId).ToList());

                var lstSumPostingNews = JsonConvert.DeserializeObject<List<AdminPostingNewsModel>>(response.Data);
                foreach (var customerItemInfo in lstCus)
                {
                    var adminInfo = lstAdminInfo.Find(o => o.CustomerId == customerItemInfo.Id);
                    if (adminInfo != null)
                    {
                        var adminSumPostingNews = ((List<AdminPostingNewsModel>)lstSumPostingNews).Find(o => o.AdminId == adminInfo.AccountId);
                        if (adminSumPostingNews != null)
                        {
                            customerItemInfo.SumPostingNews = adminSumPostingNews.SumPostingNews;
                        }
                    }
                }
            }

            return lstCus;
        }
    }
}