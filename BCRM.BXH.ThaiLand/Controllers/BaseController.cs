﻿using BCRM.BXH.ThaiLand.Common;
using BCRM.BXH.ThaiLand.Error;
using BCRM.BXH.ThaiLand.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace BCRM.BXH.ThaiLand.Controllers
{
    [HandleCustomError]
    public class BaseController : Controller
    {
        //public BaseController() 
        //{
        //    var lang = "";
        //    HttpCookie langCookie = Request.Cookies["culture"];
        //    if (langCookie != null)
        //    {
        //        lang = langCookie.Value;
        //    }
        //    else
        //    {
        //        var userLanguage = Request.UserLanguages;
        //        var userLang = userLanguage != null ? userLanguage[0] : "";
        //        if (userLang != "")
        //        {
        //            lang = userLang;
        //        }
        //        else
        //        {
        //            lang = ResourceManager.GetDefaultLanguage();
        //        }
        //    }
        //    new ResourceManager().SetLanguage(lang);
        //}
                
        public UserAuthenticateModel UserContext
        {
            get { return GetCurrentUser(); }
        }
        private UserAuthenticateModel GetCurrentUser()
        {
            if (System.Web.HttpContext.Current == null) return null;
            var user = System.Web.HttpContext.Current.User;
            if (user == null)
            {
                return null;
            }
            if (!string.IsNullOrWhiteSpace(user.Identity.Name))
            {
                return JsonConvert.DeserializeObject<UserAuthenticateModel>(user.Identity.Name);
            }
            return null;
        }
    }

    public class FilterRole : ActionFilterAttribute
    {
        public List<int> ListRole { get; set; }
        public FilterRole(int permission)
        {
            ListRole = new List<int>();
            ListRole.Add(permission);
        }
        public FilterRole(params int[] arrRole)
        {
            ListRole = new List<int>(arrRole);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var url = WebConfigurationManager.AppSettings["LoginUrl"];
            var currentUser = new BaseController().UserContext;
            if (currentUser != null)
            {
                var isPermit = false;
                if (!currentUser.HasRole(RoleSystem.Admin))
                {
                    foreach (var role in ListRole)
                    {
                        if (currentUser.HasRole(role))
                        {
                            isPermit = true;
                            break;
                        }
                    }
                }
                else
                {
                    isPermit = true;
                }
                if (!isPermit)
                {
                    filterContext.Result = new RedirectResult(url);
                    return;
                }
            }
            else
            {
                filterContext.Result = new RedirectResult(url);
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }

    public class FilterAuthorize : ActionFilterAttribute
    {
        Stopwatch stopWatch;
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            this.stopWatch = new Stopwatch();
            stopWatch.Start();
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var url = WebConfigurationManager.AppSettings["LoginUrl"];
                filterContext.Result = new RedirectResult(url);
                return;
            }
            base.OnActionExecuting(filterContext);
        }
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            stopWatch.Stop();
            ElapsedTimeCommon.TrackingTime(filterContext.RouteData, stopWatch.ElapsedMilliseconds);
        }
    }
}