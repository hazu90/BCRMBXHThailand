﻿using BCRM.BXH.ThaiLand.DAL;
using BCRM.BXH.ThaiLand.Library;
using BCRM.BXH.ThaiLand.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using DVS.Algorithm;
using BCRM.BXH.ThaiLand.Cache;
using BCRM.BXH.ThaiLand.Filter;

namespace BCRM.BXH.ThaiLand.Controllers
{
    public class AccountController : BaseController
    {
        [Localization]
        public ActionResult LogOn()
        {
            return View(new LogonViewModel());
        }

        [HttpPost]
        public ActionResult LogOn(string username, string password,string language,  bool remember = false)
        {
            var userLayer = new UserLayer();
            var user = userLayer.GetByUserName(username);
            if (user == null || !password.Trim().ToMD5().Equals(user.Password) || user.Status != UserStatus.Avaiable.GetHashCode())
            {
                var response = new LogonViewModel
                {
                    UserName = username,
                    Password = password,
                    Status = false
                };
                return View(response);
            }

            if (!language.Equals("th")
              && !language.Equals("en"))
            {
                language = "en";
            }

            var userCookie = new HttpCookie("culture", language);
            userCookie.Expires.AddDays(365);
            HttpContext.Response.Cookies.Add(userCookie);

            var cacheUser = new IISUser();
            cacheUser.RemoveUserInfo(user.UserName);
            var authentication = cacheUser.SetUserInfo(user);
            FormsAuthentication.SetAuthCookie(JsonConvert.SerializeObject(authentication, Newtonsoft.Json.Formatting.None), remember);
            userLayer.UpdateLastActivity(user.Id);
            return RedirectToAction("Index", "Home");
        }
        public RedirectResult LogOff()
        {
            FormsAuthentication.SignOut();
            return Redirect(Url.Action("LogOn", "Account"));
        }
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        [FilterAuthorize]
        public JsonResult ChangePasswordUser(ChangePasswordModel model)
        {
            var response = new Response();

            var user = UserContext;
            var userLayer = new UserLayer();
            try
            {
                var password = userLayer.GetByUserName(user.UserName).Password;
                if (password == null)
                {
                    response.Code = SystemCode.Error;
                    response.Message = "Old password is not correct!";
                }
                user.Password = password;
                if (user.Password == model.OldPassword.ToMD5())
                {
                    if (model.NewPassword.Equals(model.ConfirmPassword))
                    {
                        user.Password = model.NewPassword.ToMD5();

                        var rpm = new ResetPasswordModel { Id = user.Id, NewPassword = model.NewPassword.ToMD5() };
                        userLayer.ResetPassword(rpm);
                        response.Code = SystemCode.Success;
                    }
                }
                else
                {
                    response.Code = SystemCode.Error;
                    response.Message = "Old password is not correct!";
                }

            }
            catch (Exception exception)
            {
                response.Code = SystemCode.Error;
                response.Message = exception.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}