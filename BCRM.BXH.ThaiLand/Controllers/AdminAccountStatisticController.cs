﻿using BCRM.BXH.ThaiLand.DAL;
using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Filter;
using BCRM.BXH.ThaiLand.Language;
using BCRM.BXH.ThaiLand.Library;
using BCRM.BXH.ThaiLand.Models;
using DVS.Algorithm;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.ThaiLand.Controllers
{
    public class AdminAccountStatisticController : BaseController
    {
        [FilterAuthorize]
        [Localization]
        public ActionResult Index()
        {
            // Lấy ra danh sách mà nhân viên có quyền 
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            var groupLayer = new GroupLayer();
            var isSearchAll = false;
            var lstGroup = new List<Group>();
            var lstUsers = new List<UserSearchModel>();
            if (UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
            {
                isSearchAll = true;
                var allGroups = groupLayer.GetAll();
                lstGroup = lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                                ? allGroups : allGroups.FindAll(o => lstRoledGroup.Contains(o.Id));
                var groupIds = string.Join(",", lstGroup.Select(o => o.Id).ToList());
                var userLayer = new UserLayer();
                lstUsers = userLayer.GetList(groupIds);
            }
            else
            {
                var groupInfo = groupLayer.GetById(UserContext.GroupId);
                lstGroup = new List<Group>() { groupInfo };
                lstUsers = new List<UserSearchModel>() { new UserSearchModel() { UserName = UserContext.UserName } };
            }
            var model = new AdminAccountStatisticIndexModel();
            if (DateTime.Now.Day <= 6)
            {
                var beforeMonth = DateTime.Now.AddMonths(-1);
                model.TimeToCreationFrom = new DateTime(beforeMonth.Year, beforeMonth.Month, 1);
                model.TimeToCreationTo = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).AddDays(-1);
            }
            else
            {
                var afterMonth = DateTime.Now.AddMonths(1);
                model.TimeToCreationFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                model.TimeToCreationTo = (new DateTime(afterMonth.Year, afterMonth.Month, 1)).AddDays(-1);
            }
            model.SetListGroup(isSearchAll, lstGroup);
            model.SetListUsers(isSearchAll, lstUsers);
            return View(model);
        }
        [FilterAuthorize]
        [Localization]
        public ActionResult Search(AdminAccountStatisticIndexModel model)
        {
            var isManager = UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager);

            if (!isManager && (string.IsNullOrEmpty(model.AssignTo) || !model.AssignTo.Equals(UserContext.UserName)  ))
            {
                return View(new List<AdminAccountStatisticSearchModel>());
            }
            var userLayer = new UserLayer();
            var lstUserId = new List<int>();
            var groupLayer = new GroupLayer();
            if (!string.IsNullOrEmpty(model.AssignTo))
            {
                var userInfo = userLayer.GetByUserName(model.AssignTo);
                if (userInfo == null)
                {
                    return View(new List<AdminAccountStatisticSearchModel>());
                }
                lstUserId.Add(userInfo.Id);
            }
            else
            {
                // Lấy ra danh sách mà nhân viên có quyền 
                var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
                if (model.GroupId == 0)
                {
                    var allGroups = groupLayer.GetAll();
                    var lstGroup = lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                                    ? allGroups : allGroups.FindAll(o => lstRoledGroup.Contains(o.Id));
                    var groupIds = string.Join(",", lstGroup.Select(o => o.Id).ToList());
                    var lstUsers = userLayer.GetList(groupIds);
                    lstUserId.AddRange(lstUsers.Select(o => o.Id).ToList());
                }
                else
                {
                    if(!lstRoledGroup.Contains(DefaultSystemParameter.AllGroup) && !lstRoledGroup.Contains(model.GroupId) )
                    {
                        return View(new List<AdminAccountStatisticSearchModel>());
                    }

                    var lstUsers = userLayer.GetByGroupId(model.GroupId);
                    lstUserId.AddRange(lstUsers.Select(o => o.Id).ToList());
                }
            }
            // Lấy thông tin thống kế số tài khoản được nhận
            var customerCareHistoryLayer = new CustomerCareHistoryLayer();
            var result = customerCareHistoryLayer.GetReport(lstUserId,model.TimeToCreationFrom.Value,model.TimeToCreationTo.Value);

            var lstSaleId = result.Select(o => o.UserId).ToList();

            // Lấy ra các tài khoản được tạo trong khoảng thời gian này
            var lstSuccessContact = customerCareHistoryLayer.GetSuccessContactInTime(lstSaleId, model.TimeToCreationFrom.Value, model.TimeToCreationTo.Value);
            if(lstSuccessContact.Count >0)
            {
                var synBCRMNAdminLayer = new SyncBcrmBxhNAdminLayer();
                var strCustomerIds = string.Join(",", lstSuccessContact.Select(o => o.CustomerId).ToList());
                var lstAdminInfo = synBCRMNAdminLayer.GetByListCustomerId(strCustomerIds);

                // Gọi API để lấy ra các tài khoản active
                var response = CallAPIAdminGetActivedAccount(lstAdminInfo.Select(o => o.AccountId).ToList(), model.TimeToCreationFrom.Value, model.TimeToCreationTo.Value);
                var lstActived = JsonConvert.DeserializeObject<List<long>>((string)response.Data);
                
                foreach(var item in result)
                {
                    var lstSuccessContactForEachSaler = lstSuccessContact.FindAll(o => o.UserId == item.UserId);
                    var lstCustomerId = lstSuccessContactForEachSaler.Select(o => o.CustomerId).ToList();
                    var lstCustomerAccountForEachSaler = lstAdminInfo.FindAll(o => lstCustomerId.Contains(o.CustomerId));
                    item.SumActivedSiteCustomer = lstCustomerAccountForEachSaler.FindAll(o => lstActived.Contains(o.AccountId)).Count;
                }
            }

            return View(result);
        }
        [FilterAuthorize]
        public JsonResult GetUserByGroupId( int groupId)
        {
            var lstShowedAssignTo = new List<KeyValuePair<string, string>>();
            if(!UserContext.HasRole(RoleSystem.Admin,RoleSystem.Manager))
            {
                lstShowedAssignTo.Add(new KeyValuePair<string, string>(UserContext.UserName, UserContext.UserName));
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = lstShowedAssignTo
                }, JsonRequestBehavior.AllowGet);
            }

            var groupLayer = new GroupLayer();
            var userLayer = new UserLayer();
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            if (groupId == 0)
            {
                var allGroups = groupLayer.GetAll();
                var  lstGroup = lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                                ? allGroups : allGroups.FindAll(o => lstRoledGroup.Contains(o.Id));
                var groupIds = string.Join(",", lstGroup.Select(o => o.Id).ToList());
                var lstUser = userLayer.GetList(groupIds);
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = lstUser.Select(o => new KeyValuePair<string, string>(o.UserName, o.UserName)).ToList()
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // Lấy danh sách người dùng thuộc nhóm đó
                var lstUser = userLayer.GetByGroupId(groupId);
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = lstUser.Select(o => new KeyValuePair<string, string>(o.UserName, o.UserName)).ToList()
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [NonActionAttribute]
        public Response CallAPIAdminGetActivedAccount(List<long> lstAdminId, DateTime fromDate, DateTime toDate)
        {
            // Gọi api thêm mới thông tin tài khoản khách hàng trên site
            var url = string.Format("{0}/BCRMService.svc/CustomerCheckActive", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var data = new
            {
                Token = Security.CreateKey(),
                ListAdminId = lstAdminId,
                FromDate = fromDate.ToString("dd/MM/yyyy"),
                ToDate = toDate.ToString("dd/MM/yyyy")
            };
            var res = BCRM.BXH.ThaiLand.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(data), "application/json");

            if (string.IsNullOrEmpty(res))
            {
                return new Response()
                {
                    Code = SystemCode.ErrorConnect,
                    //Message =  "Error in process , Contact IT for detail !"
                    Message = LanguageRes.ErrorProcess
                };
            }
            var hrmResponse = JsonConvert.DeserializeObject<Response>(res);
            return hrmResponse;
        }
    }
}