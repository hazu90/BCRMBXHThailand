﻿using BCRM.BXH.ThaiLand.DAL;
using BCRM.BXH.ThaiLand.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.ThaiLand.Controllers
{
    public class CityController : BaseController
    {
        [FilterAuthorize]
        public JsonResult GetCityByRegion(int regionId)
        {
            var response = new Response();
            try
            {
                var cityLayer = new CityLayer();
                var lstCity = cityLayer.GetByRegionId(regionId);
                response.Data = lstCity;
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult GetDistrictByCity(int cityId)
        {
            var response = new Response();
            try
            {
                var districtLayer = new DistrictLayer();
                var lstDistrict = districtLayer.GetByCityId(cityId);
                response.Data = lstDistrict;
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}