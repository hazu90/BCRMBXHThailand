﻿using BCRM.BXH.ThaiLand.DAL;
using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Filter;
using BCRM.BXH.ThaiLand.Library;
using BCRM.BXH.ThaiLand.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.ThaiLand.Controllers
{
    public class GroupController : BaseController
    {
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        [Localization]
        public ActionResult Index()
        {
            var groupLayer = new GroupLayer();
            var lstGroup = groupLayer.GetAll();
            var lstGroupSearch = new List<GroupIndexModel>();
            var userLayer = new UserLayer();
            var isAdmin = false;
            if (UserContext.HasRole(RoleSystem.Admin))
            {
                isAdmin = true;
            }
            foreach (var group in lstGroup)
            {
                var userName = "";
                if (group.Leader > 0)
                {
                    var userInfo = userLayer.GetById(group.Leader);
                    userName = userInfo.UserName;
                }

                lstGroupSearch.Add(new GroupIndexModel()
                {
                    LeaderName = userName,
                    Description = group.Description,
                    Id = group.Id,
                    Leader = group.Leader,
                    Name = group.Name,
                    IsEditable = isAdmin
                });
            }
            ViewBag.CurrUser = UserContext;
            ViewBag.LstUser = (new UserLayer()).GetAll().FindAll(o => o.Status == UserStatus.Avaiable.GetHashCode());
            return View(lstGroupSearch);
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public JsonResult Create()
        {
            var response = new Response();
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public JsonResult CreateAction(Group model)
        {
            var groupLayer = new GroupLayer();
            if (string.IsNullOrEmpty(model.Description))
            {
                model.Description = "";
            }
            groupLayer.Create(model);
            var response = new Response();
            response.Code = SystemCode.Success;
            response.Message = "You created new group successfully";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public JsonResult Edit(int id)
        {
            var groupLayer = new GroupLayer();
            var groupInfo = groupLayer.GetById(id);
            var response = new Response();
            response.Data = groupInfo;
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public JsonResult EditAction(Group model)
        {
            var groupLayer = new GroupLayer();
            if (string.IsNullOrEmpty(model.Description))
            {
                model.Description = "";
            }
            groupLayer.Edit(model);
            var response = new Response();
            response.Code = SystemCode.Success;
            response.Message = "You editted data successfully";
            return Json(response, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public ActionResult Search()
        {
            var groupLayer = new GroupLayer();
            var lstGroup = groupLayer.GetAll();
            var lstGroupSearch = new List<GroupIndexModel>();
            var userLayer = new UserLayer();
            var isAdmin = false;
            if(UserContext.HasRole(RoleSystem.Admin))
            {
                isAdmin = true;
            }

            foreach (var group in lstGroup)
            {
                var userName = "";
                if (group.Leader > 0)
                {
                    var userInfo = userLayer.GetById(group.Leader);
                    userName = userInfo.UserName;
                }

                lstGroupSearch.Add(new GroupIndexModel()
                {
                    LeaderName = userName,
                    Description = group.Description,
                    Id = group.Id,
                    Leader = group.Leader,
                    Name = group.Name,
                    IsEditable = isAdmin
                });
            }
            return View(lstGroupSearch);
        }
    }
}