﻿using BCRM.BXH.ThaiLand.DAL;
using BCRM.BXH.ThaiLand.Library;
using BCRM.BXH.ThaiLand.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.ThaiLand.Controllers
{
    public class UserRoleGroupController : BaseController
    {
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin)]
        public ActionResult Search(int userId)
        {
            var userRoleGroupLayer = new UserRoleGroupLayer();
            var lstUserRoleGroup = userRoleGroupLayer.GetList(userId);
            foreach (var item in lstUserRoleGroup)
            {
                if (item.GroupId == -1000)
                {
                    item.GroupName = "All of groups";
                }
                item.RoleName = Helper.ToPermissionName(item.RoleId);
            }
            return View(lstUserRoleGroup);
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin)]
        public JsonResult CreateAction(int userId, int roleId, int groupId)
        {
            if (groupId == 0)
            {
                groupId = -1000;
            }
            var userRoleGroupLayer = new UserRoleGroupLayer();
            var lstUserRoleGroup = userRoleGroupLayer.GetRoleGroupByUserId(userId);
            var response = new Response();
            if (lstUserRoleGroup.Find(o => o.GroupId == groupId && o.RoleId == roleId) != null)
            {
                response.Code = SystemCode.ErrorExist;
                response.Message = "This user has set this permission for the group!";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                userRoleGroupLayer.Create(userId, groupId, roleId);
            }
            response.Message = "You have added new role successfully";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin)]
        public JsonResult Delete(int userId, int groupId, int roleId)
        {
            var userRoleGroupLayer = new UserRoleGroupLayer();
            userRoleGroupLayer.Delete(userId, groupId, roleId);
            var response = new Response
            {
                Code = SystemCode.Success,
                Message = "You deleted this role successfully !"
            };
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}