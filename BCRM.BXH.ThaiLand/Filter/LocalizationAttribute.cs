﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.ThaiLand.Filter
{
    public class LocalizationAttribute : ActionFilterAttribute
    {
        private string _DefaultLanguage
        {
            get
            {
                return ConfigurationManager.AppSettings["LocalLanguage"].ToString();
            }
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string lang = filterContext.HttpContext.Request.Cookies["culture"] == null ? _DefaultLanguage : filterContext.HttpContext.Request.Cookies["culture"].Value;
            try
            {
                var cultureInfo = new CultureInfo(lang);
                //Thread.CurrentThread.CurrentCulture = cultureInfo;
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureInfo.Name);
                base.OnActionExecuting(filterContext);
            }
            catch 
            {
                throw new NotSupportedException(String.Format("ERROR: Invalid language code '{0}'.", lang));
            }
        }
    }
}