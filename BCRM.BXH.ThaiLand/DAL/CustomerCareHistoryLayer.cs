﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using BCRM.BXH.ThaiLand.Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.DAL
{
    public class CustomerCareHistoryLayer
    {
        public List<SumReceivedCustomerModel> GetSumReceived(List<int> lstUserId,DateTime startDate,DateTime endDate )
        {
            var ret = new List<SumReceivedCustomerModel>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_getsumreceivedcustomer", true))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var resultInfo = new SumReceivedCustomerModel();
                            EntityBase.SetObjectValue(reader, ref resultInfo);
                            if (resultInfo != null)
                            {
                                ret.Add(resultInfo);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public List<AdminAccountStatisticSearchModel> GetReport(List<int> lstUserId, DateTime startDate, DateTime endDate)
        {
            var ret = new List<AdminAccountStatisticSearchModel>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_getreport", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_lstuserid", lstUserId));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var resultInfo = new AdminAccountStatisticSearchModel();
                            EntityBase.SetObjectValue(reader, ref resultInfo);
                            if (resultInfo != null)
                            {
                                ret.Add(resultInfo);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public int Create(CustomerCareHistory model )
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_create", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_adminid", model.AdminId));
                    command.Parameters.Add(new NpgsqlParameter("_crawleradminid", model.CrawlerAdminId));
                    command.Parameters.Add(new NpgsqlParameter("_receivedby", model.ReceivedBy));
                    command.Parameters.Add(new NpgsqlParameter("_receiveddate", model.ReceivedDate));
                    command.Parameters.Add(new NpgsqlParameter("_note", string.IsNullOrEmpty(model.Note) ? "" : model.Note ));
                    command.Parameters.Add(new NpgsqlParameter("_userid", model.UserId));
                    command.Parameters.Add(new NpgsqlParameter("_customerid", model.CustomerId));
                    return ((int)command.ExecuteScalar());
                }
            }
        }
        public void UpdateSuccessContact(int id, int phoningStatus,DateTime successDate)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_updatesuccesscontact", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.Parameters.Add(new NpgsqlParameter("_phoningstatus", phoningStatus));
                    command.Parameters.Add(new NpgsqlParameter("_successcontactdate", successDate));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdateFailureContact(int id, int phoningStatus, DateTime failureDate)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_updatefailurecontact", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.Parameters.Add(new NpgsqlParameter("_phoningstatus", phoningStatus));
                    command.Parameters.Add(new NpgsqlParameter("_failurecontactdate", failureDate));
                    command.ExecuteNonQuery();
                }
            }
        }
        public List<CustomerCareHistory> GetByCrawlerAdminId(long crawlerAdminId)
        {
            var ret = new List<CustomerCareHistory>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_getbycrawleradminid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_crawleradminid", crawlerAdminId));

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var resultInfo = new CustomerCareHistory();
                            EntityBase.SetObjectValue(reader, ref resultInfo);
                            if (resultInfo != null)
                            {
                                ret.Add(resultInfo);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public List<CustomerCareHistory> GetIgnoreByListUserId(List<int> lstEmployeeId ,DateTime dateNow )
        {
            var ret = new List<CustomerCareHistory>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_getignoreduserid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_datenow", dateNow));
                    command.Parameters.Add(new NpgsqlParameter("_lstuserid", lstEmployeeId));

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var resultInfo = new CustomerCareHistory();
                            EntityBase.SetObjectValue(reader, ref resultInfo);
                            if (resultInfo != null)
                            {
                                ret.Add(resultInfo);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public List<CustomerCareHistory> GetByListUserId(List<int> lstEmployeeId, DateTime dateNow)
        {
            var ret = new List<CustomerCareHistory>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_getbylstuserid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_datenow", dateNow));
                    command.Parameters.Add(new NpgsqlParameter("_lstuserid", lstEmployeeId));

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var resultInfo = new CustomerCareHistory();
                            EntityBase.SetObjectValue(reader, ref resultInfo);
                            if (resultInfo != null)
                            {
                                ret.Add(resultInfo);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public CustomerCareHistory GetById(int id)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_getbyid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));

                    using (var reader = command.ExecuteReader())
                    {
                        if(reader.Read())
                        {
                            var resultInfo = new CustomerCareHistory();
                            EntityBase.SetObjectValue(reader, ref resultInfo);
                            return resultInfo;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
        public void UpdateNote(int id, string note)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_updatenote", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.Parameters.Add(new NpgsqlParameter("_note", note));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdateReturnStatus(int id, bool isreturn)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_updatedisreturned", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.Parameters.Add(new NpgsqlParameter("_isreturned", isreturn));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdateCustomerIdByCrawlerAdminId(long crawlerAdminId,int customerId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_updatecustomerid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_crawleradminid", crawlerAdminId));
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.ExecuteNonQuery();
                }
            }
        }
        public List<CustomerCareHistory> GetByCustomerId(int customerId)
        {
            var ret = new List<CustomerCareHistory>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_getbycustomerid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var resultInfo = new CustomerCareHistory();
                            EntityBase.SetObjectValue(reader, ref resultInfo);
                            if (resultInfo != null)
                            {
                                ret.Add(resultInfo);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public List<CustomerCareHistory> GetLastestCareHistoryByPhoningStatus(int phoningStatus)
        {
            var ret = new List<CustomerCareHistory>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_getbyphoningstatus", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_phoningstatus", phoningStatus));

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var resultInfo = new CustomerCareHistory();
                            EntityBase.SetObjectValue(reader, ref resultInfo);
                            if (resultInfo != null)
                            {
                                ret.Add(resultInfo);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public List<CustomerCareHistory> GetSuccessContactInTime(List<int> ldSaleId ,DateTime fromDate,DateTime toDate )
        {
            var ret = new List<CustomerCareHistory>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customercarehistory_getsuccesscontactbytime", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_lstuserid", ldSaleId));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", fromDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", toDate));

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var resultInfo = new CustomerCareHistory();
                            EntityBase.SetObjectValue(reader, ref resultInfo);
                            if (resultInfo != null)
                            {
                                ret.Add(resultInfo);
                            }
                        }
                    }
                }
            }
            return ret;
        }
    }
}