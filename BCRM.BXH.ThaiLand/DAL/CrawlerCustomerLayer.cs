﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.DAL
{
    public class CrawlerCustomerLayer
    {
        public void Create(CrawlerCustomer model)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("crawlercustomer_create", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_adminid", model.AdminId));
                    command.Parameters.Add(new NpgsqlParameter("_receivedby", model.ReceivedBy));
                    command.Parameters.Add(new NpgsqlParameter("_receiveddate", model.ReceiveDate));
                    command.Parameters.Add(new NpgsqlParameter("_contactdeadline", model.ContactDeadline));
                    command.ExecuteNonQuery();
                }
            }
        }
        public List<CrawlerCustomer> GetByAdminId(long adminId)
        {
            var ret = new List<CrawlerCustomer>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("crawlercustomer_getbyadminid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_adminid", adminId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new CrawlerCustomer();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public void UpdatePhoningFailure(int id,int phoneStatus,DateTime rejectDeadline)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("crawlercustomer_updatephonestatusfail", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.Parameters.Add(new NpgsqlParameter("_phonestatus", phoneStatus));
                    command.Parameters.Add(new NpgsqlParameter("_rejectdeadline", rejectDeadline));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdatePhoningSuccess(int id, int phoneStatus)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("crawlercustomer_updatephonestatussuccess", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.Parameters.Add(new NpgsqlParameter("_phonestatus", phoneStatus));
                    command.ExecuteNonQuery();
                }
            }
        }
        
    }
}