﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.DAL
{
    public class CityLayer
    {
        public List<City> GetAll()
        {
            var ret = new List<City>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("city_getall", true))
                {
                    //Truyền tham số cho command
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var city = new City();
                            EntityBase.SetObjectValue(reader, ref city);
                            if (city != null)
                            {
                                ret.Add(city);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public List<City> GetByRegionId(int regionId)
        {
            var ret = new List<City>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("city_getbyregion", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_regionid", regionId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var city = new City();
                            EntityBase.SetObjectValue(reader, ref city);
                            if (city != null)
                            {
                                ret.Add(city);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public City GetById(int id)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("city_getbyid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    //Truyền tham số cho command
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var city = new City();
                            EntityBase.SetObjectValue(reader, ref city);
                            return city;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
    }
}