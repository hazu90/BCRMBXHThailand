﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.DAL
{
    public class CustomerHistoryLayer
    {
        public void Create(CustomerHistory model)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customerhistory_create", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_action", model.Action));
                    command.Parameters.Add(new NpgsqlParameter("_oldvalue", model.OldValue));
                    command.Parameters.Add(new NpgsqlParameter("_newvalue", model.NewValue));
                    command.Parameters.Add(new NpgsqlParameter("_note", model.Note));
                    command.Parameters.Add(new NpgsqlParameter("_updatedby", model.UpdatedBy));
                    command.Parameters.Add(new NpgsqlParameter("_updateddate", model.UpdatedDate));
                    command.Parameters.Add(new NpgsqlParameter("_customerid", model.CustomerId));
                    command.ExecuteNonQuery();
                }
            }
        }
        public List<CustomerHistory> GetByCustomerId(int customerId)
        {
            var result = new List<CustomerHistory>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customerhistory_getbycustomerid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var resultItem = new CustomerHistory();
                            EntityBase.SetObjectValue(reader, ref resultItem);
                            if (resultItem != null)
                            {
                                result.Add(resultItem);
                            }
                        }
                    }
                }
            }
            return result;
        }

    }
}