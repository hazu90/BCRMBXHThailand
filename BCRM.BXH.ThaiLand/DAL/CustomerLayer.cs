﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using BCRM.BXH.ThaiLand.Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.DAL
{
    public class CustomerLayer
    {
        public List<CustomerForIndex> Search(int sort, int status, int type, string assignTo, int cityId, int districtId, DateTime startDate,
                                                DateTime endDate, string keyword, int pageIndex, int pageSize,bool isIgnoreBlacklist,string creator,  out int totalRecord)
        {
            var ret = new List<CustomerForIndex>();
            var total = 0;
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_search_gettotalrecord", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    command.Parameters.Add(new NpgsqlParameter("_status", status));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_isignoredblacklist", isIgnoreBlacklist));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", creator));
                    total = (int)command.ExecuteScalar();
                }
                if (total > 0)
                {
                    using (var command = db.CreateCommand("customer_search", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                        command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                        command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                        command.Parameters.Add(new NpgsqlParameter("_status", status));
                        command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                        command.Parameters.Add(new NpgsqlParameter("_type", type));
                        command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                        command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                        command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                        command.Parameters.Add(new NpgsqlParameter("_pageindex", pageIndex));
                        command.Parameters.Add(new NpgsqlParameter("_pagesize", pageSize));
                        command.Parameters.Add(new NpgsqlParameter("_isignoredblacklist", isIgnoreBlacklist));
                        command.Parameters.Add(new NpgsqlParameter("_createdby", creator));
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var cust = new CustomerForIndex();
                                EntityBase.SetObjectValue(reader, ref cust);
                                if (cust != null)
                                {
                                    ret.Add(cust);
                                }
                            }
                        }
                    }
                }
            }
            totalRecord = total;
            return ret;
        }

        public List<CustomerForIndex> SearchToExport(int sort, int status, int type, string assignTo, int cityId, int districtId, DateTime startDate,
                                                DateTime endDate, string keyword, bool isIgnoreBlacklist,string creator)
        {
            var ret = new List<CustomerForIndex>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_searchtoexport", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    command.Parameters.Add(new NpgsqlParameter("_status", status));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_isignoredblacklist", isIgnoreBlacklist));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", creator));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new CustomerForIndex();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public List<CustomerForIndex> SearchAll(int sort, int status, int type, string assignTo, int cityId, int districtId, DateTime startDate,
                                                DateTime endDate, string keyword,List<int> lstGroup, int pageIndex, int pageSize, bool isIgnoreBlacklist,string creator,  out int totalRecord)
        {
            var ret = new List<CustomerForIndex>();
            var total = 0;
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_searchall_gettotalrecord", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    command.Parameters.Add(new NpgsqlParameter("_status", status));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_lstgroupid", lstGroup));
                    command.Parameters.Add(new NpgsqlParameter("_isignoredblacklist", isIgnoreBlacklist));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", creator));
                    total = (int)command.ExecuteScalar();
                }
                if (total > 0)
                {
                    using (var command = db.CreateCommand("customer_searchall", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                        command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                        command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                        command.Parameters.Add(new NpgsqlParameter("_status", status));
                        command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                        command.Parameters.Add(new NpgsqlParameter("_type", type));
                        command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                        command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                        command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                        command.Parameters.Add(new NpgsqlParameter("_pageindex", pageIndex));
                        command.Parameters.Add(new NpgsqlParameter("_pagesize", pageSize));
                        command.Parameters.Add(new NpgsqlParameter("_lstgroupid", lstGroup));
                        command.Parameters.Add(new NpgsqlParameter("_isignoredblacklist", isIgnoreBlacklist));
                        command.Parameters.Add(new NpgsqlParameter("_createdby", creator));
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var cust = new CustomerForIndex();
                                EntityBase.SetObjectValue(reader, ref cust);
                                if (cust != null)
                                {
                                    ret.Add(cust);
                                }
                            }
                        }
                    }
                }
            }
            totalRecord = total;
            return ret;
        }
        public List<CustomerForIndex> SearchAllToExport(int sort, int status, int type, string assignTo, int cityId, int districtId, DateTime startDate,
                                                DateTime endDate, string keyword, List<int> lstGroup, bool isIgnoreBlacklist,string creator)
        {
            var ret = new List<CustomerForIndex>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_searchalltoexport", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    command.Parameters.Add(new NpgsqlParameter("_status", status));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_lstgroupid", lstGroup));
                    command.Parameters.Add(new NpgsqlParameter("_isignoredblacklist", isIgnoreBlacklist));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", creator));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new CustomerForIndex();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }

        public List<CustomerForIndex> SearchUnAssign(int sort, int status, int type, int cityId, int districtId, DateTime startDate,
                                                DateTime endDate, string keyword, int pageIndex, int pageSize, bool isIgnoreBlacklist,string creator, out int totalRecord)
        {
            var ret = new List<CustomerForIndex>();
            var total = 0;
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_searchunassign_gettotalrecord", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    command.Parameters.Add(new NpgsqlParameter("_status", status));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_isignoredblacklist", isIgnoreBlacklist));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", creator));
                    total = (int)command.ExecuteScalar();
                }
                if (total > 0)
                {
                    using (var command = db.CreateCommand("customer_searchunassign", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                        command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                        command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                        command.Parameters.Add(new NpgsqlParameter("_status", status));
                        command.Parameters.Add(new NpgsqlParameter("_type", type));
                        command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                        command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                        command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                        command.Parameters.Add(new NpgsqlParameter("_pageindex", pageIndex));
                        command.Parameters.Add(new NpgsqlParameter("_pagesize", pageSize));
                        command.Parameters.Add(new NpgsqlParameter("_isignoredblacklist", isIgnoreBlacklist));
                        command.Parameters.Add(new NpgsqlParameter("_createdby", creator));
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var cust = new CustomerForIndex();
                                EntityBase.SetObjectValue(reader, ref cust);
                                if (cust != null)
                                {
                                    ret.Add(cust);
                                }
                            }
                        }
                    }
                }
            }
            totalRecord = total;
            return ret;
        }
        public List<CustomerForIndex> SearchUnAssignToExport(int sort, int status, int type, int cityId, int districtId, DateTime startDate,
                                                DateTime endDate, string keyword, bool isIgnoreBlacklist,string creator)
        {
            var ret = new List<CustomerForIndex>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_searchunassigntoexport", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    command.Parameters.Add(new NpgsqlParameter("_status", status));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_isignoredblacklist", isIgnoreBlacklist));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", creator));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new CustomerForIndex();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        //public List<CustomerForIndex> SelectByPhoneNumber(long[] phoneNumber)
        public List<CustomerForIndex> SelectByPhoneNumber(string[] phoneNumber)
        {
            var ret = new List<CustomerForIndex>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_selectphonenumber", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new CustomerForIndex();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public CustomerForIndex SearchByCustomerId(int customerId)
        {
            var ret = new List<CustomerForIndex>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_searchbycustomerid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    using (var reader = command.ExecuteReader())
                    {
                        if(reader.Read())
                        {
                            var cust = new CustomerForIndex();
                            EntityBase.SetObjectValue(reader, ref cust);
                            return cust;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
        public int Create(Customer model)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_create", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_fullname", model.FullName));
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", model.PhoneNumber));
                    //command.Parameters.Add(new NpgsqlParameter("_status", model.Status));
                    command.Parameters.Add(new NpgsqlParameter("_type", model.Type));
                    command.Parameters.Add(new NpgsqlParameter("_email", model.Email));
                    command.Parameters.Add(new NpgsqlParameter("_description", model.Description));
                    command.Parameters.Add(new NpgsqlParameter("_regionid", model.RegionId));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", model.CityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", model.DistrictId));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", model.AssignTo));
                    command.Parameters.Add(new NpgsqlParameter("_caredate", model.CareDate));
                    command.Parameters.Add(new NpgsqlParameter("_icaredate", model.ICareDate));
                    command.Parameters.Add(new NpgsqlParameter("_startcaredate", model.StartCareDate));
                    command.Parameters.Add(new NpgsqlParameter("_companyid", model.CompanyId));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", model.CreatedBy));
                    command.Parameters.Add(new NpgsqlParameter("_createddate", model.CreatedDate));
                    command.Parameters.Add(new NpgsqlParameter("_lastmodifiedby", model.LastModifiedBy));
                    command.Parameters.Add(new NpgsqlParameter("_lastmodifieddate", model.LastModifiedDate));
                    command.Parameters.Add(new NpgsqlParameter("_groupid", model.GroupId));
                    command.Parameters.Add(new NpgsqlParameter("_address", model.Address));
                    return (int)command.ExecuteScalar();
                }
            }
        }
        public Customer GetById(int customerId)
        {
            var ret = new Customer();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_getbyid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", customerId));
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            EntityBase.SetObjectValue(reader, ref ret);
                            return ret;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
        public void Update(Customer model)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_update", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_fullname", model.FullName));
                    command.Parameters.Add(new NpgsqlParameter("_status", model.Status));
                    command.Parameters.Add(new NpgsqlParameter("_type", model.Type));
                    command.Parameters.Add(new NpgsqlParameter("_email", model.Email));
                    command.Parameters.Add(new NpgsqlParameter("_description", model.Description));
                    command.Parameters.Add(new NpgsqlParameter("_regionid", model.RegionId));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", model.CityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", model.DistrictId));
                    //command.Parameters.Add(new NpgsqlParameter("_assignto", model.AssignTo));
                    //command.Parameters.Add(new NpgsqlParameter("_caredate", model.CareDate));
                    //command.Parameters.Add(new NpgsqlParameter("_icaredate", model.ICareDate));
                    //command.Parameters.Add(new NpgsqlParameter("_startcaredate", model.StartCareDate));
                    command.Parameters.Add(new NpgsqlParameter("_lastmodifiedby", model.LastModifiedBy));
                    command.Parameters.Add(new NpgsqlParameter("_lastmodifieddate", model.LastModifiedDate));
                    command.Parameters.Add(new NpgsqlParameter("_id", model.Id));
                    command.Parameters.Add(new NpgsqlParameter("_address", model.Address));
                    command.Parameters.Add(new NpgsqlParameter("_companyid", model.CompanyId));
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", model.PhoneNumber));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void Block(int customerId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updateisdeleted", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_isdeleted", true));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UnBlock(int customerId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updateisdeleted", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_isdeleted", false));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdatePhoneNumber(int customerId, string phoneNumber)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updatephonenumber", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    command.ExecuteNonQuery();
                }
            }
        }
        public bool IsExistedPhoneNumber(int id, string phoneNumber)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("customer_isexistedphonenumber", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_id", id));
                    cmd.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    var iIsExisted = (int)cmd.ExecuteScalar();
                    return (iIsExisted > 0);
                }
            }
        }
        public bool IsExistedEmail(int id,string email)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("customer_isexistedemail", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_id", id));
                    cmd.Parameters.Add(new NpgsqlParameter("_email", email));
                    var iIsExisted = (int)cmd.ExecuteScalar();
                    return (iIsExisted > 0);
                }
            }
        }
        public void UpdateAssignTo(int id, int groupId,string assignTo)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updateassigntoandgroupid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.Parameters.Add(new NpgsqlParameter("_groupid", groupId));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdateBlacklist(int id,bool isBlacklist)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updateblacklist", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.Parameters.Add(new NpgsqlParameter("_isblacklist", isBlacklist));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdateNote(int id,string note)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updatenote", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.Parameters.Add(new NpgsqlParameter("_note", note));
                    command.ExecuteNonQuery();
                }
            }
        }
        public List<DetailAdminAccountStatisticSearchModel> GetReportDetail(List<int> lstGroupId,string createdBy,  DateTime startDate,DateTime endDate)
        {
            var ret = new List<DetailAdminAccountStatisticSearchModel>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_getreportdetail", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_lstgroupid", lstGroupId));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new DetailAdminAccountStatisticSearchModel();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }
    }
}