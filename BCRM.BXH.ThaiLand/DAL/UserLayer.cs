﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using BCRM.BXH.ThaiLand.Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.DAL
{
    public class UserLayer
    {
        public Users GetByUserName(string username)
        {
            var ret = new Users();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("users_getbyusername", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_username", username));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            EntityBase.SetObjectValue(reader, ref ret);
                        }
                    }
                }
            }
            return ret;
        }
        public void UpdateLastActivity(int id)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("users_updatelastactivity", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_lastactivitydate", DateTime.Now));
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void ResetPassword(ResetPasswordModel model)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("users_resetpassword", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", model.Id));
                    command.Parameters.Add(new NpgsqlParameter("_password", model.NewPassword));
                    command.ExecuteNonQuery();
                }
            }
        }
        public List<Users> GetAll()
        {
            var ret = new List<Users>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("users_getall", true))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var user = new Users();
                            EntityBase.SetObjectValue(reader, ref user);
                            if (user != null)
                            {
                                ret.Add(user);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public int Create(Users model)
        {
            try
            {
                using (var context = new PostgresSQL())
                {
                    using (var cmd = context.CreateCommand("users_create", true))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("_username", model.UserName));
                        cmd.Parameters.Add(new NpgsqlParameter("_password", model.Password));
                        cmd.Parameters.Add(new NpgsqlParameter("_email", model.Email));
                        cmd.Parameters.Add(new NpgsqlParameter("_displayname", model.DisplayName));
                        cmd.Parameters.Add(new NpgsqlParameter("_status", model.Status));
                        if (model.DOB == DateTime.MinValue)
                        {
                            cmd.Parameters.Add(new NpgsqlParameter("_dob", DBNull.Value));
                        }
                        else
                        {
                            cmd.Parameters.Add(new NpgsqlParameter("_dob", model.DOB));
                        }

                        cmd.Parameters.Add(new NpgsqlParameter("_groupid", model.GroupId));
                        cmd.Parameters.Add(new NpgsqlParameter("_adminusername", model.AdminUserName));
                        cmd.Parameters.Add(new NpgsqlParameter("_createdby", model.CreatedBy));
                        cmd.Parameters.Add(new NpgsqlParameter("_createddate", model.CreatedDate));
                        var id = (int)cmd.ExecuteScalar();
                        return id;
                    }
                }
            }
            catch
            {
                return 0;
            }

        }
        public void Update(Users model)
        {
            try
            {
                using (var context = new PostgresSQL())
                {
                    using (var cmd = context.CreateCommand("users_update", true))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("_email", model.Email));
                        cmd.Parameters.Add(new NpgsqlParameter("_displayname", model.DisplayName));
                        if (model.DOB == DateTime.MinValue)
                        {
                            cmd.Parameters.Add(new NpgsqlParameter("_dob", DBNull.Value));
                        }
                        else
                        {
                            cmd.Parameters.Add(new NpgsqlParameter("_dob", model.DOB));
                        }
                        cmd.Parameters.Add(new NpgsqlParameter("_groupid", model.GroupId));
                        cmd.Parameters.Add(new NpgsqlParameter("_adminusername", model.AdminUserName));
                        cmd.Parameters.Add(new NpgsqlParameter("_updatedby", model.UpdatedBy));
                        cmd.Parameters.Add(new NpgsqlParameter("_updateddate", model.UpdatedDate));
                        cmd.Parameters.Add(new NpgsqlParameter("_id", model.Id));
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch
            {

            }

        }
        public void UpdateStatus(int userId, int status)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("users_updatestatus", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_id", userId));
                    cmd.Parameters.Add(new NpgsqlParameter("_status", status));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public List<UserSearchModel> GetList(string groupdIds)
        {
            var ret = new List<UserSearchModel>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("users_getlist", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_groupids", groupdIds));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var user = new UserSearchModel();
                            EntityBase.SetObjectValue(reader, ref user);
                            if (user != null)
                            {
                                ret.Add(user);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public Users GetById(int id)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("users_getbyid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    using (var reader = command.ExecuteReader())
                    {

                        reader.Read();
                        var user = new Users();
                        EntityBase.SetObjectValue(reader, ref user);
                        return user;
                    }
                }
            }
        }
        public void UpdateProfile(Users model)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("users_updateprofile", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_email", model.Email));
                    cmd.Parameters.Add(new NpgsqlParameter("_displayname", model.DisplayName));
                    cmd.Parameters.Add(new NpgsqlParameter("_dob", model.DOB));
                    cmd.Parameters.Add(new NpgsqlParameter("_updatedby", model.CreatedBy));
                    cmd.Parameters.Add(new NpgsqlParameter("_updateddate", model.CreatedDate));
                    cmd.Parameters.Add(new NpgsqlParameter("_mobile", model.Mobile));
                    cmd.Parameters.Add(new NpgsqlParameter("_id", model.Id));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public void UpdateAvatar(Users model)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("users_updateprofile", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_updatedby", model.CreatedBy));
                    cmd.Parameters.Add(new NpgsqlParameter("_updateddate", model.CreatedDate));
                    cmd.Parameters.Add(new NpgsqlParameter("_avatar", model.Avatar));
                    cmd.Parameters.Add(new NpgsqlParameter("_id", model.Id));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public List<Users> GetByGroupId(int groupId)
        {
            var ret = new List<Users>();
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("users_getbygroupid", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_groupid", groupId));
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var user = new Users();
                            EntityBase.SetObjectValue(reader, ref user);
                            if (user != null)
                            {
                                ret.Add(user);
                            }
                        }
                    }
                    return ret;
                }
            }
        }
    }
}