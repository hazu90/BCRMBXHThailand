﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using BCRM.BXH.ThaiLand.Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.DAL
{
    public class UserRoleGroupLayer
    {
        public List<UserRoleGroup> GetRoleGroupByUserId(int userId)
        {
            var lstUserRoleGroup = new List<UserRoleGroup>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("userrolegroup_getbyuserid", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_userid", userId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var ret = new UserRoleGroup();
                            EntityBase.SetObjectValue(reader, ref ret);
                            if (ret != null)
                            {
                                lstUserRoleGroup.Add(ret);
                            }
                        }
                    }
                }
            }
            return lstUserRoleGroup;
        }

        public void Create(int userId, int groupId, int roleId)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("userrolegroup_create", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_userid", userId));
                    cmd.Parameters.Add(new NpgsqlParameter("_roleid", roleId));
                    cmd.Parameters.Add(new NpgsqlParameter("_groupid", groupId));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public List<UserRoleGroupSearchModel> GetList(int userId)
        {
            var lstUserRoleGroup = new List<UserRoleGroupSearchModel>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("userrolegroup_getlist", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_userid", userId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var ret = new UserRoleGroupSearchModel();
                            EntityBase.SetObjectValue(reader, ref ret);
                            if (ret != null)
                            {
                                lstUserRoleGroup.Add(ret);
                            }
                        }
                    }
                }
            }
            return lstUserRoleGroup;
        }
        public void Delete(int userId, int groupId, int roleId)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("userrolegroup_delete", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_userid", userId));
                    cmd.Parameters.Add(new NpgsqlParameter("_roleid", roleId));
                    cmd.Parameters.Add(new NpgsqlParameter("_groupid", groupId));
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}