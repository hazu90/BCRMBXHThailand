﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.DAL
{
    public class DistrictLayer
    {
        public List<District> GetByCityId(int cityId)
        {
            var ret = new List<District>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("district_getbycityid", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var district = new District();
                            EntityBase.SetObjectValue(reader, ref district);
                            if (district != null)
                            {
                                ret.Add(district);
                            }
                        }
                    }
                }
            }
            return ret;
        }

        public District GetById(int id)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("district_getbyid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    //Truyền tham số cho command
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var district = new District();
                            EntityBase.SetObjectValue(reader, ref district);
                            return district;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }

    }
}