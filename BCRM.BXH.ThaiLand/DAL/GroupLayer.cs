﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.DAL
{
    public class GroupLayer
    {
        public List<Group> GetAll()
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("group_getlist", true))
                {
                    //Nếu trả về 1 table thì phải dùng reader để đọc
                    using (var reader = cmd.ExecuteReader())
                    {
                        var lstGroup = new List<Group>();
                        if (reader.HasRows)
                        {

                            //Nếu có 1 giá trị thì gọi reader.Read() để đọc luôn, còn ko thì dùng while
                            while (reader.Read())
                            {
                                var item = new Group();
                                //cách lấy 2, dùng class EntityBase
                                EntityBase.SetObjectValue(reader, ref item);
                                lstGroup.Add(item);
                            }

                        }
                        return lstGroup;
                    }
                }
            }
        }

        public int Create(Group model)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("group_create", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_name", model.Name));
                    cmd.Parameters.Add(new NpgsqlParameter("_alias", model.Alias));
                    cmd.Parameters.Add(new NpgsqlParameter("_description", model.Description));
                    cmd.Parameters.Add(new NpgsqlParameter("_parentid", model.ParentId));
                    cmd.Parameters.Add(new NpgsqlParameter("_leader", model.Leader));
                    cmd.Parameters.Add(new NpgsqlParameter("_priority", model.Priority));
                    var id = (int)cmd.ExecuteScalar();
                    return id;
                }
            }
        }

        public void Edit(Group model)
        {
            try
            {
                using (var context = new PostgresSQL())
                {
                    using (var cmd = context.CreateCommand("group_edit", true))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("_name", model.Name));
                        cmd.Parameters.Add(new NpgsqlParameter("_alias", model.Alias));
                        cmd.Parameters.Add(new NpgsqlParameter("_description", model.Description));
                        cmd.Parameters.Add(new NpgsqlParameter("_parentid", model.ParentId));
                        cmd.Parameters.Add(new NpgsqlParameter("_leader", model.Leader));
                        cmd.Parameters.Add(new NpgsqlParameter("_priority", model.Priority));
                        cmd.Parameters.Add(new NpgsqlParameter("_id", model.Id));
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch
            {

            }

        }

        public Group GetById(int id)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("group_getbyid", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_id", id));
                    var item = new Group();
                    //Nếu trả về 1 table thì phải dùng reader để đọc
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            //Nếu có 1 giá trị thì gọi reader.Read() để đọc luôn, còn ko thì dùng while
                            reader.Read();
                            //cách lấy 2, dùng class EntityBase
                            EntityBase.SetObjectValue(reader, ref item);
                        }

                        return item;
                    }

                }
            }
        }
    }
}