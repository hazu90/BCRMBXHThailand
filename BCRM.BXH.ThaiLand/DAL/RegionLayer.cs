﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.DAL
{
    public class RegionLayer
    {
        public List<Region> GetAll()
        {
            var ret = new List<Region>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("region_getall", true))
                {
                    //Truyền tham số cho command
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var region = new Region();
                            EntityBase.SetObjectValue(reader, ref region);
                            if (region != null)
                            {
                                ret.Add(region);
                            }
                        }
                    }
                }
            }
            return ret;
        }

        public Region GetById(int id)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("region_getbyid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    //Truyền tham số cho command
                    using (var reader = command.ExecuteReader())
                    {
                        if(reader.Read())
                        {
                            var region = new Region();
                            EntityBase.SetObjectValue(reader, ref region);
                            return region;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
    }
}