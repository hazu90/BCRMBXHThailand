﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.DAL
{
    public class SyncBcrmBxhNAdminLayer
    {
        public void Create(int customerId, long accountId,string email,  string phonenumber)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("syncbcrmbxhnadmin_create", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_accountid", accountId));
                    command.Parameters.Add(new NpgsqlParameter("_email", email));
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", phonenumber));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void Update(int customerId,string email,string phoneNumber)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("syncbcrmbxhnadmin_update", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_email", email));
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    command.ExecuteNonQuery();
                }
            }
        }
        public SyncBcrmBxhNAdmin GetByCustomerId(int customerId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("syncbcrmbxhnadmin_getbycustomerid", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    //Truyền tham số cho command
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var syncInfo = new SyncBcrmBxhNAdmin();
                            EntityBase.SetObjectValue(reader, ref syncInfo);
                            return syncInfo;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
        public List<SyncBcrmBxhNAdmin>  GetByListCustomerId(string customerIds)
        {
            var ret = new List<SyncBcrmBxhNAdmin>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("syncbcrmbxhnadmin_getbylistcustomerids", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerids", customerIds));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new SyncBcrmBxhNAdmin();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }
    }
}