﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using BCRM.BXH.ThaiLand.Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.DAL
{
    public class CompanyLayer
    {
        public void Create(CompanySaveModel model,string createdBy)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("company_create", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_name", model.Name));
                    cmd.Parameters.Add(new NpgsqlParameter("_address", model.Address));
                    cmd.Parameters.Add(new NpgsqlParameter("_phonenumber", model.PhoneNumber));
                    cmd.Parameters.Add(new NpgsqlParameter("_cityid", model.CityId));
                    cmd.Parameters.Add(new NpgsqlParameter("_districtid", model.DistrictId));
                    cmd.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                    cmd.Parameters.Add(new NpgsqlParameter("_createddate", DateTime.Now));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public void Update(CompanySaveModel model, string updatedBy)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("company_update", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_id", model.Id));
                    cmd.Parameters.Add(new NpgsqlParameter("_name", model.Name));
                    cmd.Parameters.Add(new NpgsqlParameter("_address", model.Address));
                    cmd.Parameters.Add(new NpgsqlParameter("_phonenumber", model.PhoneNumber));
                    cmd.Parameters.Add(new NpgsqlParameter("_cityid", model.CityId));
                    cmd.Parameters.Add(new NpgsqlParameter("_districtid", model.DistrictId));
                    cmd.Parameters.Add(new NpgsqlParameter("_updatedby", updatedBy));
                    cmd.Parameters.Add(new NpgsqlParameter("_updateddate", DateTime.Now));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public void Delete(int id)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("company_delete", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public List<CompanySearchModel> Search(CompanyIndexModel model ,out int totalRecord)
        {
            var ret = new List<CompanySearchModel>();
            var total = 0;
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("company_search_gettotalrecord", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", model.FilterKeyword  ));
                    command.Parameters.Add(new NpgsqlParameter("_cityid",model.CityId ));
                    command.Parameters.Add(new NpgsqlParameter("_districtid",model.DistrictId ));

                    total = (int)command.ExecuteScalar();
                }
                if (total > 0)
                {
                    using (var command = db.CreateCommand("company_search", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_filterkeyword", model.FilterKeyword));
                        command.Parameters.Add(new NpgsqlParameter("_cityid", model.CityId));
                        command.Parameters.Add(new NpgsqlParameter("_districtid", model.DistrictId));
                        command.Parameters.Add(new NpgsqlParameter("_pageindex", model.PageIndex));
                        command.Parameters.Add(new NpgsqlParameter("_pagesize", model.PageSize));
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var cust = new CompanySearchModel();
                                EntityBase.SetObjectValue(reader, ref cust);
                                if (cust != null)
                                {
                                    ret.Add(cust);
                                }
                            }
                        }
                    }
                }
            }
            totalRecord = total;
            return ret;
        }
        public CompanySearchModel SearchByPhoneNumber( string phoneNumber)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("company_getbyphonenumber", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    var reader = command.ExecuteReader();
                    if(reader.Read())
                    {
                        var cust = new CompanySearchModel();
                        EntityBase.SetObjectValue(reader, ref cust);
                        return cust;
                    }
                    else
                    {
                        return null;
                    }
                    
                }
            }
        }
        public CompanySaveModel GetById(int id)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("company_getbyid", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_id", id));
                    var item = new CompanySaveModel();
                    //Nếu trả về 1 table thì phải dùng reader để đọc
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            //Nếu có 1 giá trị thì gọi reader.Read() để đọc luôn, còn ko thì dùng while
                            reader.Read();
                            //cách lấy 2, dùng class EntityBase
                            EntityBase.SetObjectValue(reader, ref item);
                            return item;
                        }
                        else
                        {
                            return null;
                        }

                        
                    }

                }
            }
        }
        public List<Company> GetAll()
        {
            var lstCompanies = new List<Company>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("company_getall", true))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var companyInfo = new Company();
                            EntityBase.SetObjectValue(reader, ref companyInfo);
                            if (companyInfo != null)
                            {
                                lstCompanies.Add(companyInfo);
                            }
                        }
                    }
                }
            }
            return lstCompanies;
        }
        public bool IsExistedPhonenumber(int id, string phoneNumber)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("company_isexistedphonenumber", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_id", id));
                    cmd.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    var iIsExisted = (int)cmd.ExecuteScalar();
                    return (iIsExisted > 0);
                }
            }
        }
        public bool IsExistedNameAndAddress(int id, string name, string address)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("company_isexistednameandaddress", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_name", name));
                    cmd.Parameters.Add(new NpgsqlParameter("_address", address));
                    cmd.Parameters.Add(new NpgsqlParameter("_id", id));
                    var iIsExisted = (int)cmd.ExecuteScalar();
                    return (iIsExisted > 0);
                }
            }
        }

    }
}