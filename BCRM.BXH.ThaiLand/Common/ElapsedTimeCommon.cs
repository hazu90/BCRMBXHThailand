﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace BCRM.BXH.ThaiLand.Common
{
    public class ElapsedTimeCommon
    {
        public static Dictionary<string, ElapsedTimeEntity> IdentifiedElapseds = new Dictionary<string, ElapsedTimeEntity>();
        public static void TrackingTime(RouteData routeData, long time)
        {
            var controllerName = routeData.Values["controller"].ToString().ToLower();
            var actionName = routeData.Values["action"].ToString().ToLower();
            ElapsedTimeCommon.Insert(time, controllerName + "." + actionName);
        }
        public static void Insert(long milliseconds, string identifier)
        {
            lock (IdentifiedElapseds)
            {
                ElapsedTimeEntity entry;
                if (!IdentifiedElapseds.TryGetValue(identifier, out entry))
                {
                    entry = new ElapsedTimeEntity();
                    IdentifiedElapseds.Add(identifier, entry);
                }
                entry.LastTime = milliseconds;
                entry.LastDate = DateTime.UtcNow;
                entry.TotalTime += milliseconds;
                entry.Count++;
                if (milliseconds < entry.MinTime)
                {
                    entry.MinTime = milliseconds;
                    entry.MinDate = DateTime.UtcNow;
                }
                if (milliseconds > entry.MaxTime)
                {
                    entry.MaxTime = milliseconds;
                    entry.MaxDate = DateTime.UtcNow;
                }
            }
        }
        public static void Reset()
        {
            IdentifiedElapseds = new Dictionary<string, ElapsedTimeEntity>();
        }
    }

    public class ElapsedTimeEntity
    {
        public long LastTime = 0;
        public long TotalTime = 0;
        public int Count = 0;
        public long MinTime = long.MaxValue;
        public long MaxTime = long.MinValue;
        public double Average
        {
            get { return (TotalTime / Count); }
        }
        public DateTime MinDate;
        public DateTime MaxDate;
        public DateTime LastDate;
    }
}