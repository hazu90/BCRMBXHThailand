﻿using BCRM.BXH.ThaiLand.DAL;
using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using BCRM.BXH.ThaiLand.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Cache
{
    public class IISUser
    {
        private IISCache cache = new IISCache();

        private const string CacheNameUserInfo = "DVS.BCRM.BXH.Thailand.{0}.Info";

        public UserAuthenticateModel SetUserInfo(Users user)
        {
            var key = string.Format(CacheNameUserInfo, user.UserName).ToLower();
            var userInfo = new UserAuthenticateModel
            {
                Id = user.Id,
                UserName = user.UserName,
                Password = user.Password,
                Avatar = user.Avatar,
                DisplayName = user.DisplayName,
                Email = user.Email,
                GroupId = user.GroupId,
                AdminUserId = user.AdminUserId,
                AdminUserName = user.AdminUserName,
                Status = user.Status
            };
            userInfo.ListRoleGroup = new Dictionary<int, List<int>>();
            var userRoleGroupLayer = new UserRoleGroupLayer();
            var lstRoleGroup = userRoleGroupLayer.GetRoleGroupByUserId(user.Id);
            var lstRole = lstRoleGroup.Select(r => r.RoleId).ToList().Distinct();
            foreach (var role in lstRole)
            {
                var lstGroupInRole = lstRoleGroup.FindAll(g => g.RoleId == role).Select(g => g.GroupId).ToList<int>();
                if (!userInfo.ListRoleGroup.ContainsKey(role))
                {
                    userInfo.ListRoleGroup.Add(role, lstGroupInRole);
                }
            }
            cache.Set(key, userInfo, DateTime.Now.Date.AddDays(1).AddMinutes(60));
            return userInfo;
        }

        public UserAuthenticateModel GetUserInfo(string username)
        {
            var cache = new IISCache();
            var key = string.Format(CacheNameUserInfo, username).ToLower();
            var userInfo = cache.Get<UserAuthenticateModel>(key);
            if (userInfo == null)
            {
                var userLayer = new UserLayer();
                var user = userLayer.GetByUserName(username);
                userInfo = this.SetUserInfo(user);
            }
            return userInfo;
        }

        public void RemoveUserInfo(string username)
        {
            cache.Remove(string.Format(CacheNameUserInfo, username).ToLower());
        }
    }
}