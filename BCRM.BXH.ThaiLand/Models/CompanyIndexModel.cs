﻿using BCRM.BXH.ThaiLand.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.ThaiLand.Models
{
    public class CompanyIndexModel
    {
        private string _filterKeyword;
        public string FilterKeyword {
            get
            {
                return _filterKeyword;
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _filterKeyword = "";
                }
                else
                {
                    _filterKeyword = value.Trim();
                }
            }
        }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool IsAdmin { get; set; }
        public List<SelectListItem> LstCity { get; set; }
        public List<SelectListItem> LstDistrict { get; set; }
        public void SetCityList(List<City> lstCity)
        {
            LstCity = lstCity.Select(o => new SelectListItem() { Value = o.Id.ToString(), Text = o.Name }).ToList();
        }
    }
    public class CompanySaveModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
    }
    public class CompanySearchModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string DistrictName { get; set; }
        public string CityName { get; set; }
    }

}