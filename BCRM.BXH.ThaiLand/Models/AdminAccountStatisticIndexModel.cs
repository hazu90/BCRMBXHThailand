﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class AdminAccountStatisticIndexModel
    {
        public int GroupId { get; set; }
        public string AssignTo { get; set; }
        private DateTime _timeToCreationFrom;
        public  DateTime? TimeToCreationFrom
        {
            get
            {
                return _timeToCreationFrom;
            }
            set
            {
                if(value == null ||  value == DateTime.MinValue)
                {
                    _timeToCreationFrom = new DateTime(2000, 1, 1, 0, 0, 0);
                }
                else
                {
                    _timeToCreationFrom = value.Value;
                }
            }
        }
        private DateTime _timeToCreationTo;
        public DateTime? TimeToCreationTo
        {
            get
            {
                return _timeToCreationTo;
            }
            set
            {
                if (value == null || value == DateTime.MinValue)
                {
                    _timeToCreationTo = new DateTime(9999, 1, 1, 0, 0, 0);
                }
                else
                {
                    _timeToCreationTo = new DateTime(value.Value.Year,value.Value.Month,value.Value.Day,23,59,59 ) ;
                }
            }
        }
        public List<KeyValuePair<string, string>> LstUser { get; set; }
        public List<KeyValuePair<int,string>> LstGroup { get; set; }
        public void  SetListGroup(bool isSearchAll, List<Group> lstGroup )
        {
            this.LstGroup = new List<KeyValuePair<int, string>>();
            if(isSearchAll)
            {
                this.LstGroup.Add(new KeyValuePair<int, string>(0, LanguageRes.PlaceHolder_AllGroups ) );
                this.LstGroup.AddRange(lstGroup.Select(o => new KeyValuePair<int, string>(o.Id, o.Name)).ToList());
            }
            else
            {
                this.LstGroup.AddRange(lstGroup.Select(o => new KeyValuePair<int, string>(o.Id, o.Name)).ToList());
            }
        }
        public void SetListUsers(bool isSearchAll,List<UserSearchModel> lstUsers )
        {
            this.LstUser = new List<KeyValuePair<string, string>>();
            if (isSearchAll)
            {
                this.LstUser.Add(new KeyValuePair<string, string>("", LanguageRes.PlaceHolder_AllUsers));
                this.LstUser.AddRange(lstUsers.Select(o => new KeyValuePair<string, string>(o.UserName, o.UserName)).ToList());
            }
            else
            {
                this.LstUser.AddRange(lstUsers.Select(o => new KeyValuePair<string ,string>(o.UserName, o.UserName)).ToList());
            }
        }

    }

    public class AdminAccountStatisticSearchModel
    {
        /// <summary>
        /// Id của sale
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Id của sale
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Tên sale
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Số điện thoại
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Tên nhóm
        /// </summary>
        public string GroupName { get; set; }
        /// <summary>
        /// Số khách hàng đã nhận của sale
        /// </summary>
        public long SumReceivedCustomer { get; set; }
        /// <summary>
        /// Số khách hàng
        /// </summary>
        public long SumSuccessContact { get; set; }
        /// <summary>
        /// Tỉ lệ liên hệ thành công
        /// </summary>
        public string DisplayRateSuccess
        {
            get
            {
                return (SumSuccessContact.ToString() + "/" + SumReceivedCustomer.ToString());
            }
        }
        /// <summary>
        /// Số tài khoản active
        /// </summary>
        public long SumActivedSiteCustomer { get; set; }
    }
}