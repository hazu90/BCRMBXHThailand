﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class DetailAdminAccountStatisticIndexModel
    {
        public List<KeyValuePair<string, string>> LstUser { get; set; }
        public List<KeyValuePair<int, string>> LstGroup { get; set; }
        public void SetListGroup(bool isSearchAll, List<Group> lstGroup)
        {
            this.LstGroup = new List<KeyValuePair<int, string>>();
            if (isSearchAll)
            {
                this.LstGroup.Add(new KeyValuePair<int, string>(0,LanguageRes.PlaceHolder_AllGroups ));
                this.LstGroup.AddRange(lstGroup.Select(o => new KeyValuePair<int, string>(o.Id, o.Name)).ToList());
            }
            else
            {
                this.LstGroup.AddRange(lstGroup.Select(o => new KeyValuePair<int, string>(o.Id, o.Name)).ToList());
            }
        }
        public void SetListUsers(bool isSearchAll, List<UserSearchModel> lstUsers)
        {
            this.LstUser = new List<KeyValuePair<string, string>>();
            if (isSearchAll)
            {
                this.LstUser.Add(new KeyValuePair<string, string>("", LanguageRes.PlaceHolder_AllUsers));
                this.LstUser.AddRange(lstUsers.Select(o => new KeyValuePair<string, string>(o.UserName, o.UserName)).ToList());
            }
            else
            {
                this.LstUser.AddRange(lstUsers.Select(o => new KeyValuePair<string, string>(o.UserName, o.UserName)).ToList());
            }
        }
        public int ActiveStatus { get; set; }
        private DateTime _timeToCreationFrom;
        public DateTime TimeToCreationFrom
        {
            get
            {
                if (_timeToCreationFrom == DateTime.MinValue)
                {
                    return new DateTime(2000, 1, 1, 0, 0, 0);
                }
                else
                {
                    return _timeToCreationFrom;
                }
            }
            set
            {
                if(value == null || value == DateTime.MinValue )
                {
                    _timeToCreationFrom= new DateTime(2000, 1, 1, 0, 0, 0);
                }
                else
                {
                    _timeToCreationFrom = value;
                }
            }
        }
        private DateTime _timeToCreationTo;
        public DateTime TimeToCreationTo
        {
            get
            {
                if ( _timeToCreationTo == DateTime.MinValue)
                {
                    return new DateTime(9999, 1, 1, 0, 0, 0);
                }
                else
                {
                    return _timeToCreationTo;
                }
                
            }
            set
            {
                if (value == null || value == DateTime.MinValue)
                {
                    _timeToCreationTo = new DateTime(9999, 1, 1, 0, 0, 0);
                }
                else
                {
                    _timeToCreationTo = value.AddHours(23).AddMinutes(59).AddSeconds(59);
                }
            }
        }
        public int GroupId { get; set; }
        public string CreatedBy { get; set; }
    }
    public class DetailAdminAccountStatisticSearchModel
    {
        public int CustomerId { get; set; }
        public int AdminId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public int NumberOfPostingNews { get; set; }
        public int ActiveStatus { get; set; }
    }
}