﻿    using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    [Serializable]
    public class UserAuthenticateModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public int GroupId { get; set; }
        public int DepartmentId { get; set; }
        public string Avatar { get; set; }
        public int AdminUserId { get; set; }
        public string AdminUserName { get; set; }
        public int Status { get; set; }
        public Dictionary<int, List<int>> ListRoleGroup { get; set; }
        public UserAuthenticateModel()
        {
            this.ListRoleGroup = new Dictionary<int, List<int>>();
        }
        public bool HasRole(int role)
        {
            return this.ListRoleGroup.Keys.Contains(role);
        }
        public bool HasRole(params int[] arrRole)
        {
            var ret = false;
            foreach (var role in arrRole)
            {
                ret = this.ListRoleGroup.Keys.Contains(role);
                if (ret) break;
            }
            return ret;
        }
        public bool HasRoleGroup(int groupId, params int[] arrRole)
        {
            var ret = false;
            foreach (var role in arrRole)
            {
                if (this.ListRoleGroup.Keys.Contains(role))
                {
                    var lstGroup = this.ListRoleGroup[role];
                    ret = lstGroup.Contains(groupId) || lstGroup.Contains(DefaultSystemParameter.AllGroup);
                }
                if (ret) break;
            }
            return ret;
        }
        /// <summary>
        /// Lấy danh sách các group mà nhân viên được phân quyền
        /// </summary>
        /// <param name="arrRole"></param>
        /// <returns></returns>
        public List<int> GetGroupsByRole(params int[] arrRole)
        {
            var lstGroup = new List<int>();

            if(this.ListRoleGroup.ContainsKey(RoleSystem.Admin.GetHashCode()) )
            {
                lstGroup.Add(DefaultSystemParameter.AllGroup);
                return lstGroup;
            }

            foreach (var roleItem in arrRole)
            {
                if(roleItem == RoleSystem.Admin.GetHashCode() )
                {
                    continue;
                }

                if(this.ListRoleGroup.ContainsKey(roleItem))
                {
                    if (this.ListRoleGroup[roleItem].Contains(DefaultSystemParameter.AllGroup))
                    {
                        lstGroup = new List<int>() { DefaultSystemParameter.AllGroup };
                        return lstGroup;
                    }

                    foreach (var groupItem in this.ListRoleGroup[roleItem])
                    {
                        if (!lstGroup.Contains(groupItem))
                        {
                            lstGroup.Add(groupItem);
                        }
                    }
                }
            }
            return lstGroup;
        }
    }
    public sealed class LogonViewModel
    {
        [Required(ErrorMessage = "Bạn vui lòng điền tên đăng nhập!")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Bạn vui lòng điền mật khẩu")]
        [DataType(DataType.Password)]
        public string Password
        {
            get;
            set;
        }

        public bool RememberMe { get; set; }

        public string Role { get; set; }

        public bool Status { get; set; }

        public LogonViewModel()
        {
            UserName = "";
            Password = "";
            RememberMe = false;
            Role = "";
            Status = true;
        }
    }
    public sealed class ChangePasswordModel
    {
        [Required(ErrorMessage = "Bạn vui lòng điền mật khẩu")]
        [DataType(DataType.Password)]
        public string OldPassword
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Bạn vui lòng điền mật khẩu")]
        [DataType(DataType.Password)]
        public string NewPassword
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Bạn vui lòng điền mật khẩu")]
        [DataType(DataType.Password)]
        public string ConfirmPassword
        {
            get;
            set;
        }
    }
    public class ResetPasswordModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        [Required]
        [Display(Name = "Password mới:")]
        public string NewPassword { get; set; }
        [Required]
        [Display(Name = "Xác nhận password:")]
        [Compare("NewPassword")]
        public string VerifyPassword { get; set; }

        public ResetPasswordModel()
        { }

        public ResetPasswordModel(UserAuthenticateModel user)
        {
            Id = user.Id;
            UserName = user.UserName;
        }
    }
}