﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class CustomerIndexModel
    {
        public CustomerIndexModel()
        {
            this.PageIndex = 1;
            this.PageSize = 25;
            this.Sort = -1;
        }
        private string _filterKeyword;
        public string FilterKeyword {
            get
            {
                return _filterKeyword;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _filterKeyword = "";
                }
                else
                {
                    _filterKeyword = value.Trim();
                }
            }
        }
        public string AssignTo { get; set; }
        public int? Status { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int Sort { get; set; }
        public int? Type { get; set; }
        public string Creator { get; set; }
        private DateTime _startDate;
        public DateTime? StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                if(!value.HasValue)
                {
                    _startDate= new DateTime(2000, 1, 1, 0, 0, 0);
                }
                else
                {
                    if (value == DateTime.MinValue)
                    {
                        _startDate = new DateTime(2000, 1, 1, 0, 0, 0);
                    }
                    else
                    {
                        _startDate = value.Value;
                    }
                }
            }
        }
        private DateTime _endDate;
        public DateTime? EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                if (!value.HasValue)
                {
                    _endDate = new DateTime(9999, 1, 1, 0, 0, 0);
                }
                else
                {
                    if (value == DateTime.MinValue)
                    {
                        _endDate = new DateTime(9999, 1, 1, 0, 0, 0);
                    }
                    else
                    {
                        _endDate = new DateTime( value.Value.Year,value.Value.Month,value.Value.Day,23,59,59) ;
                    }
                }
            }
        }
        public List<int> LstGroupId { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool IsNull
        {
            get
            {
                if (FilterKeyword != null
                    || StartDate != null
                    || EndDate != null
                    || CityId != null
                    || DistrictId != null
                    || Status != null
                    || AssignTo != null
                    || Type != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public List<CustomerForIndex> ListCustomer { get; set; }
        public List<Group> LstGroup { get; set; }
    }
    public class CustomerForIndex
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        private string _phoneNumber;
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value.WrapPhoneNumber(); }
        }
        public string AssignTo { get; set; }
        public int IAssignTo { get; set; }
        public int Status { get; set; }
        public string StatusName
        {
            get
            {
                return ((CustomerStatus)this.Status).ToCustomerStatusName();
            }
        }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int RegionId { get; set; }
        public string RegionName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public int ICareDate { get; set; }
        public DateTime CareDate { get; set; }
        public DateTime StartCareDate { get; set; }
        public int Type { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public int TotalRecord { get; set; }
        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string CreatedBy { get; set; }
        public int SumPostingNews { get; set; }
        public bool IsBlacklist { get; set; }

        public bool IsReturned(string currUser)
        {
            return (!this.IsDeleted && this.AssignTo.Equals(currUser)) ? true : false;
        }
        public bool IsEditable(bool isManager, string userName)
        {
            if(isManager)
            {
                return true;
            }
            if(this.AssignTo.Equals(userName))
            {
                return this.IsDeleted ? false : true;
            }

            return false;
        }
        public bool IsBlockable(bool isManager)
        {
            return (!this.IsDeleted && isManager) ? true : false;
        }
        public bool IsUnblockable(bool isManager)
        {
            return (this.IsDeleted && isManager) ? true : false;
        }
        public bool IsAssigningTo(bool isManager)
        {
            return (!this.IsDeleted && isManager) ? true : false;
        }
        public bool IsBlacklisted(bool isManager)
        {
            return (isManager && !this.IsBlacklist && !this.IsDeleted ) ? true : false;
        }
        public bool IsUnblacklisted(bool isManager)
        {
            return (isManager && this.IsBlacklist && !this.IsDeleted) ? true : false;
        }
        public bool IsReceived()
        {
            return string.IsNullOrEmpty(this.AssignTo) ? true : false;
        }
        public bool IsNoted(bool isManager, string userName)
        {
            if (isManager)
            {
                return true;
            }
            if (this.AssignTo.Equals(userName))
            {
                return this.IsDeleted ? false : true;
            }
            return false;
        }
        public bool IsViewDetail()
        {
            return this.IsDeleted ? false : true;
        }
    }
    public class CustomerRequest
    {
        private string _fullName;
        [Required]
        public string FullName {
            get
            {
                return _fullName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _fullName = "";
                }
                else
                {
                    _fullName = value.Trim();
                }
            }
        }
        [Required]
        public string PhoneNumber { get; set; }

        private string _email;
        [Required]
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                if(string.IsNullOrEmpty(value))
                {
                    _email= "";
                }
                else
                {
                    _email =  value.ToLower().Trim();
                }
            }
        }
        public int RegionId { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
        public string Description { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public string AssignTo { get; set; }
        public int CompanyId { get; set; }
        public string Address { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string LastModifiedBy { get; set; }
        //public DateTime LastModifiedDate { get; set; }
        //public int ICareDate { get; set; }
        //public DateTime StartCareDate { get; set; }
        //public List<Profile> ListProfile { get; set; }
        //public int Id { get; set; }
        //public bool IsDeleted { get; set; }
    }
    public class SumPostingNewsCustomer
    {
        public int AdminId { get; set; }
        public int SumPostingNews { get; set; }
    }
    public class CustomerDetailModel
    {
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string StatusName { get; set; }
        public string TypeOfCustomerName { get; set; }
        public string RegionName { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string AssignTo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string CompanyName { get; set; }
        public List<CustomerHistory> LstActivityHistory { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int SumRejectionTimes { get; set; }

        public CustomerDetailModel()
        {
            this.LstActivityHistory = new List<CustomerHistory>();
        }
    }
}