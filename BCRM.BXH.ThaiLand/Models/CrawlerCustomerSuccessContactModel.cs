﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class CrawlerCustomerSuccessContactModel
    {
        public int CareHistoryId { get; set; }
        public long CrawlerId { get; set; }
        private string _fullName;
        public string FullName
        {
            get { return _fullName; }
            set
            {
                if(string.IsNullOrEmpty(value))
                {
                    _fullName = "";
                }
                else
                {
                    _fullName = value.Trim();
                }
            }
        }
        public string PhoneNumber { get; set; }
        public int Type { get; set; }
        private string _email;
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                if(string.IsNullOrEmpty(value))
                {
                    _email = "";
                }
                else
                {
                    _email = value.ToLower().Trim();
                }
            }
        }
        private string _description;
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                if(string.IsNullOrEmpty(value))
                {
                    _description = "";
                }
                else
                {
                    _description = value;
                }
            }
        }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int RegionId { get; set; }
        public int CompanyId { get; set; }
        public string Address { get; set; }
        public string  AssignTo { get; set; }

    }
}