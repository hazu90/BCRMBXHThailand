﻿using BCRM.BXH.ThaiLand.Entity;
using BCRM.BXH.ThaiLand.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    /// <summary>
    /// Các điều kiện tìm kiếm của danh sách crawler
    /// </summary>
    public class CrawlerCustomerIndexModel
    {
        private string _filterKeyword;
        /// <summary>
        /// Tìm kiếm theo email , số điện thoại
        /// </summary>
        public string FilterKeyword
        {
            get
            {
                return _filterKeyword;
            }
            set
            {
                if(string.IsNullOrEmpty(value))
                {
                    _filterKeyword = "";
                }
                else
                {
                    _filterKeyword = value.Trim();
                }
            }
        }
        /// <summary>
        /// Trạng thái liên hệ
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DateOfCreationFrom { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalRecord { get; set; }
        public string DateOfCreationTo { get; set; }
        public List<CrawlerCustomerSearchModel> ListData { get; set; }
        public List<long> ListIgnoreAdminId { get; set; }
        public List<long> ListFilterAdminId { get; set; }

        public CrawlerCustomerIndexModel()
        {
            this.ListData = new List<CrawlerCustomerSearchModel>();
            this.Status = -1;
            this.FilterKeyword = string.Empty;
            this.ListIgnoreAdminId = new List<long>();
            this.ListFilterAdminId = new List<long>();
        }

        #region Các thông tin tìm kiếm bên BCRM
        public string AssignTo { get; set; }
        public List<Group> LstGroup;
        public List<UserSearchModel> LstUser;
        #endregion
    }
    /// <summary>
    /// Thông tin kết quả tìm kiếm tài khoản crawler
    /// </summary>
    public class CrawlerCustomerSearchModel
    {
        /// <summary>
        /// Id tài khoản
        /// </summary>
        public int AdminId { get; set; }
        /// <summary>
        /// Tên
        /// </summary>
        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Địa chỉ
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Số tin đăng đã crawl
        /// </summary>
        public int CrawleredPostingNews { get; set; }
        /// <summary>
        /// Trạng thái liên hệ
        /// (chưa liên hệ , từ chối)
        /// </summary>
        public int Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public string AssignTo { get; set; }
        public bool IsReceived
        {
            get
            {
                if(string.IsNullOrEmpty(AssignTo))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool IsWaitingOverRejectDeadline { get; set; }
        public bool IsUpdatePhoningStatus(string currUserName)
        {
            if (string.IsNullOrEmpty(AssignTo))
            {
                return false;
            }
            else
            {
                if(!currUserName.Equals(AssignTo))
                {
                    return false;
                }

                if (Status == PhoningStatus.Success.GetHashCode() || Status == PhoningStatus.Failure.GetHashCode())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public int CareHistoryId { get; set; }
        public string Note { get; set; }
    }
}