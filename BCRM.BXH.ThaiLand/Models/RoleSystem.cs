﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class RoleSystem
    {
        public const int Sale = 6;
        public const int Manager = 7;
        public const int Admin = 8;
        public const int Coordinate = 9;
        public const int TeamLeader = 10;
    }
}