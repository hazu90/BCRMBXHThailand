﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class DefaultSystemParameter
    {
        /// <summary>
        /// Tham số miêu tả tất cả các group của hệ thống
        /// </summary>
        public const int AllGroup = -1000;
        /// <summary>
        /// Tham số miêu tả thông tin khách hàng không được giao cho một nhóm nào cả
        /// </summary>
        public const int CustomerNotBelongToGroup = 0;
        /// <summary>
        /// Tham số miêu tả số ngày cần phải liên lạc
        /// với khách hàng để tạo tài khoản
        /// quá thời hạn này khách hàng sẽ bị đưa
        /// vào danh sách chung để cho các sale khác
        /// có thể nhận
        /// </summary>
        public const int DaysOverContactDeadline = 3;
        /// <summary>
        /// Số ngày chờ sau khi khách hàng từ chối
        /// không tham gia đối với 1 sale
        /// </summary>
        public const int DaysWaitingAfterRejecting = 7;
        /// <summary>
        /// Số ngày tối đa bổ sung để lấy ra danh sách
        /// các tài khoản active
        /// </summary>
        public const int DayMaximumToMarkAccountActive = 7;
        /// <summary>
        /// Người tạo là khách
        /// </summary>
        public const string GuestCreator = "Guest";
    }
}