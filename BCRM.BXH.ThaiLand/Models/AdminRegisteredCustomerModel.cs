﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class AdminRegisteredCustomerModel
    {
        /// <summary>
        /// id admin của khách hàng
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// tên của khách hàng
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Email của khách hàng
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Số điện thoại của khách hàng
        /// </summary>
        public string Phonenumber { get; set; }
        /// <summary>
        /// Địa chỉ của khách hàng
        /// </summary>
        public string Address { get; set; }
    }

}