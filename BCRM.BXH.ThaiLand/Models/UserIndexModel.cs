﻿using BCRM.BXH.ThaiLand.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class UserIndexModel
    {
        public List<Group> LstGroup { get; set; }
        public List<UserSearchModel> LstUsers { get; set; }
    }
}