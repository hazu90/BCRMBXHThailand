﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class UserRoleGroupSearchModel
    {
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string GroupName { get; set; }
    }
}