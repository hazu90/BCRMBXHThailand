﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class SumReceivedCustomerModel
    {
        public int UserId { get; set; }
        public int SumReceivedCustomer { get; set; }
    }
}