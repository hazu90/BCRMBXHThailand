﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class GroupIndexModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Leader { get; set; }
        public string LeaderName { get; set; }
        public int NumSaler { get; set; }
        public string Description { get; set; }
        public bool IsEditable { get; set; }
    }
}