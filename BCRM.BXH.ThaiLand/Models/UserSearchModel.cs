﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class UserSearchModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public DateTime DOB { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int Status { get; set; }
        public DateTime LastActivityDate { get; set; }
    }
}