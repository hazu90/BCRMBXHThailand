﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class AdminPostingNewsModel
    {
        public long AdminId { get; set; }
        public int SumPostingNews { get; set; }
    }
}