﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.ThaiLand.Models
{
    public class UserSaveModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public int Status { get; set; }
        public DateTime? DOB { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int GroupId { get; set; }
        public string AdminUsername { get; set; }
        public int Id { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class UserForChangeProfile
    {
        public int UserId { get; set; }
        public string DisplayName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public DateTime? DOB { get; set; }
    }
}