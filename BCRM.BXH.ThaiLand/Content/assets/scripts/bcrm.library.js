﻿/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các mã trả về từ Server
************************************************************************/
var ResponseCode = {
    Success: 1,
    Error: 0,
    ErrorExist: 2,
    DataNull: 3,
    ErrorParam: 4,
    NotPermitted: 5,
    NotValid: 6,
    Locked: 7,
    Overflow: 8,
    ErrorConnect: 9,
    Sync: 10
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các trạng thái khách hàng
************************************************************************/
var CustomerStatus = {
    Waiting: 1,
    Indecivise: 2,
    Success: 3,
    Rejected: 4
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các loại khách hàng
************************************************************************/
var CustomerType = {
    Owner: 0,
    Broker: 1,
    BrokerBusiness: 2,
    Investor: 3,
    RealEstateTradingFloor: 4,
    Agency: 5,
    Other: 6
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các trạng thái cơ hội
************************************************************************/
var OpportunityStatus = {
    Running: 1,
    Success: 2,
    Rejected: 3,
    Waiting: 4
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các loại thanh toán
************************************************************************/
var TypeOfPayment = {
    Banking: 1,
    DirectMoney: 2,
    Sms: 3,
    Handed: 4,
    Card: 5,
    BaoKim: 6
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các method dùng để hiển thị message
************************************************************************/
var sysmess = {
    log: function (msg) {
        console.log(msg);
    },
    info: function (msg, func) {
        sysdialog.show({ element: $('#dialog-info'), message: msg, process: func });
    },
    error: function (msg, func) {
        sysdialog.show({ element: $('#dialog-error'), message: msg, process: func });
    },
    confirm: function (msg, func) {
        sysdialog.show({ element: $('#dialog-confirm'), message: msg, process: func });
    },
    warning: function (msg, func) {
        sysdialog.show({ element: $('#dialog-warning'), message: msg, process: func });
    }
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các method dùng để xử lý thời gian
************************************************************************/
var systimer = {
    today: function (isFull) {
        var current = new Date();
        if (isFull) {
            return sysformat.date(current, 'dd/mm/yyyy hh:mm:ss');
        }
        else {
            return sysformat.date(current, 'dd/mm/yyyy');
        }
    },
    date: function (data) {
        return new Date(parseInt(data.substr(6)));
    },
    datediff: function (date1, date2) {
        if (typeof (date1) == 'string') {
            var from = date1.split("/");
            date1 = new Date(parseInt(from[2]), parseInt(from[0]) - 1, parseInt(from[1]));
        }
        if (typeof (date2) == 'string') {
            var to = date2.split("/");
            date2 = new Date(parseInt(to[2]), parseInt(to[0]) - 1, parseInt(to[1]));
        }
        var data = parseInt(date1.getTime() - date2.getTime(), 10);
        var d, h, m, s;
        s = Math.floor(data / 1000);
        m = Math.floor(s / 60);
        s = s % 60;
        h = Math.floor(m / 60);
        m = m % 60;
        d = Math.floor(h / 24);
        h = h % 24;
        var date = {
            days: d,
            hours: h,
            minutes: m,
            seconds: s
        };
        return date;
    },
    monthdiff: function (date1, date2) {
        if (typeof (date1) == 'string') {
            var from = date1.split("/");
            date1 = new Date(parseInt(from[2]), parseInt(from[0]) - 1, parseInt(from[1]));
        }
        if (typeof (date2) == 'string') {
            var from = date2.split("/");
            date2 = new Date(parseInt(from[2]), parseInt(from[0]) - 1, parseInt(from[1]));
        }
        var month = 0;
        if (date1 >= date2) {
            month = (date1.getFullYear() - date2.getFullYear()) * 12 + (date1.getMonth() - date2.getMonth());
        }
        else {
            month = ((date2.getFullYear() - date1.getFullYear()) * 12 + (date2.getMonth() - date1.getMonth())) * -1;
        }
        return month;
    }
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các method dùng để format dữ liệu
************************************************************************/
var sysformat = {
    money: function (value, options) {
        var defaults = {
            symbol: '₫',
            separator: ',',
            alignSymbol: 'right'
        };
        var settings = options == null ? defaults : $.extend({}, defaults, options);
        var buf = "";
        var sBuf = "";
        var j = 0;
        var isNegative = value < 0;
        value = String(Math.abs(value));
        if (value.indexOf(".") > 0) {
            buf = value.substring(0, value.indexOf("."));
        } else {
            buf = value;
        }
        if (buf.length % 3 != 0 && (buf.length / 3 - 1) > 0) {
            sBuf = buf.substring(0, buf.length % 3) + settings.separator;
            buf = buf.substring(buf.length % 3);
        }
        j = buf.length;
        for (var i = 0; i < (j / 3 - 1); i++) {
            sBuf = sBuf + buf.substring(0, 3) + settings.separator;
            buf = buf.substring(3);
        }
        sBuf = sBuf + buf;
        if (value.indexOf(".") > 0) 
        {
            value = sBuf + value.substring(value.indexOf("."));
        }
        else {
            value = sBuf;
        }
        if (isNegative) 
        {
            value = "-" + value;
        }
        switch (settings.alignSymbol) {
            case "right":
                value = value + ' ' + settings.symbol;
                break;
            case "left":
                value = settings.symbol + ' ' + value;
                break;
            default:
                value = value + settings.symbol;
                break;
        }
        return value;
    },
    number: function (value) {
        var number = Number(value.replace(/[^0-9\.-]+/g, ""));
        return number;
    },
    date: function (data, fmt) {
        if ($.trim(data) == '') {
            return '';
        }
        var date = null;
        switch (typeof (data)) {
            case 'string':
                var temp = data.split(' ');
                if (temp.length > 1) {
                    date = temp[0].split('/').concat(temp[1].split(':'));
                }
                else {
                    date = temp[0].split('/');
                }
                break;
            case 'object':
                if (data instanceof Date) {
                    date = new Array();
                    if (data.getDate() < 10) {
                        date.push('0' + data.getDate());
                    }
                    else {
                        date.push(data.getDate());
                    }
                    if (data.getMonth() + 1 < 10) {
                        date.push('0' + (data.getMonth() + 1));
                    }
                    else {
                        date.push(data.getMonth() + 1);
                    }
                    date.push(data.getFullYear());
                    if (data.getHours() < 10) {
                        date.push('0' + data.getHours());
                    }
                    else {
                        date.push(data.getHours());
                    }
                    if (data.getMinutes() < 10) {
                        date.push('0' + data.getMinutes());
                    }
                    else {
                        date.push(data.getMinutes());
                    }
                    if (data.getSeconds() < 10) {
                        date.push('0' + data.getSeconds());
                    }
                    else {
                        date.push(data.getSeconds());
                    }
                }
                else {
                    date = String(data);
                }
                break;
            default:
                date = String(data);
        }
        if (fmt == null) fmt = 'mm/dd/yyyy';
        switch (fmt) {
            case 'yyyy/mm/dd':
                return date[2] + '/' + date[1] + '/' + date[0];
            case 'dd/mm/yyyy':
                return date[0] + '/' + date[1] + '/' + date[2];
            case 'mm/dd/yyyy':
                return date[1] + '/' + date[0] + '/' + date[2];
            case 'dd/mm/yyyy hh:mm:ss':
                return date[0] + '/' + date[1] + '/' + date[2] + ' ' + date[3] + ':' + date[4] + ':' + date[5];
            case 'mm/dd/yyyy hh:mm:ss':
                return date[1] + '/' + date[0] + '/' + date[2] + ' ' + date[3] + ':' + date[4] + ':' + date[5];
            case 'hh:mm dd/mm/yyyy':
                return date[3] + ':' + date[4] + ' ' + date[0] + '/' + date[1] + '/' + date[2];
            case 'mm/yyyy':
                return date[1] + '/' + date[2];
            case 'yyyymmdd':
                return date[2] + date[1] + date[0];
            case 'yyyymm':
                if (date.length > 2) {
                    return date[2] + date[1];
                }
                else {
                    return date[1] + date[0];
                }
            default:
                return data;
        }
    }
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các method dùng để làm việc với editor
************************************************************************/
var syseditor = {
    destroy: function (name) {
        var instance = CKEDITOR.instances[name];
        if (instance) {
            instance.destroy();
        }
    },
    create: function (name) {
        CKEDITOR.replace(name);
    },
    set_data: function (name, value) {
        if (CKEDITOR.instances[name]) {
            CKEDITOR.instances[name].setData(value);
        }
    },
    get_data: function (name) {
        if (CKEDITOR.instances[name]) {
            return CKEDITOR.instances[name].getData();
        }
        else {
            return '';
        }
    },
    focus: function (name) {
        if (CKEDITOR.instances[name]) {
            CKEDITOR.instances[name].focus();
        }
    }
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các method dùng để valid data
************************************************************************/
var sysvalid = {
    email: function (value) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(value);
    },
    json: function (value) {
        if (/^[\],:{}\s]*$/.test(value.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
            return true;
        }
        else {
            return false;
        }
    },
    searchstring: function (value) {
        if (value.indexOf("<") > -1 || value.indexOf("'") > -1 || value.indexOf('"') > -1) {
            return false;
        }
        else {
            return true;
        }
    }
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các method tiện tích thường dùng
************************************************************************/
var syscommon = {
    scroll: function (obj) {
        try {
            $('html, body').animate({
                scrollTop: obj.offset().top - 50
            }, 200);
        }
        catch (e) {
            sysmess.error('Lỗi javascript! <br />' + e);
        }
    },
    replace: function (value, oldChar, newChar) {
        var regex = new RegExp(oldChar, "g");
        return value.replace(regex, newChar);
    },
    cutstring: function (str, max) {
        if (max >= str.length) {
            return str;
        }
        else {
            var s = str.substring(0, max);
            s = s.substring(0, s.lastIndexOf(" "));
            return s + '...';
        }
    },
    random: function (length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    },
    show_valid_message: function (element, text) {
        element.parent("div").parent("div").addClass("error");
        element.next("span.help-inline").remove();
        element.parent("div").append('<span class="help-inline no-left-padding">' + text + '</span>');
    },
    remove_valid_message: function (element) {
        $("span.help-inline", element).each(function () {
            $(this).parent("div").parent("div").removeClass("error");
            $(this).parent("div").parent("div").parent("div").removeClass("error");
            $(this).remove();
        });
    },
    load_user: function (ddlGroup, ddlUser) {
        var lstGroup = ddlGroup.data("json");
        var lstUser = ddlUser.data("json");
        var arrActiveGroup = lstGroup[ddlGroup.val()];
        var option = $("option:first", ddlUser);
        ddlUser.empty();
        var html = "";
        for (var item in lstUser) {
            if (ddlGroup.val() == null || ddlGroup.val() == "" || $.inArray(lstUser[item].GroupId, arrActiveGroup) > -1) {
                html += '<option value="' + lstUser[item].UserName + '">' + lstUser[item].UserName + '</option>';
            }
        }
        if (option.length > 0 && (option.val() == null || option.val() == "")) {
            option.appendTo(ddlUser);
        }
        $(html).appendTo(ddlUser);
        if (ddlUser.hasClass("chosen")) {
            ddlUser.trigger("liszt:updated");
        }
    },
    get_type_of_payment_name: function (type) {
        switch (type) {
            case TypeOfPayment.Banking:
                return "Chuyển Khoản";
            case TypeOfPayment.DirectMoney:
                return "Thu Phí Trực Tiếp";
            case TypeOfPayment.Sms:
                return "SMS";
            case TypeOfPayment.Handed:
                return "Thu Phí Bàn Giao";
            case TypeOfPayment.Card:
                return "Thẻ Cào";
            case TypeOfPayment.BaoKim:
                return "Bảo Kim";
            default:
                return "";
        }
    },
    get_opportunity_status_name: function (status) {
        switch (status) {
            case OpportunityStatus.Success:
                return "Hoàn Thành";
            case OpportunityStatus.Rejected:
                return "Từ Chối";
            case OpportunityStatus.Running:
                return "Đang Tiến Hành";
            case OpportunityStatus.Waiting:
                return "Chờ Xác Nhận";
            default:
                return "";
        }
    }
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các method dùng để vẽ mã html
************************************************************************/
var syshtml = {
    def: {
        Type: "modal" | "form" | "html",
        IsNoDiv: false,
        Box: {
            Class: ""
        },
        Head: {
            Title: "",
            Icon: "",
            RemoveId: "",
            Relation: ""
        },
        Body: [
            [{
                Type: "ui" | "section" | "html" | "table" | "tr",
                Class: "",
                Title: "",
                Id: "",
                UI: {
                    Type: "text" | "textarea" | "label" | "span" | "select" | "checkbox" | "radio" | "button" | "link",
                    Title: "",
                    Html: "",
                    Id: "",
                    Name: "",
                    Class: "",
                    Attribute: [{
                        Name: "",
                        Value: ""
                    }],
                    AddOn: {
                        Class: "",
                        Item: [{
                            Title: "",
                            Class: ""
                        }]
                    },
                    Option: [{
                        Value: "",
                        Text: ""
                    }],
                    Icon: "",
                    Text: "",
                    THead: [
                        [{
                            Id: "",
                            Name: "",
                            Class: "",
                            Attribute: [{
                                Name: "",
                                Value: ""
                            }],
                            Column: [{
                                Id: "",
                                Name: "",
                                Class: "",
                                Attribute: [{
                                    Name: "",
                                    Value: ""
                                }],
                                Html: ""
                            }]
                        }]
                    ],
                    TBody: [
                        [{
                            Id: "",
                            Name: "",
                            Class: "",
                            Attribute: [{
                                Name: "",
                                Value: ""
                            }],
                            Column: [{
                                Id: "",
                                Name: "",
                                Class: "",
                                Attribute: [{
                                    Name: "",
                                    Value: ""
                                }],
                                Html: "",
                                Button: {
                                    Type: "group" | "link" | "button",
                                    Title: "",
                                    Class: "",
                                    Item: [{
                                        Id: "",
                                        Name: "",
                                        Class: "",
                                        Attribute: [{
                                            Name: "",
                                            Value: ""
                                        }],
                                        Text: "",
                                        Icon: ""
                                    }]
                                }
                            }]
                        }]
                    ]
                },
                Required: true | false
            }]
        ],
        Button: [{
            Type: "link" | "button",
            Id: "",
            Name: "",
            Class: "",
            Attribute: [{
                Name: "",
                Value: ""
            }],
            Icon: "",
            Value: "",
            Text: ""
        }]
    },
    raw: function (option) {
        var html = '';
        try {
            switch (option.Type.toLowerCase()) {
                case "modal":
                    html += '<div class="modal-header">';
                    html += '   <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>';
                    html += '   <h3>' + (option.Head.Title != null ? option.Head.Title : "") + '</h3>';
                    html += '</div>';
                    html += '<div class="modal-body' + (option.Box != null ? (option.Box.Class != null ? " " + option.Box.Class : "") : "") + '">';
                    html += syshtml.render(true, option.Body, option.IsNoDiv);
                    html += '</div>';
                    html += '<div class="modal-footer">';
                    html += syshtml.render(false, option.Button, option.IsNoDiv);
                    html += '   <button type="button" class="btn blue" data-dismiss="modal" aria-hidden="true" title="Close">';
                    html += '       <i class="icon-remove"></i>';
                    html += '       Close';
                    html += '   </button>';
                    html += '</div>';
                    break;
                case "form":
                    html += '<div class="portlet box ' + (option.Box != null ? (option.Box.Class != null ? option.Box.Class : "blue") : "blue") + '">';
                    html += '   <div class="portlet-title">';
                    html += '       <div class="caption">';
                    html += '           <i class="' + (option.Head.Icon != null ? option.Head.Icon : "") + '"></i>';
                    html += '           ' + (option.Head.Title != null ? option.Head.Title : "");
                    html += '       </div>';
                    html += '       <div class="tools">';
                    html += '           <a href="javascript:;" class="collapse"></a>';
                    html += '           <a href="javascript:;" class="remove" id="' + (option.Head.RemoveId != null ? option.Head.RemoveId : "") + '" control-relation="' + (option.Head.Relation != null ? option.Head.Relation : "") + '"></a>';
                    html += '       </div>';
                    html += '   </div>';
                    html += '   <div class="portlet-body form">';
                    html += syshtml.render(true, option.Body, option.IsNoDiv);
                    html += '       <div class="form-actions">';
                    html += syshtml.render(false, option.Button, option.IsNoDiv);
                    html += '       </div>';
                    html += '   </div>';
                    html += '</div>';
                    break;
                case "html":
                    html = syshtml.render(true, option.Body, option.IsNoDiv);
                    break;
            }
        } catch (e) {
            html = e;
        }
        return html;
    },
    render: function (isBody, data, isNoDiv) {
        var html = '';
        if (isBody) {
            for (var i = 0; i < data.length; i++) {
                if (!isNoDiv) {
                    html += '<div class="row-fluid">';
                }
                for (var j = 0; j < data[i].length; j++) {
                    if (!isNoDiv) {
                        html += '<div' + (data[i][j].Class != null ? ' class="' + data[i][j].Class + '"' : '') + (data[i][j].Id != null ? ' id="' + data[i][j].Id + '"' : '') + '>';
                    }
                    switch (data[i][j].Type.toLowerCase()) {
                        case "section":
                            html += '<h3 class="form-section">' + data[i][j].Title + '</h3>';
                            break;
                        case "html":
                            html += data[i][j].UI.Html;
                            break;
                        case "table":
                            html += '<table';
                            if (data[i][j].UI.Id != null) {
                                html += ' id="' + data[i][j].UI.Id + '"';
                            }
                            if (data[i][j].UI.Name != null) {
                                html += ' name="' + data[i][j].UI.Name + '"';
                            }
                            if (data[i][j].UI.Class != null) {
                                html += ' class="' + data[i][j].UI.Class + '"';
                            }
                            if (data[i][j].UI.Attribute != null) {
                                for (var k = 0; k < data[i][j].UI.Attribute.length; k++) {
                                    html += ' ' + data[i][j].UI.Attribute[k].Name + '=\'' + data[i][j].UI.Attribute[k].Value + '\'';
                                }
                            }
                            html += '>';
                            html += '   <thead>';
                            if (data[i][j].UI.THead != null) {
                                for (var k = 0; k < data[i][j].UI.THead.length; k++) {
                                    html += '   <tr';
                                    if (data[i][j].UI.THead[k].Id != null) {
                                        html += ' id="' + data[i][j].UI.THead[k].Id + '"';
                                    }
                                    if (data[i][j].UI.THead[k].Name != null) {
                                        html += ' name="' + data[i][j].UI.THead[k].Name + '"';
                                    }
                                    if (data[i][j].UI.THead[k].Class != null) {
                                        html += ' class="' + data[i][j].UI.THead[k].Class + '"';
                                    }
                                    if (data[i][j].UI.THead[k].Attribute != null) {
                                        for (var l = 0; l < data[i][j].UI.THead[k].Attribute.length; l++) {
                                            html += ' ' + data[i][j].UI.THead[k].Attribute[l].Name + '=\'' + data[i][j].UI.THead[k].Attribute[l].Value + '\'';
                                        }
                                    }
                                    html += '>';
                                    if (data[i][j].UI.THead[k].Column != null) {
                                        for (var l = 0; l < data[i][j].UI.THead[k].Column.length; l++) {
                                            html += '   <th';
                                            if (data[i][j].UI.THead[k].Column[l].Id != null) {
                                                html += ' id="' + data[i][j].UI.THead[k].Column[l].Id + '"';
                                            }
                                            if (data[i][j].UI.THead[k].Column[l].Name != null) {
                                                html += ' name="' + data[i][j].UI.THead[k].Column[l].Name + '"';
                                            }
                                            if (data[i][j].UI.THead[k].Column[l].Class != null) {
                                                html += ' class="' + data[i][j].UI.THead[k].Column[l].Class + '"';
                                            }
                                            if (data[i][j].UI.THead[k].Column[l].Attribute != null) {
                                                for (var m = 0; m < data[i][j].UI.THead[k].Column[l].Attribute.length; m++) {
                                                    html += ' ' + data[i][j].UI.THead[k].Column[l].Attribute[m].Name + '=\'' + data[i][j].UI.THead[k].Column[l].Attribute[m].Value + '\'';
                                                }
                                            }
                                            html += '>';
                                            if (data[i][j].UI.THead[k].Column[l].Icon != null) {
                                                html += '   <i class="' + data[i][j].UI.THead[k].Column[l].Icon + '"></i>  ';
                                            }
                                            if (data[i][j].UI.THead[k].Column[l].Html != null) {
                                                html += data[i][j].UI.THead[k].Column[l].Html;
                                            }
                                            html += '   </th>';
                                        }
                                    }
                                    html += '   </tr>';
                                }
                            }
                            html += '   </thead>';
                            html += '   <tbody>';
                            if (data[i][j].UI.TBody != null) {
                                for (var k = 0; k < data[i][j].UI.TBody.length; k++) {
                                    html += '   <tr';
                                    if (data[i][j].UI.TBody[k].Id != null) {
                                        html += ' id="' + data[i][j].UI.TBody[k].Id + '"';
                                    }
                                    if (data[i][j].UI.TBody[k].Name != null) {
                                        html += ' name="' + data[i][j].UI.TBody[k].Name + '"';
                                    }
                                    if (data[i][j].UI.TBody[k].Class != null) {
                                        html += ' class="' + data[i][j].UI.TBody[k].Class + '"';
                                    }
                                    if (data[i][j].UI.TBody[k].Attribute != null) {
                                        for (var l = 0; l < data[i][j].UI.TBody[k].Attribute.length; l++) {
                                            html += ' ' + data[i][j].UI.TBody[k].Attribute[l].Name + '=\'' + data[i][j].UI.TBody[k].Attribute[l].Value + '\'';
                                        }
                                    }
                                    html += '>';
                                    if (data[i][j].UI.TBody[k].Column != null) {
                                        for (var l = 0; l < data[i][j].UI.TBody[k].Column.length; l++) {
                                            html += '   <td';
                                            if (data[i][j].UI.TBody[k].Column[l].Id != null) {
                                                html += ' id="' + data[i][j].UI.TBody[k].Column[l].Id + '"';
                                            }
                                            if (data[i][j].UI.TBody[k].Column[l].Name != null) {
                                                html += ' name="' + data[i][j].UI.TBody[k].Column[l].Name + '"';
                                            }
                                            if (data[i][j].UI.TBody[k].Column[l].Class != null) {
                                                html += ' class="' + data[i][j].UI.TBody[k].Column[l].Class + '"';
                                            }
                                            if (data[i][j].UI.TBody[k].Column[l].Attribute != null) {
                                                for (var m = 0; m < data[i][j].UI.TBody[k].Column[l].Attribute.length; m++) {
                                                    html += ' ' + data[i][j].UI.TBody[k].Column[l].Attribute[m].Name + '=\'' + data[i][j].UI.TBody[k].Column[l].Attribute[m].Value + '\'';
                                                }
                                            }
                                            html += '>';
                                            if (data[i][j].UI.TBody[k].Column[l].Html != null) {
                                                html += data[i][j].UI.TBody[k].Column[l].Html;
                                            }
                                            if (data[i][j].UI.TBody[k].Column[l].Button != null) {
                                                var isGroup = data[i][j].UI.TBody[k].Column[l].Button.Type == "group";
                                                var isLink = data[i][j].UI.TBody[k].Column[l].Button.Type == "link";
                                                var isButton = data[i][j].UI.TBody[k].Column[l].Button.Type == "button";
                                                if (isGroup) {
                                                    html += '<div class="btn-group none-bottom">';
                                                    html += '   <button class="btn dropdown-toggle ' + data[i][j].UI.TBody[k].Column[l].Button.Class + '" data-toggle="dropdown">';
                                                    html += '       <i class="icon-cog"></i>';
                                                    html += '   ' + data[i][j].UI.TBody[k].Column[l].Button.Title;
                                                    html += '       <i class="icon-angle-down"></i>';
                                                    html += '   </button>';
                                                    html += '   <ul class="dropdown-menu pull-right">';
                                                }
                                                if (data[i][j].UI.TBody[k].Column[l].Button.Item != null) {
                                                    var arrItem = data[i][j].UI.TBody[k].Column[l].Button.Item;
                                                    for (var m = 0; m < arrItem.length; m++) {
                                                        if (isGroup) {
                                                            html += '<li>';
                                                            html += '   <a';
                                                        }
                                                        if (isLink) {
                                                            html += '   <a';
                                                        }
                                                        if (isButton) {
                                                            html += '   <button';
                                                        }
                                                        if (arrItem[m].Id != null) {
                                                            html += ' id="' + arrItem[m].Id + '"';
                                                        }
                                                        if (arrItem[m].Name != null) {
                                                            html += ' name="' + arrItem[m].Name + '"';
                                                        }
                                                        if (arrItem[m].Class != null) {
                                                            html += ' class="' + arrItem[m].Class + '"';
                                                        }
                                                        if (arrItem[m].Attribute != null) {
                                                            for (var n = 0; n < arrItem[m].Attribute.length; n++) {
                                                                html += ' ' + arrItem[m].Attribute[n].Name + '=\'' + arrItem[m].Attribute[n].Value + '\'';
                                                            }
                                                        }
                                                        html += '>';
                                                        if (arrItem[m].Icon != null) {
                                                            html += '   <i class="' + arrItem[m].Icon + '"></i>  ';
                                                        }
                                                        if (arrItem[m].Text != null) {
                                                            html += arrItem[m].Text;
                                                        }
                                                        if (isLink) {
                                                            html += '   </a>';
                                                        }
                                                        if (isButton) {
                                                            html += '   </button>';
                                                        }
                                                        if (isGroup) {
                                                            html += '   </a>';
                                                            html += '</li>';
                                                        }
                                                    }
                                                }
                                                if (isGroup) {
                                                    html += '   </ul>';
                                                    html += '</div>';
                                                }
                                            }
                                            html += '   </td>';
                                        }
                                    }
                                    html += '   </tr>';
                                }
                            }
                            html += '   </tbody>';
                            html += '</table>';
                            break;
                        case "tr":
                            if (data[i][j].UI.TBody != null) {
                                for (var k = 0; k < data[i][j].UI.TBody.length; k++) {
                                    html += '   <tr';
                                    if (data[i][j].UI.TBody[k].Id != null) {
                                        html += ' id="' + data[i][j].UI.TBody[k].Id + '"';
                                    }
                                    if (data[i][j].UI.TBody[k].Name != null) {
                                        html += ' name="' + data[i][j].UI.TBody[k].Name + '"';
                                    }
                                    if (data[i][j].UI.TBody[k].Class != null) {
                                        html += ' class="' + data[i][j].UI.TBody[k].Class + '"';
                                    }
                                    if (data[i][j].UI.TBody[k].Attribute != null) {
                                        for (var l = 0; l < data[i][j].UI.TBody[k].Attribute.length; l++) {
                                            html += ' ' + data[i][j].UI.TBody[k].Attribute[l].Name + '=\'' + data[i][j].UI.TBody[k].Attribute[l].Value + '\'';
                                        }
                                    }
                                    html += '>';
                                    if (data[i][j].UI.TBody[k].Column != null) {
                                        for (var l = 0; l < data[i][j].UI.TBody[k].Column.length; l++) {
                                            html += '   <td';
                                            if (data[i][j].UI.TBody[k].Column[l].Id != null) {
                                                html += ' id="' + data[i][j].UI.TBody[k].Column[l].Id + '"';
                                            }
                                            if (data[i][j].UI.TBody[k].Column[l].Name != null) {
                                                html += ' name="' + data[i][j].UI.TBody[k].Column[l].Name + '"';
                                            }
                                            if (data[i][j].UI.TBody[k].Column[l].Class != null) {
                                                html += ' class="' + data[i][j].UI.TBody[k].Column[l].Class + '"';
                                            }
                                            if (data[i][j].UI.TBody[k].Column[l].Attribute != null) {
                                                for (var m = 0; m < data[i][j].UI.TBody[k].Column[l].Attribute.length; m++) {
                                                    html += ' ' + data[i][j].UI.TBody[k].Column[l].Attribute[m].Name + '=\'' + data[i][j].UI.TBody[k].Column[l].Attribute[m].Value + '\'';
                                                }
                                            }
                                            html += '>';
                                            if (data[i][j].UI.TBody[k].Column[l].Html != null) {
                                                html += data[i][j].UI.TBody[k].Column[l].Html;
                                            }
                                            if (data[i][j].UI.TBody[k].Column[l].Button != null) {
                                                var isGroup = data[i][j].UI.TBody[k].Column[l].Button.Type == "group";
                                                var isLink = data[i][j].UI.TBody[k].Column[l].Button.Type == "link";
                                                var isButton = data[i][j].UI.TBody[k].Column[l].Button.Type == "button";
                                                if (isGroup) {
                                                    html += '<div class="btn-group none-bottom">';
                                                    html += '   <button class="btn dropdown-toggle ' + data[i][j].UI.TBody[k].Column[l].Button.Class + '" data-toggle="dropdown">';
                                                    html += '       <i class="icon-cog"></i>';
                                                    html += '   ' + data[i][j].UI.TBody[k].Column[l].Button.Title;
                                                    html += '       <i class="icon-angle-down"></i>';
                                                    html += '   </button>';
                                                    html += '   <ul class="dropdown-menu pull-right">';
                                                }
                                                if (data[i][j].UI.TBody[k].Column[l].Button.Item != null) {
                                                    var arrItem = data[i][j].UI.TBody[k].Column[l].Button.Item;
                                                    for (var m = 0; m < arrItem.length; m++) {
                                                        if (isGroup) {
                                                            html += '<li>';
                                                            html += '   <a';
                                                        }
                                                        if (isLink) {
                                                            html += '   <a';
                                                        }
                                                        if (isButton) {
                                                            html += '   <button';
                                                        }
                                                        if (arrItem[m].Id != null) {
                                                            html += ' id="' + arrItem[m].Id + '"';
                                                        }
                                                        if (arrItem[m].Name != null) {
                                                            html += ' name="' + arrItem[m].Name + '"';
                                                        }
                                                        if (arrItem[m].Class != null) {
                                                            html += ' class="' + arrItem[m].Class + '"';
                                                        }
                                                        if (arrItem[m].Attribute != null) {
                                                            for (var n = 0; n < arrItem[m].Attribute.length; n++) {
                                                                html += ' ' + arrItem[m].Attribute[n].Name + '=\'' + arrItem[m].Attribute[n].Value + '\'';
                                                            }
                                                        }
                                                        html += '>';
                                                        if (arrItem[m].Icon != null) {
                                                            html += '   <i class="' + arrItem[m].Icon + '"></i>  ';
                                                        }
                                                        if (arrItem[m].Text != null) {
                                                            html += arrItem[m].Text;
                                                        }
                                                        if (isLink) {
                                                            html += '   </a>';
                                                        }
                                                        if (isButton) {
                                                            html += '   </button>';
                                                        }
                                                        if (isGroup) {
                                                            html += '   </a>';
                                                            html += '</li>';
                                                        }
                                                    }
                                                }
                                                if (isGroup) {
                                                    html += '   </ul>';
                                                    html += '</div>';
                                                }
                                            }
                                            html += '   </td>';
                                        }
                                    }
                                    html += '   </tr>';
                                }
                            }
                            break;
                        case "ui":
                            var id = '';
                            var name = '';
                            var style = '';
                            var value = '';
                            var attr = '';
                            if (data[i][j].UI.Id != null) {
                                id = ' id="' + data[i][j].UI.Id + '"';
                            }
                            if (data[i][j].UI.Name != null) {
                                name = ' name="' + data[i][j].UI.Name + '"';
                            }
                            if (data[i][j].UI.Class != null) {
                                style = ' class="' + data[i][j].UI.Class + '"';
                            }
                            if (data[i][j].UI.Value != null) {
                                value = data[i][j].UI.Value;
                            }
                            if (data[i][j].UI.Attribute != null) {
                                for (var k = 0; k < data[i][j].UI.Attribute.length; k++) {
                                    attr += ' ' + data[i][j].UI.Attribute[k].Name + '=\'' + data[i][j].UI.Attribute[k].Value + '\'';
                                }
                            }
                            html += '<div class="control-group">';
                            html += '   <label class="control-label" for="' + (data[i][j].UI.Id != null ? data[i][j].UI.Id : "") + '">';
                            html += '   ' + (data[i][j].UI.Title != null ? data[i][j].UI.Title : "");
                            if (data[i][j].Required) {
                                html += '<span class="required">*</span>';
                            }
                            html += '   </label>';
                            html += '   <div class="controls">';
                            switch (data[i][j].UI.Type.toLowerCase()) {
                                case "text":
                                    var hasAddOn = data[i][j].UI.AddOn != null;
                                    if (hasAddOn) {
                                        html += '<div class="input-append date ' + data[i][j].UI.AddOn.Class + '">';
                                        html += '<input type="text"' + id + name + style + attr + ' value="' + value + '"/>';
                                        for (var k = 0; k < data[i][j].UI.AddOn.Item.length; k++) {
                                            html += '<span class="add-on" title="' + data[i][j].UI.AddOn.Item[k].Title + '"><i class="' + data[i][j].UI.AddOn.Item[k].Class + '"></i></span>';
                                        }
                                        html += '</div>';
                                    }
                                    else {
                                        html += '<input type="text"' + id + name + style + attr + ' value="' + value + '"/>';
                                    }
                                    break;
                                case "textarea":
                                    html += '<textarea' + id + name + style + attr + '>' + value + '</textarea>';
                                    break;
                                case "label":
                                    html += '<label' + id + name + style + attr + '>' + value + '</label>';
                                    break;
                                case "span":
                                    html += '<span' + id + name + style + attr + '>' + value + '</span>';
                                    break;
                                case "select":
                                    html += '<select' + id + name + style + attr + '>';
                                    if (data[i][j].UI.Option != null && data[i][j].UI.Option.length > 0) {
                                        for (var l = 0; l < data[i][j].UI.Option.length; l++) {
                                            html += '<option value="' + data[i][j].UI.Option[l].Value + '">' + data[i][j].UI.Option[l].Text + '</option>';
                                        }
                                    }
                                    html += '</select>';
                                    break;
                                case "checkbox":
                                    html += '<label' + style + '">';
                                    html += '   <input type="checkbox"' + id + name + attr + ' value="' + value + '"/>';
                                    html += '</label>';
                                    break;
                                case "radio":
                                    html += '<label' + style + '">';
                                    html += '   <input type="radio"' + id + name + attr + ' value="' + value + '"/>';
                                    html += '</label>';
                                    break;
                                case "button":
                                    html += '<button type="button"' + id + name + style + attr + ' value="' + value + '">';
                                    if (data[i][j].UI.Icon != null) {
                                        html += '<i class="' + data[i][j].UI.Icon + '"></i>  ';
                                    }
                                    if (data[i][j].UI.Text != null) {
                                        html += data[i][j].UI.Text;
                                    }
                                    html += '</button>  ';
                                    break;
                                case "link":
                                    html += '<a' + id + name + style + attr + '>';
                                    if (data[i][j].UI.Icon != null) {
                                        html += '<i class="' + data[i][j].UI.Icon + '"></i>  ';
                                    }
                                    if (data[i][j].UI.Text != null) {
                                        html += data[i][j].UI.Text;
                                    }
                                    html += '</a>  ';
                                    break;
                            }
                            if (data[i][j].Help != null) {
                                html += '<span class="help-block">' + data[i][j].Help + '</span>';
                            }
                            html += '   </div>';
                            html += '</div>';
                            break;
                    }
                    if (!isNoDiv) {
                        html += '</div>';
                    }

                }
                if (!isNoDiv) {
                    html += '</div>';
                }
            }
        }
        else {
            if (data != null) {
                for (var i = 0; i < data.length; i++) {
                    var id = '';
                    var style = '';
                    var value = '';
                    var attr = '';
                    if (data[i].Id != null) {
                        id = ' id="' + data[i].Id + '"';
                    }
                    if (data[i].Class != null) {
                        style = ' class="' + data[i].Class + '"';
                    }
                    if (data[i].Value != null) {
                        value = ' value="' + data[i].Value + '"';
                    }
                    if (data[i].Attribute != null) {
                        for (var k = 0; k < data[i].Attribute.length; k++) {
                            attr += ' ' + data[i].Attribute[k].Name + '=\'' + data[i].Attribute[k].Value + '\'';
                        }
                    }
                    if (data[i].Type == "link") {
                        html += '<a ' + id + style + attr + '>';
                    }
                    else {
                        html += '<button type="button"' + id + style + value + attr + '>';
                    }
                    if (data[i].Icon != null) {
                        html += '<i class="' + data[i].Icon + '"></i> ';
                    }
                    if (data[i].Text != null) {
                        html += data[i].Text;
                    }
                    if (data[i].Type == "link") {
                        html += '</a>   ';
                    }
                    else {
                        html += '</button>  ';
                    }
                }
            }
        }
        return html;
    }
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các method dùng cho localStorage
************************************************************************/
var sysstorage = {
    exist: function () {
        try {
            localStorage.setItem('systemtest', 'test');
            localStorage.removeItem('systemtest');
            return true;
        } catch (e) {
            return false;
        }
    },
    set: function (key, value) {
        if (sysstorage.exist()) {
            localStorage.removeItem(key);
            localStorage.setItem(key, value);
        }
    },
    get: function (key) {
        if (sysstorage.exist()) {
            var result = localStorage.getItem(key);
            return result;
        }
        else {
            return null;
        }
    },
    remove: function (key) {
        if (sysstorage.exist()) {
            localStorage.removeItem(key);
        }
    }
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các method dùng để load file động
************************************************************************/
var sysfile = {
    item: new Array(),
    load: function (obj, callback) {
        var arrJs = new Array();
        var arrCss = new Array();
        var type = $.type(obj);
        var version = "1.0.0";
        if (!sysenvironment.is_real) {
            version = syscommon.random(10);
        }
        else if (!(typeof sysversion === "undefined")) {
            version = sysversion;
        }
        switch (type) {
            case "array":
                for (var i = 0; i < obj.length; i++) {
                    var url = obj[i] + "?v=" + version;
                    if ($.inArray(url, sysfile.item) == -1) {
                        sysfile.item.push(url);
                        switch (sysfile.get_extension(obj[i])) {
                            case "js":
                                arrJs.push(url);
                                break;
                            case "css":
                                arrCss.push(url);
                                break;
                        }
                    }
                }
                break;
            case "string":
                var url = obj + "?v=" + version;
                if ($.inArray(url, sysfile.item) == -1) {
                    sysfile.item.push(url);
                    switch (sysfile.get_extension(obj)) {
                        case "js":
                            arrJs.push(url);
                            break;
                        case "css":
                            arrCss.push(url);
                            break;
                    }
                }
                break;
        }
        if (arrCss.length > 0) {
            head.load(arrCss);
        }
        if (arrJs.length > 0) {
            head.load(arrJs, function () {
                callback();
            });
        }
        else {
            if ($.type(callback) === "function") {
                callback();
            }
        }
    },
    get_extension: function (url) {
        try {
            url = url.toLowerCase().split('?')[0];
            return url.split('.').pop();
        }
        catch (e) {
            return '';
        }
    }
};
/***********************************************************************
Author  : VanDH
Desc    : Class dùng để xác định môi trường đang chạy là thật hay test
************************************************************************/
var sysenvironment = {
    is_real: false,
    init: function () {
        var host = window.location.hostname;
        if (host == "bcrm.cintamobil.com") {
            sysenvironment.is_real = true;
        }
        else {
            sysenvironment.is_real = false;
        }
    }
};
/***********************************************************************
Author  : VanDH
Desc    : Class định nghĩa các method dùng để gửi và nhận data
************************************************************************/
var sysrequest = {
    defaults: {
        element: null,
        blockUI: null,
        scrollUI: null,
        action: '',
        controller: '',
        type: "POST",
        data: {},
        dataType: "json",
        async: true,
        traditional: true,
        callback: function (data) { }
    },
    send: function (options) {
        var settings = $.extend({}, sysrequest.defaults, options);
        var url = "/" + settings.controller + (settings.action != '' ? "/" + settings.action : settings.action);
        if (settings.element == null) settings.element = $('div#main-content');
        if (settings.blockUI == null) settings.blockUI = settings.element;
        if (settings.scrollUI == null) settings.scrollUI = settings.blockUI;
        $.ajax({
            url: url,
            type: settings.type,
            data: (settings.data),
            dataType: settings.dataType,
            async: settings.async,
            traditional: settings.traditional,
            beforeSend: function () {
                app.blockUI(settings.blockUI);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.modal.in').each(function(){
                    $(this).modal('hide');
                });
                sysmess.error('Có lỗi xảy ra khi gửi yêu cầu xử lý!');
                sysmess.log(textStatus);
                sysmess.log(errorThrown);
            },
            success: function (response) {
                switch (settings.dataType) {
                    case "html":
                        settings.element.html(response);
                        settings.callback();
                        syscommon.scroll(settings.scrollUI);
                        break;
                    case "json":
                        settings.callback(response);
                        break;
                }
            },
            complete: function () {
                app.unblockUI(settings.blockUI);
            }
        });
    }
};
/***********************************************************************
Author  : VanDH
Desc    : Initiation
************************************************************************/
(function (win) {
    $(document).ready(function () {
        app.init();
        sysdialog.init();
        sysenvironment.init();
    });
})(window);