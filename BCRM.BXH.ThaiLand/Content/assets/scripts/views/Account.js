﻿var account_logon = {
    lang_res : {
        msg_required_logininfo :'Enter your account infomation.',
        msg_required_usrandpassword : 'Username or Pasword is not correct.'
    },
    init: function (status,lan_res) {
        $.extend(account_logon.lang_res ,lan_res );

        $(document).ready(function () {
            $(".alert-error").addClass("hide");
            $(".alert-error span").html(account_logon.lang_res.msg_required_logininfo );
            if (status == "False") {
                $(".alert-error").removeClass("hide");
                $(".alert-error span").html(account_logon.lang_res.msg_required_usrandpassword );
            }
            $("#btnLogOn").off("click");
            $("#btnLogOn").on("click", function () {
                $(".alert-error").addClass("hide");
                $(".alert-error span").html(account_logon.lang_res.msg_required_logininfo );
                $("#frmLogOn").submit();
            });
        });
    }
}
 